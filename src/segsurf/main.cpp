/* Opfront - Optimal front segmentation
 * Copyright (c) 2014, Jens Petersen
 * All rights reserved.
 *
 * Opfront is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this software.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Opfront has a range of dependencies and if you decide to use or
 * modify it, it should be done in compliance with what ever licenses and
 * copyright issues that apply for those dependencies.
 */
#include <iostream>
#include <boost/program_options.hpp>
#include "cimgext/cimgext.h"
#include "opfront/opfront.h"
#include "opfront/thumbnail.h"
#include "opfront/opfront_init.h"
#include "opfront/multi_surface_meta.h"
#include "opfront/image_trace.h"
#include "util/gts_util.h"
#include "util/cimg_util.h"
#include "util/lalg.h"

#ifdef VISUALIZATION
#include "opfront/mainloop.h"
#endif

namespace po = boost::program_options;

opfront *segmentor;
cimg_library::CImg<unsigned char> mask;
cimg_library::CImg<ofloat> volume, probability;

int smoothness_constraint;
ofloat smoothness_penalty, seed_radius, weight;

int
edge_offset(int surface_i, int surface_j, int column_i, int column_j,
    void *params)
{
    // No preference for any specific separation.
    return 0;
}

void
edge_constraint(int surface_i, int surface_j, int column_i, int column_j,
    int &minus_delta, int &plus_delta, void *params)
{
    // Smoothness constraints.
    plus_delta = smoothness_constraint;
    minus_delta = -smoothness_constraint;
    return;
}

ofloat
edge_cost(int surface_i, int surface_j, int column_i, int column_j, int delta,
    void *params)
{
    // We are using only linear costs, so signal this (return 1 if not).
    if(delta < 0) return -1;
    // Smoothness penalties.
    return delta*smoothness_penalty;
}

ofloat
surface_cost(int surface, int column_index, int node_index, void *params)
{
    if(segmentor->column_length(column_index) == 1) return 0;
    // Handle boundary conditions
    if(node_index == segmentor->column_length(column_index)-1) node_index--;

    ofloat x0[3], x1[3];
    segmentor->column_position(column_index, node_index, x0),
    segmentor->column_position(column_index, node_index+1, x1);
    ofloat d = linalg_dis_ve3(x0, x1);
    if(d < 0.00001) return 0;
    volume.xyz2ijk(x0);
    volume.xyz2ijk(x1);
    ofloat v0 = volume.cubic_atXYZ(x0[0], x0[1], x0[2]),
           v1 = volume.cubic_atXYZ(x1[0], x1[1], x1[2]);
    //std::cout << x0[0] << " " << x0[1] << " " << x0[2] << ": " << v0 << " - ";
    //std::cout << x1[0] << " " << x1[1] << " " << x1[2] << ": " << v1
    //    << " - " << -weight*(v1-v0)/d << std::endl;
    return -weight*(v1-v0)/d;
}

static bool
mask_vertex(GtsMVertex *m, void *params)
{
    if(!mask.width()) return 1;
    GtsPoint *p = GTS_POINT(m);
    ofloat x = p->x, y = p->y, z = p->z;
    mask.xyz2ijk(x, y, z);
    return mask.atXYZ(static_cast<int>(x+0.5), static_cast<int>(y+0.5),
        static_cast<int>(z+0.5), 0, 0);
}

ofloat
region_cost(int region, int column_index, int node_index, void *params)
{
    // Make sure we return early if probability files were not supplied.
    if(weight == 1) return 0;
    ofloat x[3];
    segmentor->column_position(column_index, node_index, x);
    probability.xyz2ijk(x);
    ofloat l = probability.cubic_atXYZ(x[0], x[1], x[2]);
    switch(region)
    {
    case 0: return (1-weight)*(1-l);
    default: return (1-weight)*(l);
    }
}

int
main(int ac, char* av[])
{
    try
    {
        char thumbnail_axis;
        int front_edge_links, maximum_front_size, max_iterations,
            flow_line_type, flow_line_kernel_size, compute_min_marginals;
        ofloat sphx, sphy, sphz, sphr, flow_line_kernel_spacing,
            flow_line_regularization, flow_line_sample_interval,
            maximum_edge_length, flow_line_max_inner, flow_line_max_outer;
            std::string volume_file, probability_file,
            segmentation_file, mask_file, output_prefix;
        po::options_description options_description("Allowed options");
        options_description.add_options()
            ("help,h", "produce help message")
            ("sphere_x,x", po::value<ofloat>(&sphx)->default_value(-1),
                "seed sphere center x coordinate")
            ("sphere_y,y", po::value<ofloat>(&sphy)->default_value(-1),
                "seed sphere center y coordinate")
            ("sphere_z,z", po::value<ofloat>(&sphz)->default_value(-1),
                "seed sphere center z coordinate")
            ("sphere_radius,g",
                po::value<ofloat>(&sphr)->default_value(5),
                "seed sphere radius")
            ("volume_file,v",
                po::value<std::string>(&volume_file),
                "the volume file")
            ("probability_file,L",
                po::value<std::string>(&probability_file),
                "the probability file")
            ("segmentation_file,s",
                po::value<std::string>(&segmentation_file),
                "(optional) the initial segmentation file")
            ("mask,m",
                po::value<std::string>(&mask_file),
                "(optional) only segment within positive mask values")
            ("(optional) output_prefix,p",
                po::value<std::string>(&output_prefix),
                "output segmentation files using this prefix")
            ("smoothness_penalty,i",
                po::value<ofloat>(&smoothness_penalty)->default_value(1),
                "smoothness penalty of the surface")
            ("smoothness_constraint,I",
                po::value<int>(&smoothness_constraint)->default_value(-1),
                "smoothness constraint of the surface")
            ("weight,w",
                po::value<ofloat>(&weight)->default_value(0.5),
                "weight between surface (1) and region (0) cost")
            ("front_edge_links,l",
                po::value<int>(&front_edge_links)->default_value(20),
                "the recomputed boundary around moving vertices")
            ("maximum_front_size,a",
                po::value<int>(&maximum_front_size)->default_value(-1),
                "limit front sizes, by splitting fronts (for memory reasons)")
            ("flow_line_sample_interval,b",
                po::value<ofloat>(&flow_line_sample_interval)->
                default_value(1), "Flow line sampling interval")
            ("flow_line_type,t",
                po::value<int>(&flow_line_type)->default_value(0),
                "0 - Gauss, 1 - Canny-Deriche, 2 - ELF, 3 - Distance")
            ("flow_line_kernel_spacing,k",
                po::value<ofloat>(&flow_line_kernel_spacing)->default_value(1),
                "the convolution grid spacing")
            ("flow_line_regularization,r",
                po::value<ofloat>(&flow_line_regularization)->default_value(2),
                "the regularization used when computing flow lines")
            ("flow_line_kernel_size,c",
                po::value<int>(&flow_line_kernel_size)->default_value(21),
                "the convolution kernel size, should be odd positive number")
            ("flow_line_max_inner,M",
                po::value<ofloat>(&flow_line_max_inner)->default_value(-1),
                "the maximum inner flow line length (mm)")
            ("flow_line_max_outer,N",
                po::value<ofloat>(&flow_line_max_outer)->default_value(-1),
                "the maximum exterior flow line length (mm)")
            ("mesh_edge_length,e",
                po::value<ofloat>(&maximum_edge_length)->default_value(1),
                "the triangle mesh maximum edge length")
            ("thumbnail_axis,n",
                po::value<char>(&thumbnail_axis)->default_value(' '),
                "(optional) output projected 2D images along 'x', 'y' or 'z'")
            ("maximum_iterations,K",
                po::value<int>(&max_iterations)->default_value(-1),
                "(optional) stop after this amount of iterations")
            ("min_marginals,C",
                po::value<int>(&compute_min_marginals)->default_value(0),
                (std::string("0 - no min marginals, 1 = 0/1, ")+
                 std::string("2 = banded binary, 3 = full binary, ")+
                 std::string("4 = banded n-ary, 5 = full n-ary")).c_str());
        po::variables_map vm;
        po::store(po::parse_command_line(ac, av, options_description), vm);
        po::notify(vm);

        if(vm.count("help"))
        {
            std::cout << "Optimal front single surface segmentation"
                      << std::endl;
            std::cout << "Version 0.70 2015 Jens Petersen" << std::endl;
            std::cout << std::endl;
            std::cout << "Usage: options_description [options]" << std::endl;
            std::cout << options_description;
            return 0;
        }
        if((sphx == -1 || sphy == -1 || sphz == -1) && segmentation_file == "")
        {
            std::cout << "Error: please specify seed point or initial "
                      << "segmentation" << std::endl;
            return 1;
        }
        if(volume_file == "")
        {
            std::cout << "Error: please specify volume" << std::endl;
            return 1;
        }
        else
        {
            std::cout << "Loading volume... " << std::flush;
            volume.load(volume_file.c_str());
            std::cout << "spacing: " << volume.metadata.spacing[0]
                      << "x" << volume.metadata.spacing[1]
                      << "x" << volume.metadata.spacing[2]
                      << " origin: (" << volume.metadata.origin[0]
                      << "," << volume.metadata.origin[1]
                      << "," << volume.metadata.origin[2]
                      << ") done!" << std::endl;
        }
        if(probability_file != "")
        {
            std::cout << "Loading probability file... " << std::flush;
            probability.load(probability_file.c_str());
            std::cout << "done!" << std::endl;
        } else weight = 1;
        GtsSurface *surface;
        if(segmentation_file == "")
            surface = opfront_init_sphere(volume, sphx, sphy, sphz, sphr, 1);
        else
        {
            surface = opfront_init_gts(volume, segmentation_file, 1);
            std::cout << "Loaded vertices: "
                << gts_surface_vertex_number(surface)
                << " edges: " << gts_surface_edge_number(surface)
                << " faces: " << gts_surface_face_number(surface) << std::endl;
        }
        if(mask_file != "") mask.load(mask_file.c_str());
        image_tracer_type image_tracer(flow_line_regularization,
            flow_line_sample_interval, flow_line_kernel_spacing,
            flow_line_max_inner, flow_line_max_outer,
            flow_line_type, flow_line_kernel_size);
        segmentor = new opfront(surface, maximum_edge_length, edge_offset, 0,
            edge_constraint, 0, edge_cost, 0, surface_cost, 0, region_cost, 0,
            mask.width()? mask_vertex: 0, 0, compute_min_marginals);
#ifdef VISUALIZATION
        std::cout << "Visualizing!" << std::endl;
        main_loop(segmentor, &image_tracer, surface, output_prefix,
            front_edge_links, maximum_front_size);
#else
        std::cout << "Iterating..." << std::endl;
        int i = 0;
        do
        {
            std::stringstream ss;
            ss << output_prefix << "iteration" << i++ << ".bmp";
            create_thumbnail(thumbnail_axis, ss.str().c_str(), volume,
                surface);
        }
        while(segmentor->complete_step(image_tracer, front_edge_links,
            maximum_front_size) && max_iterations--);
#endif
        std::cout << "Done!" << std::endl;
        std::cout << "Saving... " << std::flush;
        multi_surface_meta_xyz2ijk(volume, surface);
        multi_surface_save_tool(surface, output_prefix.c_str(),
            compute_min_marginals > 0, compute_min_marginals > 0);
        std::cout << "done!" << std::endl;
        delete segmentor;
        gts_object_destroy(GTS_OBJECT(surface));
    }
    catch(std::exception& e)
    {
        std::cout << "Error: " << e.what() << std::endl;
        gts_finalize();
        return 1;
    }
    gts_finalize();
    return 0;
}

