/* Opfront - Optimal front segmentation
 * Copyright (c) 2014, Jens Petersen
 * All rights reserved.
 *
 * Opfront is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this software.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Opfront has a range of dependencies and if you decide to use or
 * modify it, it should be done in compliance with what ever licenses and
 * copyright issues that apply for those dependencies.
 */
#include <iostream>
#include <boost/program_options.hpp>
#include "cimgext/cimgext.h"
#include "util/lalg.h"
#include "opfront/image_trace.h"
#include "util/gts_util.h"
#include "util/cimg_util.h"
#include "util/statistics.h"
#include "opfront/opfront.h"
#include "opfront/thumbnail.h"
#include "opfront/opfront_init.h"
#include "opfront/multi_surface_meta.h"

#ifdef VISUALIZATION
#include "opfront/mainloop.h"
#endif

namespace po = boost::program_options;

opfront *segmentor;
cimg_library::CImg<short> *volumes;
cimg_library::CImg<unsigned char> mask;
cimg_library::CImg<ofloat> *deformations;
linalg_mat<ofloat, 4> *affs;

static std::vector<gdouble> inner_distance, outer_distance;

GtsSurface* surface;
int surface_count;

int inner_smoothness_constraint, outer_smoothness_constraint;
ofloat inner_smoothness_penalty, outer_smoothness_penalty, separation_penalty,
    longitudinal_penalty;
ofloat seed_radius;

int
edge_offset(int surface_i, int surface_j, int column_i, int column_j,
    void *params)
{
    // No preference for any specific separation.
    return 0;
}

void
edge_constraint(int surface_i, int surface_j, int column_i, int column_j,
    int &minus_delta, int &plus_delta, void *params)
{
    static const int big_number = 10000;
    if(column_i == column_j)
    {
        assert(surface_i != surface_j);
        // Interior and exterior surface separation constraints.
        int max_surf, min_surf;
        if(surface_i > surface_j)
        {
            max_surf = surface_i;
            min_surf = surface_j;
        }
        else
        {
            max_surf = surface_j;
            min_surf = surface_i;
        }
        if(max_surf%2 && max_surf-min_surf == 1)
        {
            // Ordered surfaces.
            plus_delta = big_number;
            minus_delta = 0;
        }
        // Longitudinal separation constraint. Note we will limit longitudinal
        // connections to the nearest in time surfaces. We could connect all
        // but this will be much more expensive. 
        else if(max_surf-min_surf == 2)
        {
            // No hard constraint
            plus_delta = big_number;
            minus_delta = -big_number;
        }
    }
    // Smoothness constraints.
    else if(surface_i == surface_j && column_i != column_j)
    {
        if(surface_i)
        {
            plus_delta = outer_smoothness_constraint;
            minus_delta = -outer_smoothness_constraint;
        }
        else
        {
            plus_delta = inner_smoothness_constraint;
            minus_delta = -inner_smoothness_constraint;
        }
    }
}

ofloat
edge_cost(int surface_i, int surface_j, int column_i, int column_j, int delta,
    void *params)
{
    // We are using only linear costs, so signal this (return 1 if not).
    if(delta < 0) return -1;

    if(surface_i == surface_j)
    {
        // Smoothness penalties.
        assert(column_i != column_j);
        if(surface_i) return delta*outer_smoothness_penalty;
        else return delta*inner_smoothness_penalty;
    }
    else if(column_i == column_j)
    {
        assert(surface_i != surface_j);
        // Interior and exterior surface separation constraints.
        if(abs(surface_i-surface_j) == 1) return delta*separation_penalty;
        // Longitudinal separation constraint
        else if(!((surface_j-surface_i)%2)) return delta*longitudinal_penalty;
        // Optimal front lib currently assumes every surface to be connected...
        else return 0;
    }
    // We haven't added such edges, so we shouldn't get this callback.
    std::cout << surface_i << " " << surface_j << " " << column_i << " "
              << column_j << " " << delta << std::endl;
    assert(0);
    return 0;
}

// Deform
//  Deform a point using a deformation field.
template<class FLOAT_TYPE>
void
deform(const cimg_library::CImg<ofloat> &field, FLOAT_TYPE *x)
{
    // Handle zero deformation fields.
    if(!field.width()) return;

    field.xyz2ijk(x);
    // Removed the bounds check for easy handling of the argument zero to
    // allow fields with no deformation.
    //assert(x[0] >= 0 && x[0] < field.width()-1 ||
    //       x[1] >= 0 && x[1] < field.height()-1 ||
    //       x[2] >= 0 && x[2] < field.depth()-1);
    FLOAT_TYPE y[] = {x[0], x[1], x[2]};
    for(int i = 0; i < 3; i++) x[i] = field.cubic_atXYZ(y[0], y[1], y[2], i);
}

void
position(int surface_index, int column_index, int node_index, ofloat *p)
{
    segmentor->column_position(column_index, node_index, p);
    deform(deformations[surface_index/2], p);
    linalg_mul_m44_ve3(affs[surface_index/2], p);
}

ofloat
surface_cost(int surface_index, int column_index, int node_index, void *params)
{
    if(segmentor->column_length(column_index) == 1) return 0;
    // Handle boundary conditions
    if(node_index == segmentor->column_length(column_index)-1) node_index--;

    ofloat c0[3], c1[3];
    position(surface_index, column_index, node_index, c0);
    position(surface_index, column_index, node_index+1, c1);
    ofloat d = linalg_dis_ve3(c0, c1);
    if(d < 0.00001) return 0;
    cimg_library::CImg<short> &vol = volumes[surface_index/2];
    vol.xyz2ijk(c0);
    vol.xyz2ijk(c1);
    ofloat v0 = vol.cubic_atXYZ(c0[0], c0[1], c0[2]),
           v1 = vol.cubic_atXYZ(c1[0], c1[1], c1[2]);
    return (2*(surface_index%2)-1)*(v1-v0)/d;
}

ofloat
region_cost(int region, int column_index, int node_index, void *params)
{
    return 0;
}

static bool
mask_vertex(GtsMVertex *m, void *params)
{
    if(!mask.width()) return 1;
    GtsPoint *p = GTS_POINT(m);
    ofloat x = p->x, y = p->y, z = p->z;
    mask.xyz2ijk(x, y, z);
    return mask.atXYZ(static_cast<int>(x+0.5), static_cast<int>(y+0.5),
        static_cast<int>(z+0.5), 0, 0);
}

gint
vertex_distance(gpointer item, gpointer data)
{
    GtsMVertex *m = GTS_MVERTEX(item);
    for(int i = 0; i < surface_count/2-1; i++)
    {
        inner_distance.push_back(multi_surface_euclidean_distance(m, i, 2*i));
        outer_distance.push_back(
            multi_surface_euclidean_distance(m, i+1, 2*i+1));
    }
    return 0;
}

gint
vertex_mean_curvature(gpointer item, gpointer data)
{
    std::vector<gdouble> *mean_curvature =
        static_cast<std::vector<gdouble>*>(data);
    GtsVertex *v = GTS_VERTEX(item);
    GtsVector Kh;
    if(!gts_vertex_mean_curvature_normal(v, surface, Kh)) return 0;
    mean_curvature->push_back(sqrt(Kh[0]*Kh[0]+Kh[1]*Kh[1]+Kh[2]*Kh[2])/2);
    return 0;
}

void
surface_stats()
{
    inner_distance.clear();
    outer_distance.clear();
    gts_surface_foreach_vertex(surface, vertex_distance, 0);
    std::cout << "Longitudinal distance: " << std::endl
        << " Inner: " << statistics::summary(inner_distance) << std::endl
        << " Outer: " << statistics::summary(outer_distance) << std::endl;
    std::cout << "Surface mean curvature: " << std::endl; 
    for(int i = 0; i < surface_count; i++)
    {
        std::vector<double> mean_curvature;
        multi_surface_select(surface, i);
        gts_surface_foreach_vertex(surface, vertex_mean_curvature,
            static_cast<void*>(&mean_curvature));
        std::cout << " Surface " << i << ": "
            << statistics::summary(mean_curvature) << std::endl;
    }
    multi_surface_select(surface, 0);
}

#ifdef VISUALIZATION
extern void update();
#endif

void
sub_iteration_done(int, void *params)
{
#ifdef VISUALIZATION
    update();
#endif
}

gint
vertex_to_volumes(gpointer item, gpointer data)
{
    GtsMVertex *m = GTS_MVERTEX(item);
    for(int i = 0; i < mvertex_column_size(m); i++)
    {
        gdouble *p = mvertex_position(m, i);
        deform(deformations[i/2], p);
        linalg_mul_m44_ve3(affs[i/2], p);
        volumes[i/2].xyz2ijk(p);
    }
    return 0;
}

int
main(int ac, char* av[])
{
    try
    {
        char thumbnail_axis;
        int front_edge_links, maximum_front_size, max_iterations,
            flow_line_type, flow_line_kernel_size;
        ofloat sphx, sphy, sphz, sphr, flow_line_kernel_spacing,
            flow_line_regularization, flow_line_sample_interval,
            maximum_edge_length, flow_line_max_inner, flow_line_max_outer;
        std::string segmentation_file, mask_file, output_prefix;
        std::vector<std::string> dates;
        po::options_description options_description("Allowed options");
        options_description.add_options()
            ("help,h", "produce help message")
            ("sphere_x,x", po::value<ofloat>(&sphx)->default_value(-1),
                "seed sphere center x coordinate")
            ("sphere_y,y", po::value<ofloat>(&sphy)->default_value(-1),
                "seed sphere center y coordinate")
            ("sphere_z,z", po::value<ofloat>(&sphz)->default_value(-1),
                "seed sphere center z coordinate")
            ("sphere_radius,g",
                po::value<ofloat>(&sphr)->default_value(5),
                "seed sphere radius")
            ("segmentation_file,s",
                po::value<std::string>(&segmentation_file),
                "(optional) the initial segmentation file")
            ("mask,m",
                po::value<std::string>(&mask_file),
                "(optional) only segment within positive mask values")
            ("(optional) output_prefix,p",
                po::value<std::string>(&output_prefix),
                "output segmentation files using this prefix")
            ("inner_smoothness_penalty,i",
                po::value<ofloat>(&inner_smoothness_penalty)->
                default_value(1), "smoothness penalty of the inner surface")
            ("outer_smoothness_penalty,o",
                po::value<ofloat>(&outer_smoothness_penalty)->
                default_value(1), "smoothness penalty of the outer surface")
            ("inner_smoothness_constraint,I",
                po::value<int>(&inner_smoothness_constraint)->
                default_value(-1),
                "smoothness constraint of the inner surface")
            ("outer_smoothness_constraint,O",
                po::value<int>(&outer_smoothness_constraint)->
                default_value(-1),
                "smoothness constraint of the outer surface")
            ("separation_penalty,d",
                po::value<ofloat>(&separation_penalty)->default_value(0),
                "surface separation penalty")
            ("longitudinal_penalty,L",
                po::value<ofloat>(&longitudinal_penalty)->default_value(1),
                "longitudinal separation penalty")
            ("front_edge_links,l",
                po::value<int>(&front_edge_links)->default_value(20),
                "the recomputed boundary around moving vertices")
            ("maximum_front_size,a",
                po::value<int>(&maximum_front_size)->default_value(-1),
                "limit front sizes, by splitting fronts (for memory reasons)")
            ("flow_line_sample_interval,b",
                po::value<ofloat>(&flow_line_sample_interval)->
                default_value(0.5), "Flow line sampling interval")
            ("flow_line_type,t",
                po::value<int>(&flow_line_type)->default_value(0),
                "0 - Gauss, 1 - Canny-Deriche, 2 - ELF, 3 - Distance")
             ("flow_line_kernel_spacing,k",
                po::value<ofloat>(&flow_line_kernel_spacing)->default_value(1),
                "the convolution grid spacing")
            ("flow_line_regularization,r",
                po::value<ofloat>(&flow_line_regularization)->default_value(2),
                "the regularization used when computing flow lines")
            ("flow_line_kernel_size,c",
                po::value<int>(&flow_line_kernel_size)->default_value(21),
                "the convolution kernel size, should be odd positive number")
            ("flow_line_max_inner,M",
                po::value<ofloat>(&flow_line_max_inner)->default_value(-1),
                "the maximum inner flow line length (mm)")
            ("flow_line_max_outer,N",
                po::value<ofloat>(&flow_line_max_outer)->default_value(-1),
                "the maximum exterior flow line length (mm)")
            ("mesh_edge_length,e",
                po::value<ofloat>(&maximum_edge_length)->
                default_value(1), "the triangle mesh maximum edge length")
            ("thumbnail_axis,n",
                po::value<char>(&thumbnail_axis)->default_value(' '),
                "(optional) output projected 2D images along 'x', 'y', or 'z'")
            ("max_iterations,K",
                po::value<int>(&max_iterations)->default_value(-1),
                "(optional) stop after this amount of iterations")
            ("dates,D", po::value<std::vector<std::string> >(&dates)->
                multitoken(),
                "nrr deformations, aff matrices, followed by volumes");
        po::variables_map vm;
        po::store(po::parse_command_line(ac, av, options_description), vm);
        po::notify(vm);
        if(vm.count("help"))
        {
            std::cout << "Optimal front 4D wall segmentation" << std::endl;
            std::cout << "Version 0.20 2014 Jens Petersen" << std::endl;
            std::cout << std::endl;
            std::cout << "Usage: options_description [options]" << std::endl;
            std::cout << options_description;
            return 0;
        }
        if((sphx == -1 || sphy == -1 || sphz == -1) && segmentation_file == "")
        {
            std::cout << "Error: please specify seed point or initial "
                      << "segmentation" << std::endl;
            return 1;
        }
        std::cout << "Loading images... " << std::flush;
        deformations = new cimg_library::CImg<ofloat>[dates.size()/3];
        volumes = new cimg_library::CImg<short>[dates.size()/3];
        affs = new linalg_mat<ofloat, 4>[dates.size()/3];
        for(int i = 0; i < dates.size()/3; i++)
        {
            volumes[i].load(dates[i+2*dates.size()/3].c_str());
            if(dates[i] != "zero") deformations[i].load(dates[i].c_str());
            if(dates[i+dates.size()/3] == "identity")
                linalg_identity_mat(affs[i]);
            else affs[i].load(dates[i+dates.size()/3].c_str());
        }
        std::cout << "Done!" << std::endl;
        surface_count = 2*dates.size()/3;
        GtsSurface *surface;
        if(segmentation_file == "")
            surface = opfront_init_sphere(deformations[0], sphx, sphy, sphz,
                sphr, surface_count);
        else
        {
            surface = opfront_init_gts(deformations[0], segmentation_file,
                surface_count);
            std::cout << "Loaded vertices: "
                << gts_surface_vertex_number(surface)
                << " edges: " << gts_surface_edge_number(surface)
                << " faces: " << gts_surface_face_number(surface) << std::endl;
        }
        if(mask_file != "") mask.load(mask_file.c_str());
        image_tracer_type image_tracer(flow_line_regularization,
            flow_line_sample_interval, flow_line_kernel_spacing,
            flow_line_max_inner, flow_line_max_outer,
            flow_line_type, flow_line_kernel_size);
        segmentor = new opfront(surface, maximum_edge_length, edge_offset, 0,
            edge_constraint, 0, edge_cost, 0, surface_cost, 0, region_cost, 0,
            mask.width()? mask_vertex: 0, 0, 0, sub_iteration_done);
#ifdef VISUALIZATION
        std::cout << "Visualizing!" << std::endl;
        main_loop(segmentor, &image_tracer, surface, output_prefix,
            front_edge_links, maximum_front_size);
#else
        std::cout << "Iterating..." << std::endl;
        int i = 0;
        do
        {
            std::stringstream ss;
            ss << output_prefix << "iteration" << i++ << ".bmp";
            create_thumbnail(thumbnail_axis, ss.str().c_str(), volumes[0],
                surface);
        }
        while(segmentor->complete_step(image_tracer, front_edge_links,
            maximum_front_size) && max_iterations--);
#endif
        std::cout << "Done!" << std::endl;
        std::cout << "Saving... " << std::flush;
        gts_surface_foreach_vertex(surface, vertex_to_volumes, 0);
        char *filenames[surface_count];
        for(int i = 0; i < surface_count; i++)
        {
            std::stringstream ss;
            ss << output_prefix << "surface" << i%2 << "_" << i/2 << ".gts";
            filenames[i] = new char[ss.str().length()+1];
            strcpy(filenames[i], ss.str().c_str());
        }
        multi_surface_save_all(filenames, surface);
        for(int i = 0; i < surface_count; i++) delete[] filenames[i];
        std::cout << "done!" << std::endl;
        delete[] deformations;
        delete[] volumes;
        delete[] affs;
        delete segmentor;
        gts_object_destroy(GTS_OBJECT(surface));
    }
    catch(std::exception& e)
    {
        std::cout << "Error: " << e.what() << std::endl;
        gts_finalize();
        return 1;
    }
    gts_finalize();
    return 0;
}

