/* Opfront - Optimal front segmentation
 * Copyright (c) 2014, Jens Petersen
 * All rights reserved.
 *
 * Opfront is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.

 * You should have received a copy of the GNU Lesser General Public License
 * along with this software.  If not, see <http://www.gnu.org/licenses/>.

 * Opfront has a range of dependencies and if you decide to use or
 * modify it, it should be done in compliance with what ever licenses and
 * copyright issues that apply for those dependencies.
 */
#include <iostream>
#include <cassert>
#include <cmath>
#include "opfront/front.h"
#include "util/gts_conn.h"

struct front_data
{
    bool (*mask_vertex)(GtsMVertex*, void*);
    void *params;
};

static gint
front_initialize_vertices(gpointer item, gpointer data)
{
    front_data *d = static_cast<front_data*>(data);
    GtsMVertex *m = GTS_MVERTEX(item);
    m->moving = d->mask_vertex(m, d->params);
    return 0;
}

void
front_initialize(GtsSurface *surface,
    bool (*mask_vertex)(GtsMVertex* v, void *params),
    void *mask_vertex_params)
{
    front_data d;
    d.mask_vertex = mask_vertex;
    d.params = mask_vertex_params;
    gts_surface_foreach_vertex(surface, front_initialize_vertices,
        static_cast<front_data*>(&d));
}

static gint
front_copy_vertex(gpointer item, gpointer data)
{
    GtsMVertex *v = GTS_MVERTEX(item);
    vertex_container *vertices = static_cast<vertex_container*>(data);
    v->index = vertices->size();
    vertices->push_back(v);
    return 0;
}

static gint
front_copy_edge(gpointer item, gpointer data)
{
    edge_container *edges = static_cast<edge_container*>(data);
    edges->push_back(GTS_EDGE(item));
    return 0;
}

void
front_copy(GtsSurface *surface, vertex_container &vertices,
    edge_container &edges)
{
    gts_surface_foreach_vertex(surface, front_copy_vertex,
        static_cast<gpointer>(&vertices));
    gts_surface_foreach_edge(surface, front_copy_edge,
        static_cast<gpointer>(&edges));
}

static gint
extract_moving_surface(gpointer item, gpointer data)
{
    GtsSurface *moving_surface = GTS_SURFACE(data);
    GtsTriangle *t = GTS_TRIANGLE(item);
    GtsVertex *a, *b, *c;
    gts_triangle_vertices(t, &a, &b, &c);
    if(GTS_MVERTEX(a)->moving || GTS_MVERTEX(b)->moving ||
       GTS_MVERTEX(c)->moving)
        gts_surface_add_face(moving_surface, GTS_FACE(t));
    return 0;
}

struct front_expand_data
{
    GtsSurface *surface,
        *moving_surface,
        *expanded_surface;
};

static gint
expand_moving_surface(gpointer item, gpointer data)
{
    GtsFace *f = GTS_FACE(item);
    front_expand_data *d = static_cast<front_expand_data*>(data);
    GSList *neighbors = gts_face_neighbors(f, d->surface);
    for(GSList *n = neighbors; n; n = n->next)
    {
        GtsFace *f = GTS_FACE(n->data);
        // Mark all of front as moving.
        GtsVertex *a, *b, *c;
        gts_triangle_vertices(GTS_TRIANGLE(f), &a, &b, &c);
        GTS_MVERTEX(a)->moving = true;
        GTS_MVERTEX(b)->moving = true;
        GTS_MVERTEX(c)->moving = true;
        // Add face to expanded region if it is not already registered as 
        // moving and expanded.
        if(!gts_face_has_parent_surface(f, d->moving_surface))
        {
            gts_surface_add_face(d->moving_surface, f);
            gts_surface_add_face(d->expanded_surface, f);
        }
    }
    g_slist_free(neighbors);
    return 0;
}

GSList*
front_partition(GSList* fronts, GtsSurface* surface, int edge_links, int maximum_size)
{
    GtsGraph *g = gts_surface_graph_new(
            GTS_GRAPH_CLASS(gts_wgraph_class()), surface);
    guint nmin = 100;
    guint mmax = 50;
    guint ntry = 10;
    gfloat imbalance = 0.1;
    gfloat partitions;
    if(maximum_size > 0) partitions = std::ceil(log(
                static_cast<gfloat>(gts_surface_vertex_number(surface))/
                maximum_size)/log(2));
    else partitions = 0;
#ifdef OPFRONT_DEBUG
    std::cout << "Splitting front into roughly " << std::pow(2, partitions)
        << " partitions..." << std::flush; 
#endif
    GSList *partition;
    if(partitions <= 0)
    {
        partition = g_slist_append(NULL, g);
    }
    else
    {
        partition = gts_graph_recursive_bisection(GTS_WGRAPH(g),
                partitions, ntry, mmax, nmin, imbalance);
    }
#ifdef OPFRONT_DEBUG
    std::cout << " Done!" << std::endl;
#endif        
    //GSList *partition = gts_graph_bubble_partition(g, partitions, 10, 0, 0);
    int c = 0;
    GSList * big_components = NULL;
    for(GSList *i = partition; i; i = i->next)
    {
#ifdef OPFRONT_DEBUG
        std::cout << "Component: " << c << " vertices:" << std::flush;
#endif
        GtsSurface *s1 = gts_surface_graph_surface(
                GTS_GRAPH(i->data), surface);
        GSList *connected_components = gts_connected_components(s1);
        for(GSList *j = connected_components; j; j = j->next)
        {
            int compsize = gts_surface_vertex_number(GTS_SURFACE(
                j->data));
            if(maximum_size <= 0 || compsize <= maximum_size)
            {
                fronts = g_slist_append(fronts, j->data);
#ifdef OPFRONT_DEBUG
                std::cout << " " << compsize << std::flush;
#endif
                c++;
            }
            else
            {
                big_components = g_slist_append(big_components, j->data);
            }
        }
        g_slist_free(connected_components);

        gts_object_destroy(GTS_OBJECT(s1));
#ifdef OPFRONT_DEBUG
        std::cout << std::endl;
#endif
    }
    gts_object_destroy(GTS_OBJECT(g));
    g_slist_free(partition);
#ifdef OPFRONT_DEBUG
    if(big_components != NULL)
    {
        std::cout << "A number of components still too big, splitting each." << std::endl;
    }
#endif
    for(GSList *i = big_components; i; i = i->next)
    {
        fronts = front_partition(fronts, GTS_SURFACE(i->data), edge_links, maximum_size);
    }   
    g_slist_free(big_components);
    return fronts;
}

GSList*
front_extract(GtsSurface *surface, int edge_links, int maximum_size)
{
    GSList *fronts = NULL;
    front_expand_data d;
    d.surface = surface;
    d.moving_surface = gts_surface_new(gts_surface_class(),
        gts_face_class(), gts_edge_class(), gts_vertex_class());
    // Construct a moving surface of all faces with a moving vertex.
    gts_surface_foreach_face(surface, extract_moving_surface,
        d.moving_surface);
    if(gts_surface_face_number(d.moving_surface))
    {
        d.expanded_surface = gts_surface_new(gts_surface_class(),
            gts_face_class(), gts_edge_class(), gts_vertex_class());
        GtsSurface *updated_faces = gts_surface_new(gts_surface_class(),
                gts_face_class(), gts_edge_class(), gts_vertex_class());
        gts_surface_merge(updated_faces, d.moving_surface);
        // Expand the moving surface over edge_links edge links.
        for(int i = 0; i < edge_links; i++)
        {
            gts_surface_foreach_face(updated_faces, expand_moving_surface,
                    static_cast<gpointer>(&d));
            if(gts_surface_face_number(d.expanded_surface))
            {
                gts_object_destroy(GTS_OBJECT(updated_faces));
                updated_faces = d.expanded_surface;
                d.expanded_surface = gts_surface_new(gts_surface_class(),
                    gts_face_class(), gts_edge_class(), gts_vertex_class());
            } else break;
        }
        gts_object_destroy(GTS_OBJECT(d.expanded_surface));
        gts_object_destroy(GTS_OBJECT(updated_faces));
        // Split surface into a number of partitions of roughly maximum_size
        // vertices.
        fronts = front_partition(fronts, d.moving_surface, edge_links, maximum_size);
    }
    gts_object_destroy(GTS_OBJECT(d.moving_surface));
    return fronts;
}

