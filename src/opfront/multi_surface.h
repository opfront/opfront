/* Opfront - Optimal front segmentation
 * Copyright (c) 2014, Jens Petersen
 * All rights reserved.
 *
 * Opfront is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this software.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Opfront has a range of dependencies and if you decide to use or
 * modify it, it should be done in compliance with what ever licenses and
 * copyright issues that apply for those dependencies.
 */
#ifndef __MULTI_SURFACE_H__
#define __MULTI_SURFACE_H__

#include <gts.h>

#define GTS_MVERTEX(obj) GTS_OBJECT_CAST(obj,\
                                         GtsMVertex,\
                                         gts_mvertex_class ())

#define GTS_MVERTEX_CLASS(klass) GTS_OBJECT_CLASS_CAST(klass,\
                                                       GtsMVertexClass,\
                                                       gts_mvertex_class())

#define GTS_IS_MVERTEX(obj) (gts_object_is_from_class (obj,\
                                                       gts_mvertex_class ()))

struct _GtsMVertex
{
    GtsVertex parent;
    gint m_surfaces, m_column_size, index;
    // discretized column positions (m_column_size gives the size).
    gdouble *m_positions;
    // surface indices into flow line positions (m_surfaces gives the size).
    int *m_indices;
    // certainty of surface position (m_column_size*m_surfaces gives the size).
    gdouble *m_min_marginals;
    gint moving;
};

struct _GtsMVertexClass
{
    GtsVertexClass parent_class;
};

typedef struct _GtsMVertex GtsMVertex;
typedef struct _GtsMVertexClass GtsMVertexClass;

GtsMVertexClass*
gts_mvertex_class();

static void
mvertex_init(GtsMVertex *mvertex);

static void
mvertex_class_init(GtsMVertexClass *klass);

// Mvertex position
//  Get the position of vertex m with index in column.
gdouble*
mvertex_position(GtsMVertex* m, gint index);

const gdouble*
mvertex_position(const GtsMVertex* m, gint index);

// Mvertex position size
//  Return the number of column positions.
gint
mvertex_column_size(const GtsMVertex *m);

// Mvertex column
//  Set the column of vertex m.
template<class FLOAT_TYPE>
void
mvertex_column(GtsMVertex *m, FLOAT_TYPE *positions, int column_size)
{
    if(m->m_positions) delete[] m->m_positions;
    m->m_positions = new gdouble[column_size*3];
    m->m_column_size = column_size;
    for(int i = 0; i < column_size*3; i++)
        m->m_positions[i] = positions[i];
}

// Mvertex min marginals
//  Set the min marginals of column positions in vertex m.
template<class FLOAT_TYPE>
void
mvertex_min_marginals(GtsMVertex *m, FLOAT_TYPE *min_marginals)
{
    if(m->m_min_marginals) delete[] m->m_min_marginals;
    m->m_min_marginals = new gdouble[2*m->m_surfaces*m->m_column_size];
    for(int i = 0; i < 2*m->m_surfaces*m->m_column_size; i++)
        m->m_min_marginals[i] = min_marginals[i];
}

// Multi midvertex
//  Construct a new midvertex inbetween v0 and v1.
GtsMVertex*
mvertex_midvertex2(const GtsMVertex* v0, const GtsMVertex* v1);

GtsMVertex*
mvertex_midvertex3(const GtsMVertex *v0, const GtsMVertex *v1,
    const GtsMVertex *v2);

// Multi surface
//  Extend current surface with multi surface information.
void
multi_surface(GtsSurface *s, gint count);

// Multi surface index
//  Get the column index of surface.
gint
multi_surface_index(const GtsMVertex *m, gint surface);

// Multi surface min marginal index
//  Get the column index of the minimum min-marginal surface.
gdouble
multi_surface_min_marginal_index(GtsMVertex *m, gint surface);

// Multi surface index
//  Set the column index of surface.
void
multi_surface_index(GtsMVertex *m, gint surface, gint index);


// Multi surface position
//  Get the surface position.
void
multi_surface_position(GtsMVertex *m, gint surface, gdouble *position);

// Multi surface euclidean distance
//  Return the euclidean distance between the multi-surface point on
//  surface s0 and s1.
gdouble
multi_surface_euclidean_distance(GtsMVertex* m, gint s0, gint s1);

// Multi surface select
//  Select the surface with index s, meaning the GTS library will work with its
//  coordinates. 
void
multi_surface_select(GtsSurface *m, gint s);

// Multi surface save
//  Save the surface m with index s to the given filename.
void
multi_surface_save(const char* filename, GtsSurface *m, gint s);

// Multi surface load columns
//  Load the columns in the order given by vertex_order, from filename.
void
multi_surface_load_columns(const char* filename, GtsSurface *m,
    GtsVertex **vertex_order);

// Multi surface save columns
//  Save the columns, possibly in the order given by vertex_order, to filename.
void
multi_surface_save_columns(const char* filename, GtsSurface *m,
    GtsVertex **vertex_order = 0);

// Multi surface save all
//  Saves all surfaces of m, preserving the vertex correspondencies.
void
multi_surface_save_all(char** gts_filenames, GtsSurface *m,
    char *column_filename = 0, char **min_marginal_filenames = 0,
    GtsVertex **vertex_order = 0, GtsEdge **edge_order = 0,
    GtsFace **face_order = 0);

// Loads a multi surface
//void
//multi_surface_load(char **filenames, GtsSurface *m, int surface_count);

// Multi surface count
//  Return the number of contained surfaces.
gint
multi_surface_count(GtsSurface* s);

// Multi surface save tool
//  Handy tool to save surfaces, columns and min marginals.
//  (Should perhaps not be here).
void
multi_surface_save_tool(GtsSurface *surface, const char *output_prefix,
    bool output_columns, bool output_min_marginals, GtsVertex **vertices = 0,
    GtsEdge **edges = 0, GtsFace **faces = 0);

#endif

