/* Opfront - Optimal front segmentation
 * Copyright (c) 2014, Jens Petersen
 * All rights reserved.
 *
 * Opfront is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this software.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Opfront has a range of dependencies and if you decide to use or
 * modify it, it should be done in compliance with what ever licenses and
 * copyright issues that apply for those dependencies.
 */
#ifndef __IMAGE_TRACE_H__
#define __IMAGE_TRACE_H__

#include <vector>
#include "cimgext/cimgext.h"
#include "util/fill.h"

// Should probably work on removing gts from this file.
#include "opfront/defines.h"
#include "opfront/multi_surface.h"
#include "util/gts_voxelization.h"

class image_tracer_type
{
private:
    gts_voxelization voxelizer;
    fill<ofloat, ofloat, gts_voxelization> filler;
    ofloat regularization,
        sample_interval,
        kernel_spacing,
        max_inner,
        max_outer;
    int type,
        kernel_size;
    
public:
    image_tracer_type(ofloat regularization, ofloat sample_interval,
        ofloat kernel_spacing, ofloat max_inner, ofloat max_outer, int type,
        int kernel_size);

    void
    update_surface(GtsSurface *s);

    void
    operator()(const std::vector<GtsMVertex*>& vertices) const;
};

#endif

