/* Opfront - Optimal front segmentation
 * Copyright (c) 2014, Jens Petersen
 * All rights reserved.
 *
 * Opfront is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this software.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Opfront has a range of dependencies and if you decide to use or
 * modify it, it should be done in compliance with what ever licenses and
 * copyright issues that apply for those dependencies.
 */
#ifndef __OPFRONT_H__
#define __OPFRONT_H__

#ifdef OPFRONT_DEBUG
#include <boost/progress.hpp>
#endif

#include <list>
#include "opfront/defines.h"
#include "opfront/multi_surface.h"
#include "opfront/front.h"

void
line_stat(const vertex_container& vertices);

class opfront
{
private:
    // Surfaces.
    GtsSurface* surface;
    int m_surface_count;

    // User callbacks.
    int (*edge_offset)(int, int, int, int, void*);
    void *edge_offset_params;
    void (*edge_constraint)(int, int, int, int, int&, int&, void*);
    void *edge_constraint_params;
    ofloat (*edge_cost)(int, int, int, int, int, void*);
    void *edge_cost_params;
    ofloat (*surface_cost)(int, int, int, void*);
    void *surface_cost_params;
    ofloat (*region_cost)(int, int, int, void*);
    void *region_cost_params;
    bool (*mask)(GtsMVertex*, void*);
    void *mask_params;
    void (*sub_iteration_done)(int, void*);
    void *sub_iteration_done_params;
    void (*error_function)(const char *);

    // Variable used to stop oscilleratory behaviour (should perhaps be
    // user accessible options).
    static const int stored_iterations = 3;

    // Internal stuff.
    int sub_iteration;
    ofloat maximum_edge_length;
    vertex_container front_vertices;
    edge_container front_edges;
    GSList *fronts, *processing_front;
    std::list<GtsSurface*> previous_iterations;
    std::vector<int> front_separation;
    int m_min_marginals;

    int
    balance_surface(int front_edge_links, int maximum_front_size);

    bool
    next_front();

    void
    compute_minimum_cut();

    void
    stop_oscillerations();

public:
    opfront(GtsSurface *_surface,
        ofloat _maximum_edge_length,
        int (*_edge_offset)(int surface_i, int surface_j, int column_i,
            int column_j, void *params),
        void *_edge_offset_params,
        void (*_edge_constraint)(int surface_i, int surface_j, int column_i,
            int column_j, int &minus_delta, int &plus_delta, void *params),
        void *_edge_constraint_params,
        ofloat (*_edge_cost)(int surface_i, int surface_j, int column_i,
            int column_j, int delta, void* params),
        void *_edge_cost_params,
        ofloat (*_surface_cost)(int surface_index, int column_index,
            int node_index, void *params),
        void *_surface_cost_params,
        ofloat (*_region_cost)(int region, int column_index, int node_index,
            void *params),
        void *_region_cost_params,
        bool (*_mask)(GtsMVertex*, void *params) = 0,
        void *_mask_params = 0,
        int min_marginals = 0,
        void (*_sub_iteration_done)(int sub_iteration, void *params) = 0,
        void *_sub_iteration_params = 0,
        void (*error_function)(const char*) = 0);

    ~opfront();

    void
    initialize(ofloat _maximum_edge_length);

    double
    label(int surface, int column_index) const;
    
    double
    min_marginal_label(int surface, int column_index) const;

    const gdouble*
    column_position(int column_index, int node_index) const;

    template<class FLOAT_TYPE>
    FLOAT_TYPE*
    column_position(int column_index, int node_index, FLOAT_TYPE* position)
        const
    {
        const gdouble *p = mvertex_position(front_vertices[column_index],
            node_index);
        for(int i = 0; i < 3; i++) position[i] = p[i];
        return position;
    }

    int
    surface_node_index(int surface, int column_index) const;

    int
    column_count() const;

    int
    edge_count() const;
    
    void
    edge(int edge_index, int &column_index_0, int &column_index_1);

    int
    column_length(int column_index) const;

    int
    surface_count() const;

    int
    front_number(GtsFace *f) const;
    
    template<class column_generator_type>
    bool
    step(column_generator_type &column_generator,
         int front_edge_links,
         int maximum_front_size)
    {
        GtsSurface *s;
        switch(sub_iteration)
        {
        case 0:
            if(previous_iterations.size() >= stored_iterations)
            {
                s = previous_iterations.front();
                gts_object_destroy(GTS_OBJECT(s));
                previous_iterations.pop_front();
            }
            // Not sure if mvertex class is needed or not, but we only check
            // movement with interior surface, so try this...
            s = gts_surface_new(gts_surface_class(),
                    gts_face_class(), gts_edge_class(),
                    gts_vertex_class());
            //    GTS_VERTEX_CLASS(gts_mvertex_class()));
            gts_surface_copy(s, surface);
            previous_iterations.push_back(s);
            if(balance_surface(front_edge_links, maximum_front_size))
            {
                // Note we are assuming the gts surface is unchanged by the
                // sub steps involved. If it was changed, then flow lines
                // would be computed from this changed surface and not the
                // prior iteration surface.
                column_generator.update_surface(surface);
                sub_iteration = 1;
            }
            else
            {
                sub_iteration = 0;
                return false;
            }
            break;
        case 1:
            if(next_front())
            {
#ifdef OPFRONT_DEBUG
                std::cout << "Tracing flow lines" << std::endl;
                boost::progress_timer timer;
#endif
                // Compute flow lines from vertices in the front.
                column_generator(front_vertices);
#ifdef OPFRONT_DEBUG
                line_stat(front_vertices);
#endif
                sub_iteration = 2;
            }
            else sub_iteration = 3;
            break;
        case 2:
            compute_minimum_cut();
            sub_iteration = 1;
            break;
        case 3:
            for(GSList *f = fronts; f; f = f->next)
                gts_object_destroy(GTS_OBJECT(f->data));
            g_slist_free(fronts);
            fronts = NULL;
            // Update gts surface with information computed in the iteration.
            multi_surface_select(surface, 0);

            stop_oscillerations();
            sub_iteration = 0;
            break;
        }
        if(sub_iteration_done)
            sub_iteration_done(sub_iteration, sub_iteration_done_params);
        return true;
    }

    template<class column_generator_type>
    bool
    complete_step(column_generator_type &column_generator,
        int front_edge_links,
        int maximum_front_size)
    {
        do
        {
            if(!step(column_generator, front_edge_links, maximum_front_size))
                return false;
        } while(sub_iteration);
        return true;
    }
};

#endif

