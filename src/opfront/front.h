/* Opfront - Optimal front segmentation
 * Copyright (c) 2014, Jens Petersen
 * All rights reserved.
 *
 * Opfront is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this software.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Opfront has a range of dependencies and if you decide to use or
 * modify it, it should be done in compliance with what ever licenses and
 * copyright issues that apply for those dependencies.
 */
#ifndef __FRONT_H__
#define __FRONT_H__

#include <vector>
#include "opfront/multi_surface.h"

typedef std::vector<GtsMVertex*> vertex_container;
typedef std::vector<GtsEdge*> edge_container;

// Front copy
//  This copies the vertices and edges in the surface into useful containers.
void
front_copy(GtsSurface *surface, vertex_container &vertices,
    edge_container &edges);

// Front initialize
//  Sets all non-static vertices as moving. 
void
front_initialize(GtsSurface *surface,
    bool (*mask_vertex) (GtsMVertex* v, void *params),
    void* mask_vertex_params);

// Front extract
//  This extracts fronts from the surface. A vertex is part of a front if it
//  is connected to a vertex in the surface within 'edge_links' edge links.
//  Fronts are optimized to be equal in size and limited to 'maximum_size'
//  vertices. 
GSList*
front_extract(GtsSurface *surface, int edge_links, int maximum_size);

#endif

