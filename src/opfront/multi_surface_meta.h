/* Opfront - Optimal front segmentation
 * Copyright (c) 2014, Jens Petersen
 * All rights reserved.
 *
 * Opfront is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this software.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Opfront has a range of dependencies and if you decide to use or
 * modify it, it should be done in compliance with what ever licenses and
 * copyright issues that apply for those dependencies.
 */
#ifndef __MULTI_SURFACE_META_H__
#define __MULTI_SURFACE_META_H__

template<class CIMG_TYPE>
gint
multi_surface_meta_vertex_ijk2xyz(gpointer item, gpointer data)
{
    GtsMVertex *m = GTS_MVERTEX(item);
    cimg_library::CImg<CIMG_TYPE> *img =
        static_cast<cimg_library::CImg<CIMG_TYPE>*>(data);
    for(int s = 0; s < mvertex_column_size(m); s++)
        img->ijk2xyz(mvertex_position(m, s));
    return 0;
}

template<class CIMG_TYPE>
gint
multi_surface_meta_vertex_xyz2ijk(gpointer item, gpointer data)
{
    GtsMVertex *m = GTS_MVERTEX(item);
    cimg_library::CImg<CIMG_TYPE> *img =
        static_cast<cimg_library::CImg<CIMG_TYPE>*>(data);
    for(int s = 0; s < mvertex_column_size(m); s++)
        img->xyz2ijk(mvertex_position(m, s));
    return 0;
}

template<class CIMG_TYPE>
void
multi_surface_meta_xyz2ijk(cimg_library::CImg<CIMG_TYPE> &img,
    GtsSurface *surface)
{
    gts_surface_foreach_vertex(surface,
        multi_surface_meta_vertex_xyz2ijk<CIMG_TYPE>,
        static_cast<gpointer>(&img));
}

template<class CIMG_TYPE>
void
multi_surface_meta_ijk2xyz(cimg_library::CImg<CIMG_TYPE> &img,
    GtsSurface *surface)
{
    gts_surface_foreach_vertex(surface,
        multi_surface_meta_vertex_ijk2xyz<CIMG_TYPE>,
        static_cast<gpointer>(&img));
}

#endif

