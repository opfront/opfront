/* Modified GTS library code
 * Copyright (c) 2014, Jens Petersen
 *
 * Based on:
 * GTS - Library for the manipulation of triangulated surfaces
 * Copyright (C) 1999--2002 St�phane Popinet
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	 See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include <math.h>
#include "opfront/gts_plugin.h"

static gint
front_triangle(GtsTriangle *t)
{
    GtsVertex *v1, *v2, *v3;
    gts_triangle_vertices(t, &v1, &v2, &v3);
  
    return GTS_MVERTEX(v1)->moving || GTS_MVERTEX(v2)->moving ||
        GTS_MVERTEX(v3)->moving;
}

static void
add_intersecting(GtsBBox *bb1, GtsBBox *bb2, GtsSurface *intersected)
{
    gts_surface_add_face(intersected, GTS_FACE(bb1->bounded));
    gts_surface_add_face(intersected, GTS_FACE(bb2->bounded));
}

static gint
copy_front_faces(gpointer item, gpointer data)
{
    GtsFace *f = GTS_FACE(item);
    GtsSurface *s = GTS_SURFACE(data);
    if(front_triangle(GTS_TRIANGLE(f))) gts_surface_add_face(s, f);
    return 0;
}

/**
 * gts_surface_is_self_intersecting:
 * @s: a #GtsSurface.
 *
 * Returns: a new #GtsSurface containing the faces of @s which are
 * self-intersecting or %NULL if no faces of @s are self-intersecting.
 */
GtsSurface*
gts_surface_is_self_intersecting_front(GtsSurface *s)
{
    GtsSurface *intersected, *front;
    g_return_val_if_fail(s != NULL, NULL);
    intersected = gts_surface_new(GTS_SURFACE_CLASS(GTS_OBJECT(s)->klass),
        s->face_class, s->edge_class, s->vertex_class);
    front = gts_surface_new(GTS_SURFACE_CLASS(GTS_OBJECT(s)->klass),
        s->face_class, s->edge_class, s->vertex_class);
    gts_surface_foreach_face(s, copy_front_faces, front);
    if(gts_surface_face_number(front) &&
       !gts_surface_foreach_intersecting_face(front,
        (GtsBBTreeTraverseFunc)add_intersecting, intersected) ||
       !gts_surface_vertex_number(intersected))
    {
        gts_object_destroy(GTS_OBJECT(intersected));
        intersected = NULL;
    }
    gts_object_destroy(GTS_OBJECT(front));
    return intersected;
}
