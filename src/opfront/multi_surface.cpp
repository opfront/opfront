/* Opfront - Optimal front segmentation
 * Copyright (c) 2014, Jens Petersen
 * All rights reserved.
 *
 * Opfront is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this software.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Opfront has a range of dependencies and if you decide to use or
 * modify it, it should be done in compliance with what ever licenses and
 * copyright issues that apply for those dependencies.
 */
#include <cassert>
#include <stdio.h>
#include <string.h>
#include <iostream>
#include <sstream>
#include <cmath>
#include <limits>
#include <vector>
#include "opfront/multi_surface.h"
#include "util/lalg.h"

static void
mvertex_init(GtsMVertex *mvertex)
{
    mvertex->m_surfaces = 0;
    mvertex->m_positions = 0;
    mvertex->m_indices = 0;
    mvertex->m_min_marginals = 0;
    mvertex->moving = 0;
}

static void
mvertex_destroy(GtsObject *object)
{
    GtsMVertex *m = GTS_MVERTEX(object);
    if(m->m_positions)
    {
        delete[] m->m_positions;
        m->m_positions = 0;
    }
    if(m->m_indices)
    {
        delete[] m->m_indices;
        m->m_indices = 0;
    }
    if(m->m_min_marginals)
    {
        delete[] m->m_min_marginals;
        m->m_min_marginals = 0;
    }
    m->m_surfaces = 0;
    m->m_column_size = 0;
    (*GTS_OBJECT_CLASS(gts_mvertex_class())->parent_class->destroy)(object);
}

static void
mvertex_clone(GtsObject *clone, GtsObject *object)
{
    GtsMVertex *c = GTS_MVERTEX(clone),
               *o = GTS_MVERTEX(object);
    (*GTS_OBJECT_CLASS(gts_mvertex_class())->parent_class->clone)(clone,
        object);
    c->m_surfaces = o->m_surfaces;
    c->m_column_size = o->m_column_size;
    c->m_positions = new gdouble[c->m_column_size*3];
    c->m_indices = new int[c->m_surfaces];
    c->moving = o->moving;
    for(gint i = 0; i < c->m_column_size*3; i++)
        c->m_positions[i] = o->m_positions[i];
    for(gint i = 0; i < c->m_surfaces; i++)
        c->m_indices[i] = o->m_indices[i];
    if(o->m_min_marginals)
    {
        c->m_min_marginals = new gdouble[2*c->m_surfaces*c->m_column_size];
        for(gint i = 0; i < 2*c->m_surfaces*c->m_column_size; i++)
            c->m_min_marginals[i] = o->m_min_marginals[i];
    } else c->m_min_marginals = 0;
}

static void
mvertex_class_init(GtsMVertexClass *klass)
{
    GTS_OBJECT_CLASS(klass)->destroy = mvertex_destroy;
    GTS_OBJECT_CLASS(klass)->clone = mvertex_clone;
}

GtsMVertexClass*
gts_mvertex_class()
{
    static GtsMVertexClass *klass = NULL;
    if(klass == NULL)
    {
        GtsObjectClassInfo mvertex_info =
        {
            "GtsMVertex",
            sizeof(GtsMVertex),
            sizeof(GtsMVertexClass),
            (GtsObjectClassInitFunc) mvertex_class_init,
            (GtsObjectInitFunc) mvertex_init,
            (GtsArgSetFunc) NULL,
            (GtsArgGetFunc) NULL
        };
        klass = GTS_MVERTEX_CLASS(gts_object_class_new(GTS_OBJECT_CLASS(
            gts_vertex_class()), &mvertex_info));
    }
    return klass;
}

gint
multi_vertex(gpointer item, gpointer data)
{
    gint count = *static_cast<int*>(data);
    GtsMVertex *v = GTS_MVERTEX(item);
    v->m_surfaces = count;
    v->m_indices = new int[count];
    for(int i = 0; i < count; i++) v->m_indices[i] = 0;
    v->m_column_size = 1;
    v->m_positions = new gdouble[v->m_column_size*3];
    v->m_positions[0] = v->parent.p.x;
    v->m_positions[1] = v->parent.p.y;
    v->m_positions[2] = v->parent.p.z;
    return 0;
}

GtsMVertex*
gts_mvertex_new(GtsMVertexClass *klass, gdouble x, gdouble y, gdouble z,
    gint surface_count)
{
  GtsMVertex *v = GTS_MVERTEX(gts_object_new(GTS_OBJECT_CLASS(klass)));
  gts_point_set(GTS_POINT(v), x, y, z);
  multi_vertex((gpointer)v, (gpointer)&surface_count);
  return v;
}

gdouble*
mvertex_position(GtsMVertex* m, gint index)
{
    assert(index >= 0);
    assert(index < m->m_column_size);
    return m->m_positions+index*3;
}

const gdouble*
mvertex_position(const GtsMVertex* m, gint index)
{
    assert(index >= 0);
    assert(index < m->m_column_size);
    return m->m_positions+index*3;
}

gint
mvertex_column_size(const GtsMVertex *m)
{
    return m->m_column_size;
}


const gdouble*
mvertex_min_marginals(const GtsMVertex *m, gint surface, gint index)
{
    assert(surface >= 0);
    assert(surface < m->m_surfaces);
    assert(index >= 0);
    assert(index < m->m_column_size);
    return m->m_min_marginals+2*(index*m->m_surfaces+surface);
}

gint
multi_surface_index(const GtsMVertex *m, gint surface)
{
    assert(surface >= 0);
    assert(surface < m->m_surfaces);
    return m->m_indices[surface];
}

void
multi_surface_index(GtsMVertex *m, gint surface, gint index)
{
    assert(surface >= 0);
    assert(surface < m->m_surfaces);
    m->m_indices[surface] = index;
}

gdouble
multi_surface_min_marginal_index(GtsMVertex *m, gint surface)
{
    assert(surface >= 0);
    assert(surface < m->m_surfaces);
    int idx = m->m_indices[surface], nidx = idx+1;
    gdouble r = 0;
    if(nidx < m->m_column_size)
    {
        if(m->m_min_marginals)
        {
            // Note check this!
            long double
                sb = m->m_min_marginals[(idx*m->m_surfaces+surface)*2],
                so = m->m_min_marginals[(idx*m->m_surfaces+surface)*2+1],
                sa = m->m_min_marginals[(nidx*m->m_surfaces+surface)*2+1];
            long double rd = sb+sa-2*so;
            // If energy is locally flat, then we will use the middle point.
            if(rd == 0) r = 0;
            else r = (sb-so)/rd-0.5;
            // To avoid absurd round off errors:
            if(r > 0.5) r = 0.5;
            if(r < -0.5) r = -0.5;
        }
        else r = 0;
    }
    return r+idx;
}

void
multi_surface_position(GtsMVertex *m, gint surface, gdouble *position)
{
    // Shift position by 0.5 (note we should probably think about making this
    // more sensible. 
    gdouble r = multi_surface_min_marginal_index(m, surface)+0.5;

    int idx = m->m_indices[surface];
    int nidx = idx+1;
    r -= idx;
    gdouble *p = mvertex_position(m, idx);
    if(nidx < m->m_column_size)
    {
        gdouble *n = mvertex_position(m, nidx);
        gdouble t[3];
        linalg_mul_scl_ve3(1-r, p, position);
        linalg_mul_scl_ve3(r, n, t);
        linalg_add_ve3_ve3(t, position);
    }
    else linalg_cop_ve3(p, position);
}

gdouble
multi_surface_euclidean_distance(GtsMVertex* m, gint s0, gint s1)
{
    gdouble d0[3], d1[3];
    multi_surface_position(m, s0, d0);
    multi_surface_position(m, s1, d1);
    return linalg_dis_ve3(d0, d1); 
}

void
multi_surface(GtsSurface *s, gint count)
{
    assert(count > 0);
    gts_surface_foreach_vertex(s, static_cast<GtsFunc>(multi_vertex),
                               static_cast<gpointer>(&count));
}

GtsMVertex*
mvertex_midvertex2(const GtsMVertex *v0, const GtsMVertex *v1)
{
    assert(v0->m_surfaces == v1->m_surfaces);
    GtsMVertex *m = GTS_MVERTEX(gts_object_new(GTS_OBJECT_CLASS(
        gts_mvertex_class())));
    gts_point_set(GTS_POINT(m),
                 (v0->parent.p.x+v1->parent.p.x)/2,
                 (v0->parent.p.y+v1->parent.p.y)/2,
                 (v0->parent.p.z+v1->parent.p.z)/2);
    if(v0->moving || v1->moving) m->moving = true;
    gint surfaces = v0->m_surfaces;
    multi_vertex(m, static_cast<gpointer>(&surfaces));
    return m;
}

GtsMVertex*
mvertex_midvertex3(const GtsMVertex *v0, const GtsMVertex *v1,
                 const GtsMVertex *v2)
{
    assert(v0->m_surfaces == v1->m_surfaces &&
           v1->m_surfaces == v2->m_surfaces);
    GtsMVertex *m = GTS_MVERTEX(gts_object_new(GTS_OBJECT_CLASS(
        gts_mvertex_class())));
    gts_point_set(GTS_POINT(m),
                 (v0->parent.p.x+v1->parent.p.x+v2->parent.p.x)/3,
                 (v0->parent.p.y+v1->parent.p.y+v2->parent.p.y)/3,
                 (v0->parent.p.z+v1->parent.p.z+v2->parent.p.z)/3);
    gint surfaces = v0->m_surfaces;
    multi_vertex(m, static_cast<gpointer>(&surfaces));
    return m;
}

static gint
multi_surface_select_vertex(gpointer item, gpointer data)
{
    GtsMVertex *m = GTS_MVERTEX(item);
    gint *s = (gint*)(data);
    gdouble s0[3];
    multi_surface_position(m, *s, s0);
    m->parent.p.x = s0[0];
    m->parent.p.y = s0[1];
    m->parent.p.z = s0[2];
    return 0;
}

void
multi_surface_select(GtsSurface *m, gint s)
{
    gts_surface_foreach_vertex(m, multi_surface_select_vertex, &s);
}

void
multi_surface_save(const char* filename, GtsSurface *m, gint s)
{
    multi_surface_select(m, s);
    FILE *pfile = fopen(filename, "w");
    gts_surface_write(m, pfile);
    fclose(pfile);
    multi_surface_select(m, 0);
}

struct save_data
{
    FILE **gts_files, *col_file, **min_marginal_files;
    int idx, surface_count;
};

static gint
write_vertex(gpointer item, gpointer data)
{
    save_data *s = (save_data*)data;
    GtsMVertex *m = GTS_MVERTEX(item);
    // Print columns
    if(s->col_file)
    {
        for(int j = 0; j < mvertex_column_size(m); j++)
        {
            const gdouble *c = mvertex_position(m, j);
            fprintf(s->col_file, "%.17g %.17g %.17g ", c[0], c[1], c[2]);
        }
        fprintf(s->col_file, "\n");
    }
    for(gint i = 0; i < s->surface_count; i++)
    {
        gdouble p[3];
        multi_surface_position(m, i, p);
        fprintf(s->gts_files[i], "%.17g %.17g %.17g\n", p[0], p[1], p[2]);
        // Print min marginals
        if(s->min_marginal_files)
        {
            for(int j = 0; j < mvertex_column_size(m); j++)
            {
                const gdouble *c = mvertex_min_marginals(m, i, j);
                fprintf(s->min_marginal_files[i], "%.17g %.17g ", c[0], c[1]);
            }
            fprintf(s->min_marginal_files[i], "\n");
        }
    }
    GTS_OBJECT(m)->reserved = GINT_TO_POINTER(s->idx);
    s->idx++;
    return 0;
}

static gint
write_edge(gpointer item, gpointer data)
{
    save_data *s = (save_data*)(data);
    GtsEdge *e = GTS_EDGE(item);
    GtsPoint *p0 = GTS_POINT(GTS_SEGMENT(e)->v1),
        *p1 = GTS_POINT(GTS_SEGMENT(e)->v2);
    for(gint i = 0; i < s->surface_count; i++)
    {
        int v0 = GPOINTER_TO_INT(GTS_OBJECT(p0)->reserved),
            v1 = GPOINTER_TO_INT(GTS_OBJECT(p1)->reserved);
        fprintf(s->gts_files[i], "%i %i\n", v0, v1);
    }
    GTS_OBJECT(e)->reserved = GINT_TO_POINTER(s->idx);
    s->idx++;
    return 0;
}

static gint
write_face(gpointer item, gpointer data)
{
    save_data *s = (save_data*)(data);
    GtsTriangle *t = GTS_TRIANGLE(item);
    GtsEdge *e1, *e2, *e3;
    GtsVertex *v1, *v2, *v3;
    gts_triangle_vertices_edges(t, 0, &v1, &v2, &v3, &e1, &e2, &e3);
    for(gint i = 0; i < s->surface_count; i++)
    {
        fprintf(s->gts_files[i], "%i %i %i\n",
            GPOINTER_TO_INT(GTS_OBJECT(e1)->reserved),
            GPOINTER_TO_INT(GTS_OBJECT(e2)->reserved),
            GPOINTER_TO_INT(GTS_OBJECT(e3)->reserved));
    }
    return 0;
}

static gint
vertex_surface_count(gpointer item, gpointer data)
{
    *(gint*)data = GTS_MVERTEX(item)->m_surfaces;
    return 1;
}

gint
multi_surface_count(GtsSurface* s)
{
    gint count = 0;
    gts_surface_foreach_vertex(s, vertex_surface_count, gpointer(&count));
    return count;
}

static gint
write_column(gpointer item, gpointer data)
{
    FILE *file = (FILE*)data;
    GtsMVertex *m = GTS_MVERTEX(item);
    for(int j = 0; j < mvertex_column_size(m); j++)
    {
        const gdouble *c = mvertex_position(m, j);
        fprintf(file, "%f %f %f ", c[0], c[1], c[2]);
    }
    fprintf(file, "\n");
    return 0;
}

void
multi_surface_load_columns(const char* filename, GtsSurface *m,
    GtsVertex **vertex_order)
{
    std::ifstream in(filename);
    gint vertex_count = gts_surface_vertex_number(m);
    for(int i = 0; i < vertex_count; i++)
    {
        GtsMVertex *m = GTS_MVERTEX(vertex_order[i]);
        std::vector<gdouble> column;
        std::string line;
        int j = 0, c;
        std::getline(in, line);
        gdouble x, y, z, d = std::numeric_limits<gdouble>::max();
        std::stringstream ss(line);
        while(ss >> x >> y >> z)
        {
            column.push_back(x);
            column.push_back(y);
            column.push_back(z);
            gdouble d0 = x-m->parent.p.x,
                    d1 = y-m->parent.p.y,
                    d2 = z-m->parent.p.z;
            gdouble ds = d0*d0+d1*d1+d2*d2;
            if(ds < d)
            {
                c = j;
                d = ds;
            }
            j++;
        }
        mvertex_column(m, &column[0], column.size()/3);
        multi_surface_index(m, 0, c);
    }
}

void
multi_surface_save_columns(const char* filename, GtsSurface *m,
    GtsVertex **vertex_order)
{
    FILE* file = fopen(filename, "w");
    gint vertex_count = gts_surface_vertex_number(m);
    if(vertex_order)
        for(int i = 0; i < vertex_count; i++)
            write_column(vertex_order[i], (gpointer)file);
    else gts_surface_foreach_vertex(m, write_column, (gpointer)file);
    fclose(file);
}

void
multi_surface_save_all(char** gts_filenames, GtsSurface *m,
    char *column_filename, char **min_marginal_filenames,
    GtsVertex **vertex_order, GtsEdge **edge_order, GtsFace **face_order)
{
    save_data s;
    s.surface_count = multi_surface_count(m);
    gint file_count = s.surface_count;
    s.gts_files = new FILE*[file_count];
    if(min_marginal_filenames) s.min_marginal_files = new FILE*[file_count];
    else s.min_marginal_files = 0;
    s.idx = 1;
    gint vertex_count = gts_surface_vertex_number(m),
         edge_count = gts_surface_edge_number(m),
         face_count = gts_surface_face_number(m);
    if(column_filename)
    {
        s.col_file = fopen(column_filename, "w");
        assert(s.col_file);
    }
    else s.col_file = 0;
    for(int i = 0; i < s.surface_count; i++)
    {
        s.gts_files[i] = fopen(gts_filenames[i], "w");
        assert(s.gts_files[i]);
        fprintf(s.gts_files[i], "%i %i %i\n", vertex_count, edge_count,
            face_count);
        if(min_marginal_filenames)
        {
            s.min_marginal_files[i] = fopen(min_marginal_filenames[i], "w");
            assert(s.min_marginal_files[i]);
        }
    }
    if(vertex_order)
        for(int i = 0; i < vertex_count; i++)
            write_vertex(vertex_order[i], (gpointer)&s);
    else gts_surface_foreach_vertex(m, write_vertex, (gpointer)&s);
    s.idx = 1;
    if(edge_order)
        for(int i = 0; i < edge_count; i++)
            write_edge(edge_order[i], (gpointer)&s);
    else gts_surface_foreach_edge(m, write_edge, (gpointer)&s);
    if(face_order)
        for(int i = 0; i < face_count; i++)
            write_face(face_order[i], (gpointer)&s);
    else gts_surface_foreach_face(m, write_face, (gpointer)&s);
    for(int i = 0; i < file_count; i++)
    {
        fclose(s.gts_files[i]);
        if(s.min_marginal_files) fclose(s.min_marginal_files[i]);
    }
    delete[] s.gts_files;
    if(s.min_marginal_files) delete[] s.min_marginal_files;
    if(s.col_file) fclose(s.col_file);
    gts_surface_foreach_vertex(m, (GtsFunc)gts_object_reset_reserved, NULL);
    gts_surface_foreach_edge(m, (GtsFunc)gts_object_reset_reserved, NULL);
}

/*void
multi_surface_load(char **filenames, GtsSurface *m, int surface_count)
{
    gint vertex_count, edge_count, face_count;
    FILE **pfiles = new FILE*[surface_count];
    for(int i = 0; i < surface_count; i++)
    {
        std::cout << filenames[i] << std::endl;
        pfiles[i] = fopen(filenames[i], "r");
        if(fscanf(pfiles[i], "%i %i %i\n", &vertex_count, &edge_count,
            &face_count) != 3)
        {
            fprintf(stderr, "ERROR: Header incorrect!\n");
        }
    }
    GtsMVertex **vertices = new GtsMVertex*[vertex_count];
    for(int i = 0; i < vertex_count; i++)
    {
        vertices[i] = gts_mvertex_new(gts_mvertex_class(), 0, 0, 0,
            surface_count);
        for(int j = 0; j < surface_count; j++)
        {
            if(fscanf(pfiles[j], "%lf %lf %lf\n",
                multi_position(vertices[i], j, 0),
                multi_position(vertices[i], j, 1),
                multi_position(vertices[i], j, 2)) != 3)
            {
                fprintf(stderr, "ERROR: Vertex incorrect!\n");
            }
        }
    }
    GtsEdge **edges = new GtsEdge*[edge_count];
    for(int i = 0; i < edge_count; i++)
    {
        int v0, v1;
        if(fscanf(pfiles[0], "%i %i\n", &v0, &v1) != 2)
        {
            fprintf(stderr, "ERROR: Edge incorrect!\n");
        }
        edges[i] = gts_edge_new(gts_edge_class(),
            GTS_VERTEX(vertices[v0-1]),
            GTS_VERTEX(vertices[v1-1]));
    }
    for(int i = 0; i < face_count; i++)
    {
        int e0, e1, e2;
        if(fscanf(pfiles[0], "%i %i %i\n", &e0, &e1, &e2) != 3)
        {
            fprintf(stderr, "ERROR: Face incorrect!\n");
        }
        gts_surface_add_face(m, gts_face_new(gts_face_class(), edges[e0-1],
            edges[e1-1], edges[e2-1]));
    }
    delete[] pfiles;
    delete[] vertices;
    delete[] edges;
}*/

void
multi_surface_save_tool(GtsSurface *surface, const char *output_prefix,
    bool output_columns, bool output_min_marginals, GtsVertex **vertices,
    GtsEdge **edges, GtsFace **faces)
{
    int surface_count = multi_surface_count(surface);
    char **gts_filenames = new char*[surface_count],
        **min_marginal_filenames = new char*[surface_count];
    std::string col_filename = std::string(output_prefix)+"columns.col";
    char *col_file = new char[col_filename.length()+1];
    strcpy(col_file, col_filename.c_str());
    for(int i = 0; i < surface_count; i++)
    {
        std::stringstream ss;
        ss << output_prefix << "surface";
        if(surface_count > 1) ss << i;
        std::string gts_filename = ss.str()+".gts",
            min_marginal_filename = ss.str()+".mm";
        gts_filenames[i] = new char[gts_filename.length()+1];
        strcpy(gts_filenames[i], gts_filename.c_str());
        min_marginal_filenames[i] =
            new char[min_marginal_filename.length()+1];
        strcpy(min_marginal_filenames[i], min_marginal_filename.c_str());
    }
    multi_surface_save_all(gts_filenames, surface, output_columns? col_file: 0,
        output_min_marginals? min_marginal_filenames: 0, vertices, edges,
        faces);
    delete[] col_file;
    for(int i = 0; i < surface_count; i++)
    {
        delete[] gts_filenames[i];
        delete[] min_marginal_filenames[i];
    }
    delete[] min_marginal_filenames;
}
