set(SOURCES image_trace.cpp opfront.cpp front.cpp multi_surface.cpp gts_boolean_plugin.cpp gts_surface_plugin.cpp graph_setup.cpp ../util/gts_conn.cpp ../util/gts_util.cpp ${MAXFLOW_INCLUDE_DIR}/graph.cpp ${MAXFLOW_INCLUDE_DIR}/maxflow.cpp)

if(VISUALIZE AND SDL_FOUND AND OPENGL_FOUND)
    list(APPEND SOURCES mainloop.cpp)
endif(VISUALIZE AND SDL_FOUND AND OPENGL_FOUND)

add_library(opfront ${SOURCES})

if(VISUALIZE)
    if(SDL_FOUND AND OPENGL_FOUND)
        target_include_directories(opfront PUBLIC ${SDL_INCLUDE_DIR} ${OPENGL_INCLUDE_DIR})
        target_link_libraries(opfront ${SDL_LIBRARY} ${OPENGL_LIBRARIES})
    else(SDL_FOUND AND OPENGL_FOUND)
        message("Cannot visualize because SDL and OpenGL was not found")
    endif(SDL_FOUND AND OPENGL_FOUND)
endif(VISUALIZE)

if(USE_FFTW) 
    if(FFTW_FOUND)
        target_include_directories(opfront PUBLIC ${FFTW_INCLUDES})
        target_link_libraries(opfront ${FFTW_LIBRARIES})
    else(FFTW_FOUND)
        message("Cannot use FFTW because FFTW library was not found")
    endif(FFTW_FOUND)
endif(USE_FFTW)

target_include_directories(opfront PUBLIC ${MAXFLOW_INCLUDE_DIR})
target_include_directories(opfront PUBLIC ${GTS_INCLUDE_DIR})
target_include_directories(opfront PUBLIC ${GLIB_INCLUDE_DIRS})

target_link_libraries(opfront ${CMAKE_THREAD_LIBS_INIT})
target_link_libraries(opfront ${Boost_LIBRARIES})
target_link_libraries(opfront ${GTS_LIBRARIES})
target_link_libraries(opfront ${CImg_SYSTEM_LIBS})

if(NiftiCLib_FOUND)
    target_link_libraries(opfront ${NiftiCLib_LIBRARIES})
endif(NiftiCLib_FOUND)

export(TARGETS opfront FILE opfront.cmake)

