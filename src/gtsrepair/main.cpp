/* Opfront - Optimal front segmentation
 * Copyright (c) 2014, Jens Petersen
 * All rights reserved.
 *
 * Opfront is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.

 * You should have received a copy of the GNU Lesser General Public License
 * along with this software.  If not, see <http://www.gnu.org/licenses/>.

 * Opfront has a range of dependencies and if you decide to use or
 * modify it, it should be done in compliance with what ever licenses and
 * copyright issues that apply for those dependencies.
 */
#include <iostream>
#include <boost/program_options.hpp>
#include "util/gts_repair.h"

namespace po = boost::program_options;

int
main(int ac, char* av[])
{
    try
    {
        int iterations;
        std::string input_gts_file, output_gts_file;
        po::options_description options_description("Allowed options");
        options_description.add_options()
            ("help,h", "produce help message")
            ("input_gts_file,i", po::value<std::string>(&input_gts_file),
                "the input gts file")
            ("output_gts_file,o", po::value<std::string>(&output_gts_file),
                "the output gts file")
            ("iterations,I", po::value<int>(&iterations)->default_value(10),
                "the number of iterations to try");
        po::variables_map vm;
        po::store(po::parse_command_line(ac, av, options_description), vm);
        po::notify(vm);
        if(vm.count("help") || input_gts_file == "" || output_gts_file == "")
        {
            std::cout << "GTS Repair" << std::endl;
            std::cout << std::endl;
            std::cout << "Usage: options_description [options]" << std::endl;
            std::cout << options_description;
            return 0;
        }
#ifdef OPFRONT_DEBUG
        std::cout << "Loading gts files: " << input_gts_file << std::endl;
        std::cout << "  " << output_gts_file << std::endl;
#endif
        GtsSurface *surface = gts_surface_new(gts_surface_class(),
            gts_face_class(), gts_edge_class(), gts_vertex_class());
        FILE *pfile = fopen(input_gts_file.c_str(), "r");
        GtsFile *fp = gts_file_new(pfile);
        int err = gts_surface_read(surface, fp);
        gts_file_destroy(fp);
        fclose(pfile);
#ifdef OPFRONT_DEBUG
        std::cout << "done!" << std::endl;
#endif
        gts_repair(surface, iterations, (GtsVertex* (*)(GtsEdge*,
            GtsVertexClass*))gts_segment_midvertex);
        
        gts_object_destroy(GTS_OBJECT(surface));
    }
    catch(std::exception& e)
    {
        std::cout << "Error: " << e.what() << std::endl;
        gts_finalize();
        return 1;
    }
    gts_finalize();
    return 0;
}

