/* Opfront - Optimal front segmentation
 * Copyright (c) 2014, Jens Petersen
 * All rights reserved.
 *
 * Opfront is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this software.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Opfront has a range of dependencies and if you decide to use or
 * modify it, it should be done in compliance with what ever licenses and
 * copyright issues that apply for those dependencies.
 */
#include <iostream>
#include <boost/program_options.hpp>
#include "cimgext/cimgext.h"
#include "opfront/opfront.h"
#include "opfront/thumbnail.h"
#include "opfront/opfront_init.h"
#include "opfront/multi_surface_meta.h"
#include "opfront/image_trace.h"
#include "util/gts_util.h"
#include "util/cimg_util.h"
#include "util/lalg.h"
#include "util/statistics.h"

#ifdef VISUALIZATION
#include "opfront/mainloop.h"
#endif

namespace po = boost::program_options;

static const int surface_count = 2;

opfront *segmentor;
cimg_library::CImg<ofloat> volume, lumen_probability, wall_probability;
cimg_library::CImg<unsigned char> mask;

int inner_smoothness_constraint, outer_smoothness_constraint;
ofloat inner_smoothness_penalty, outer_smoothness_penalty, separation_penalty,
    seed_radius, weight, inner_derivative_weight, outer_derivative_weight,
    flow_line_sample_interval, finite_difference_delta;

int
edge_offset(int surface_i, int surface_j, int column_i, int column_j,
    void *params)
{
    // No preference for any specific separation.
    return 0;
}

void
edge_constraint(int surface_i, int surface_j, int column_i, int column_j,
    int &minus_delta, int &plus_delta, void *params)
{
    static const int big_number = 10000;
    // Separation constraints.
    if(surface_i != surface_j && column_i == column_j)
    {
        // We are only doing ordered surfaces right now.
        plus_delta = big_number;
        minus_delta = 0;
    }
    // Smoothness constraints.
    else if(surface_i == surface_j && column_i != column_j)
    {
        if(surface_i)
        {
            plus_delta = outer_smoothness_constraint;
            minus_delta = -outer_smoothness_constraint;
        }
        else
        {
            plus_delta = inner_smoothness_constraint;
            minus_delta = -inner_smoothness_constraint;
        }
    }
    else assert(0);
}

ofloat
edge_cost(int surface_i, int surface_j, int column_i, int column_j,
    double delta, void *params)
{
    // We are using only linear costs, so signal this (return 1 if not).
    if(delta < 0) return -1;

    if(surface_i == surface_j && column_i != column_j)
    {
        // Smoothness penalties.
        if(surface_i) return delta*outer_smoothness_penalty;
        else return delta*inner_smoothness_penalty;
    }
    else if(surface_i != surface_j && column_i == column_j)
    {
        // Separation penalties.
        return delta*separation_penalty;
        //    fabs(delta-opfront_separation_estimation[column_i]);
    }
    // We haven't added such edges, so we shouldn't get this callback.
    else assert(0);
    return 0;
}

ofloat
edge_cost(int surface_i, int surface_j, int column_i, int column_j, int delta,
    void *params)
{
    return edge_cost(surface_i, surface_j, column_i, column_j,
        static_cast<double>(delta), params);
}

// Column value
//   Return the value of the given image at distance d from node_index along
//   the column. If column ends are reached, we simply return the value at
//   these positions.
template<class CIMG_TYPE>
ofloat
column_value(cimg_library::CImg<CIMG_TYPE> &img, int column_index,
    double node_index, ofloat d)
{
    int l = segmentor->column_length(column_index);
    ofloat p[3], n[3], pn[3];
    segmentor->column_position(column_index, node_index, p);
    if(d > 0)
    {
        while(node_index < l-1)
        {
            segmentor->column_position(column_index, node_index+1, n);
            if(d < flow_line_sample_interval)
            {
                linalg_sub_ve3_ve3(n, p, pn);
                linalg_mul_scl_ve3(d/flow_line_sample_interval, pn);
                linalg_add_ve3_ve3(pn, p);
                break;
            }
            d -= flow_line_sample_interval;
            node_index++;
            linalg_cop_ve3(n, p);
        }
    }
    else
    {
        d *= -1;
        while(node_index > 0)
        {
            segmentor->column_position(column_index, node_index-1, n);
            if(d < flow_line_sample_interval)
            {
                linalg_sub_ve3_ve3(n, p, pn);
                linalg_mul_scl_ve3(d/flow_line_sample_interval, pn);
                linalg_add_ve3_ve3(pn, p);
                break;
            }
            d -= flow_line_sample_interval;
            node_index--;
            linalg_cop_ve3(n, p);
        }
    }
    img.xyz2ijk(p);
    return img.cubic_atXYZ(p[0], p[1], p[2]);
}


ofloat
dx(int column_index, int node_index, ofloat d)
{
    ofloat
        v0 = -column_value(volume, column_index, node_index,
                           d-finite_difference_delta)/2,
        v1 = column_value(volume, column_index, node_index, 
                          d+finite_difference_delta)/2;
    return (v0+v1)/finite_difference_delta;
}

ofloat
p(int column_index, int node_index, ofloat d)
{
    ofloat r = dx(column_index, node_index, d);
    if(r > 0) return r;
    else return 0;
}

ofloat
n(int column_index, int node_index, ofloat d)
{
    ofloat r = dx(column_index, node_index, d);
    if(r < 0) return r;
    else return 0;
}

ofloat
dxp(int column_index, int node_index, ofloat d)
{
    return (p(column_index, node_index, d+finite_difference_delta)-
            p(column_index, node_index, d-finite_difference_delta))/
            (2*finite_difference_delta);
}

ofloat
dxn(int column_index, int node_index, ofloat d)
{
    return (n(column_index, node_index, d+finite_difference_delta)-
            n(column_index, node_index, d-finite_difference_delta))/
            (2*finite_difference_delta);
}

// Inner cost
//  The inner cost function of the column at index, using specified
//  derivative weightings.
ofloat
inner_cost(int column_index, double node_index)
{
    int idx = std::floor(node_index);
    double d = node_index-idx;
    return -((1-fabs(inner_derivative_weight))*
        p(column_index, idx, d+flow_line_sample_interval/2)+
        inner_derivative_weight*
        dxp(column_index, idx, d+flow_line_sample_interval/2));
}

// Outer cost
//  The outer cost function of the column at index, using specified
//  derivative weightings.
ofloat
outer_cost(int column_index, double node_index)
{
    int idx = std::floor(node_index);
    double d = node_index-idx;
    return -((outer_derivative_weight)*
        dxn(column_index, idx, d+flow_line_sample_interval/2)-
        (1-fabs(outer_derivative_weight))*
        n(column_index, idx, d+flow_line_sample_interval/2));
}

ofloat
surface_cost(int surface, int column_index, double node_index, void *params)
{
    assert(surface >= 0);
    assert(column_index >= 0);
    assert(column_index < segmentor->column_count());
    if(segmentor->column_length(column_index) == 1) return 0;
    //std::cout << surface << " " << column_index << " " << node_index << " "
    //    << weight*inner_cost(column_index, node_index) << " "
    //    << weight*outer_cost(column_index, node_index) << std::endl;
    if(!surface) return weight*inner_cost(column_index, node_index);
    else return weight*outer_cost(column_index, node_index);
}

ofloat
surface_cost(int surface, int column_index, int node_index, void *params)
{
    return surface_cost(surface, column_index, static_cast<double>(node_index),
        params);
}

ofloat
region_cost(int region, int column_index, double node_index, void *params)
{
    // Make sure we return early if probability files were not supplied.
    if(weight == 1) return 0;

    ofloat l = column_value(lumen_probability, column_index,
        std::floor(node_index), node_index-std::floor(node_index));
    ofloat w = column_value(wall_probability, column_index,
        std::floor(node_index), node_index-std::floor(node_index));
    ofloat p = 1-l-w;
    switch(region)
    {
    case 0:
        return (1-weight)*(p+w-l);
    case 1:
        return (1-weight)*(p+l-w);
    default: return (1-weight)*(l+w-p);
    }
}

ofloat
region_cost(int region, int column_index, int node_index, void *params)
{
    return region_cost(region, column_index, static_cast<double>(node_index),
        params);
}

static bool
mask_vertex(GtsMVertex *m, void *params)
{
    if(!mask.width()) return 1;
    GtsPoint *p = GTS_POINT(m);
    ofloat x = p->x, y = p->y, z = p->z;
    mask.xyz2ijk(x, y, z);
    return mask.atXYZ(static_cast<int>(x+0.5), static_cast<int>(y+0.5),
        static_cast<int>(z+0.5), 0, 0);
}

void
error_reporter(const char *msg)
{
    std::cout << msg << std::endl;
}

double
regional_cost_interval(int column_index, double lower_bound,
    double upper_bound, int region)
{
    //std::cout << "Lb: " << lower_bound << " ub: " << upper_bound << " region: "
    //    << region << std::endl;
    double r = 0;
    for(int i = round(lower_bound); i <= round(upper_bound); i++)
    {
        double f = std::min(i+0.5, upper_bound)-std::max(i-0.5, lower_bound);
        //std::cout << "  node: " << i << " interval: "
        //    << std::max(i-0.5, lower_bound) << "-"
        //    << std::min(i+0.5, upper_bound) << " cost: "
        //    <<  region_cost(region, column_index, i, 0)
        //    << " fraction: " << f << std::endl;
        r += region_cost(region, column_index, i, 0)*f;
    }
    return r;
}

void
compute_energy(int sub_iteration, void *params)
{
#ifdef OPFRONT_PRINT_CUT_TIME
    static int *offsets;
    // If columns have been generated then store neighbour offsets.
    if(sub_iteration == 2)
    {
        //std::cout << "Storing offsets" << std::endl;
        offsets = new int[segmentor->edge_count()];
        for(int i = 0; i < segmentor->edge_count(); i++)
        {
            int c0, c1;
            segmentor->edge(i, c0, c1);
            double l0 = segmentor->label(0, c0),
                   l1 = segmentor->label(0, c1);
            offsets[i] = l0-l1;
        }
    }
    // If solution has been computed, then output energy and cleanup.
    if(sub_iteration != 1 || segmentor->column_count() == 0) return;
    double e = 0;
    for(int j = 0; j < segmentor->column_count(); j++)
    {
        //double k = 0;
        for(int i = 0; i < segmentor->surface_count(); i++)
        {
            double l = segmentor->min_marginal_label(i, j);
            e += surface_cost(i, j, l, 0);
        }
        //std::cout << std::endl;
        double l, lb = -0.5;
        for(int i = 0; i < segmentor->surface_count(); i++)
        {
            l = segmentor->min_marginal_label(i, j);
            //std::cout << "surface: " << i << " label: " << l << std::endl;
            e += regional_cost_interval(j, lb, l, i);
            lb = l;
        }
        e += regional_cost_interval(j, l, segmentor->column_length(j)+0.5,
            segmentor->surface_count());
        //std::cout << std::endl;
    }
    std::vector<double> dv, pv0, pv1;
    double s = 0;
    for(int i = 0; i < segmentor->edge_count(); i++)
    {
        //int e = segmentor->edge(i);
        int c0, c1;
        segmentor->edge(i, c0, c1);
        for(int j = 0; j < segmentor->surface_count(); j++)
        {
            double l0 = segmentor->min_marginal_label(j, c0),
                   l1 = segmentor->min_marginal_label(j, c1),
                   d0 = segmentor->label(j, c0),
                   d1 = segmentor->label(j, c1);
            double deltal = fabs(l0-l1-offsets[i]),
                   deltad = fabs(d0-d1-offsets[i]);
            assert(fabs(deltal-deltad) < 0.51);
            if(deltad < deltal)
            {
                std::cout << d0 << " (" << segmentor->column_length(c0) << ") "
                    << d1 << "(" << segmentor->column_length(c1) << ") "
                    << " - " << l0 << " " << l1 <<
                    std::endl;
                pv0.push_back(d0);
                pv0.push_back(d1);
            }
            else
            {
                pv1.push_back(d0);
                pv1.push_back(d1);
            }
            //if(delta) std::cout << j << " " << l0 << " " << l1 << " " <<
            //    offsets[i] << " " << delta
            //    << std::endl;
            //e += edge_cost(j, j, c0, c1, delta, 0);
            s += deltal;
            dv.push_back(deltal);
        }
    }
    std::cout << "Delta: " << statistics::summary(dv) << std::endl;
    std::cout << "Label worsening: " << statistics::summary(pv0) << std::endl;
    std::cout << "Label improvement: " << statistics::summary(pv1) << std::endl;
    for(int i = 0; i < segmentor->column_count(); i++)
    {
        double l0 = segmentor->min_marginal_label(0, i),
               l1 = segmentor->min_marginal_label(1, i);
        //e += edge_cost(0, 1, i, i, fabs(l0-l1), 0);
    }
    std::cout << "Solution energy: " << e << std::endl;

    delete[] offsets;
#endif
    return;
}


int
main(int ac, char* av[])
{
    try
    {
        char thumbnail_axis;
        int front_edge_links, maximum_front_size, max_iterations,
            flow_line_type, flow_line_kernel_size, compute_min_marginals;
        ofloat sphx, sphy, sphz, sphr, flow_line_kernel_spacing,
            flow_line_regularization, maximum_edge_length, flow_line_max_inner,
            flow_line_max_outer;
        std::string volume_file, lumen_probability_file, wall_probability_file,
            segmentation_file, mask_file, output_prefix;
        po::options_description options_description("Allowed options");
        options_description.add_options()
            ("help,h", "produce help message")
            ("sphere_x,x", po::value<ofloat>(&sphx)->default_value(-1),
                "seed sphere center x coordinate")
            ("sphere_y,y", po::value<ofloat>(&sphy)->default_value(-1),
                "seed sphere center y coordinate")
            ("sphere_z,z", po::value<ofloat>(&sphz)->default_value(-1),
                "seed sphere center z coordinate")
            ("sphere_radius,g",
                po::value<ofloat>(&sphr)->default_value(5),
                "seed sphere radius")
            ("volume_file,v",
                po::value<std::string>(&volume_file),
                "the volume file")
            ("lumen_probability_file,L",
                po::value<std::string>(&lumen_probability_file),
                "the lumen probability file")
            ("wall_probability_file,W",
                po::value<std::string>(&wall_probability_file),
                "the wall probability file")
            ("segmentation_file,s",
                po::value<std::string>(&segmentation_file),
                "(optional) the initial segmentation file")
            ("mask,m",
                po::value<std::string>(&mask_file),
                "(optional) only segment within positive mask values")
            ("(optional) output_prefix,p",
                po::value<std::string>(&output_prefix),
                "output segmentation files using this prefix")
            ("inner_smoothness_penalty,i",
                po::value<ofloat>(&inner_smoothness_penalty)->default_value(1),
                "smoothness penalty of the inner surface")
            ("outer_smoothness_penalty,o",
                po::value<ofloat>(&outer_smoothness_penalty)->default_value(1),
                "smoothness penalty of the outer surface")
            ("inner_smoothness_constraint,I",
                po::value<int>(&inner_smoothness_constraint)->
                default_value(-1),
                "smoothness constraint of the inner surface")
            ("outer_smoothness_constraint,O",
                po::value<int>(&outer_smoothness_constraint)->
                default_value(-1),
                "smoothness constraint of the outer surface")
            ("separation_penalty,d",
                po::value<ofloat>(&separation_penalty)->default_value(0),
                "surface separation penalty")
            ("weight,w",
                po::value<ofloat>(&weight)->default_value(0.5),
                "weight between surface (1) and region (0) cost")
            ("inner_derivative_weight,F",
                po::value<ofloat>(&inner_derivative_weight)->default_value(0),
                "inner surface term derivative weight")
            ("outer_derivative_weight,G",
                po::value<ofloat>(&outer_derivative_weight)->default_value(0),
                "outer surface term derivative weight")
            ("front_edge_links,l",
                po::value<int>(&front_edge_links)->default_value(20),
                "the recomputed boundary around moving vertices")
            ("maximum_front_size,a",
                po::value<int>(&maximum_front_size)->default_value(-1),
                "limit front sizes, by splitting fronts (for memory reasons)")
            ("flow_line_sample_interval,b",
                po::value<ofloat>(&flow_line_sample_interval)->
                default_value(1), "Flow line sampling interval")
            ("flow_line_type,t",
                po::value<int>(&flow_line_type)->default_value(0),
                "0 - Gauss, 1 - Canny-Deriche, 2 - ELF, 3 - Distance")
            ("flow_line_kernel_spacing,k",
                po::value<ofloat>(&flow_line_kernel_spacing)->default_value(1),
                "the convolution grid spacing")
            ("flow_line_regularization,r",
                po::value<ofloat>(&flow_line_regularization)->default_value(2),
                "the regularization used when computing flow lines")
            ("flow_line_kernel_size,c",
                po::value<int>(&flow_line_kernel_size)->default_value(21),
                "the convolution kernel size, should be odd positive number")
            ("flow_line_max_inner,M",
                po::value<ofloat>(&flow_line_max_inner)->default_value(-1),
                "the maximum inner flow line length (mm)")
            ("flow_line_max_outer,N",
                po::value<ofloat>(&flow_line_max_outer)->default_value(-1),
                "the maximum exterior flow line length (mm)")
            ("mesh_edge_length,e",
                po::value<ofloat>(&maximum_edge_length)->
                default_value(1), "the triangle mesh maximum edge length")
            ("thumbnail_axis,n",
                po::value<char>(&thumbnail_axis)->default_value(' '),
                "(optional) output projected 2D images along 'x', 'y' or 'z'")
            ("maximum_iterations,K",
                po::value<int>(&max_iterations)->default_value(-1),
                "(optional) stop after this amount of iterations")
            ("min_marginals,C",
                po::value<int>(&compute_min_marginals)->default_value(0),
                "0 - no min marginals, 1 = 0/1, 2 = band, 3 = full")
            ("finite_difference_delta,f",
                po::value<ofloat>(&finite_difference_delta)->default_value(1),
                "finite difference delta to use");
        po::variables_map vm;
        po::store(po::parse_command_line(ac, av, options_description), vm);
        po::notify(vm);
        if(vm.count("help"))
        {
            std::cout << "Optimal front wall segmentation" << std::endl;
            std::cout << std::endl;
            std::cout << "Usage: options_description [options]" << std::endl;
            std::cout << options_description;
            return 0;
        }
        if((sphx == -1 || sphy == -1 || sphz == -1) && segmentation_file == "")
        {
            std::cout << "Error: please specify seed point or initial "
                      << "segmentation" << std::endl;
            return 1;
        }
        if(volume_file == "")
        {
            std::cout << "Error: please specify volume" << std::endl;
            return 1;
        }
        else
        {
#ifdef OPFRONT_DEBUG
            std::cout << "Loading volume... " << std::flush;
#endif
            volume.load(volume_file.c_str());
#ifdef OPFRONT_DEBUG
            std::cout << "spacing: " << volume.metadata.spacing[0]
                      << "x" << volume.metadata.spacing[1]
                      << "x" << volume.metadata.spacing[2]
                      << " origin: (" << volume.metadata.origin[0]
                      << "," << volume.metadata.origin[1]
                      << "," << volume.metadata.origin[2]
                      << ") done!" << std::endl;
#endif
        }
        if(lumen_probability_file != "" && wall_probability_file != "")
        {
#ifdef OPFRONT_DEBUG
            std::cout << "Loading probability files... " << std::flush;
#endif
            lumen_probability.load(lumen_probability_file.c_str());
            wall_probability.load(wall_probability_file.c_str());
#ifdef OPFRONT_DEBUG
            std::cout << "done!" << std::endl;
#endif
        } else weight = 1;
        GtsSurface *surface;
        if(segmentation_file == "")
            surface = opfront_init_sphere(volume, sphx, sphy, sphz, sphr,
                surface_count);
        else
        {
            surface = opfront_init_gts(volume, segmentation_file,
                surface_count);
#ifdef OPFRONT_DEBUG
            std::cout << "Loaded vertices: "
                << gts_surface_vertex_number(surface)
                << " edges: " << gts_surface_edge_number(surface)
                << " faces: " << gts_surface_face_number(surface) << std::endl;
#endif
        }

        if(mask_file != "") mask.load(mask_file.c_str());
        image_tracer_type image_tracer(flow_line_regularization,
            flow_line_sample_interval, flow_line_kernel_spacing,
            flow_line_max_inner, flow_line_max_outer,
            flow_line_type, flow_line_kernel_size);
        segmentor = new opfront(surface, maximum_edge_length, edge_offset, 0,
            edge_constraint, 0, edge_cost, 0, surface_cost, 0, region_cost, 0,
            mask.width()? mask_vertex: 0, 0, compute_min_marginals,
            compute_energy, 0, error_reporter);
#ifdef VISUALIZATION
        std::cout << "Visualizing!" << std::endl;
        main_loop(segmentor, &image_tracer, surface, output_prefix,
            front_edge_links, maximum_front_size);
#else
#ifdef OPFRONT_DEBUG
        std::cout << "Iterating..." << std::endl;
#endif
        int i = 0;
        do
        {
            std::stringstream ss;
            ss << output_prefix << "iteration" << i++ << ".bmp";
            create_thumbnail(thumbnail_axis, ss.str().c_str(), volume,
                    surface);
        }
        while(segmentor->complete_step(image_tracer, front_edge_links,
            maximum_front_size) && max_iterations--);
#endif
#ifdef OPFRONT_DEBUG
        std::cout << "Done!" << std::endl;
        std::cout << "Saving... " << std::flush;
#endif
        multi_surface_meta_xyz2ijk(volume, surface);
        multi_surface_save_tool(surface, output_prefix.c_str(),
            compute_min_marginals > 0, compute_min_marginals > 0);
#ifdef OPFRONT_DEBUG
        std::cout << "done!" << std::endl;
#endif
        delete segmentor;
        gts_object_destroy(GTS_OBJECT(surface));
    }
    catch(std::exception& e)
    {
        std::cout << "Error: " << e.what() << std::endl;
        gts_finalize();
        return 1;
    }
    gts_finalize();
    return 0;
}

