/* Opfront - Optimal front segmentation
 * Copyright (c) 2014, Jens Petersen
 * All rights reserved.
 *
 * Opfront is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this software.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Opfront has a range of dependencies and if you decide to use or
 * modify it, it should be done in compliance with what ever licenses and
 * copyright issues that apply for those dependencies.
 */
#include <iostream>
#include <fstream>
#include <vector>
#include <boost/program_options.hpp>
#include "cimgext/cimgext.h"
#include "util/colio.h"

namespace po = boost::program_options;

int
main(int ac, char* av[])
{
    try
    {
        std::string probability_file, column_file, image_file;
        po::options_description options_description("Allowed options");
        options_description.add_options()
            ("help,h", "produce help message")
            ("image_file,i",
                po::value<std::string>(&image_file)->required(),
                "input image file with probabilities")
            ("column_file,c",
                po::value<std::string>(&column_file)->required(),
                "input file with column locations")
            ("probability_file,s",
                po::value<std::string>(&probability_file)->required(),
                "output file with column probabilities");
        po::variables_map vm;
        po::store(po::parse_command_line(ac, av, options_description), vm);
        if(vm.count("help"))
        {
            std::cout << "Generate example column probabilities"
                      << std::endl;
            std::cout << "Usage: options_description [options]" << std::endl;
            std::cout << options_description;
            return 0;
        }
        po::notify(vm);
        cimg_library::CImg<double> img(image_file.c_str());
        std::vector<std::vector<double> > columns;
        load_col_file(column_file.c_str(), columns);
        std::ofstream probabilities(probability_file.c_str());
        for(int i = 0; i < columns.size(); i++)
        {
            std::vector<double> &column = columns[i];
            for(int j = 0; j < column.size(); j+=3)
            {
                probabilities << img.cubic_atXYZ(column[j], column[j+1],
                    column[j+2]);
                if(j+3 < column.size()) probabilities << " ";
                else probabilities << std::endl;
            }
        }
    }
    catch(std::exception& e)
    {
        std::cout << "Error: " << e.what() << std::endl;
        return 1;
    }
    return 0;
}

