/* Opfront - Optimal front segmentation
 * Copyright (c) 2014, Jens Petersen
 * All rights reserved.
 *
 * Opfront is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this software.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Opfront has a range of dependencies and if you decide to use or
 * modify it, it should be done in compliance with what ever licenses and
 * copyright issues that apply for those dependencies.
 */
#include <iostream>
#include <fstream>
#include <vector>
#include <boost/program_options.hpp>
#include <boost/timer/timer.hpp>
#include "cimgext/cimgext.h"
#include "opfront/defines.h"
#include "optisurf/mgraph.h"
#include "opfront/graph_setup.h"
#include "opfront/multi_surface_meta.h"
#include "util/gts_surface.h"
#include "util/colio.h"

namespace po = boost::program_options;

class surface_type
{
public:
    int smoothness_constraint;
    ofloat smoothness_penalty;
    std::vector<std::vector<ofloat> >
        surface_probabilities,
        region_probabilities;
};

ofloat weight;
std::vector<surface_type> surfaces;

int
edge_offset(int surface_i, int surface_j, int column_i, int column_j,
    void *params)
{
    // No preference for any specific separation.
    return 0;
}

void
edge_constraint(int surface_i, int surface_j, int column_i, int column_j,
    int &minus_delta, int &plus_delta, void *params)
{
    if(surface_i != surface_j && column_i == column_j)
    {
        // We are only doing ordered surfaces right now.
        plus_delta = std::numeric_limits<int>::max();
        minus_delta = 0;
    }
    // Smoothness constraints.
    else if(surface_i == surface_j && column_i != column_j)
    {
        plus_delta = surfaces[surface_i].smoothness_constraint;
        minus_delta = -surfaces[surface_i].smoothness_constraint;
    }
    // We haven't added such edges, so we shouldn't get this callback.
    else assert(0);
    return;
}

ofloat
edge_cost(int surface_i, int surface_j, int column_i, int column_j, int delta,
    void *params)
{
    // We are using only linear costs, so signal this (return 1 if not).
    if(delta < 0) return -1;
    if(surface_i == surface_j && column_i != column_j)
    {
        // Smoothness penalties.
        return delta*surfaces[surface_i].smoothness_penalty;
    }
    else if(surface_i != surface_j && column_i == column_j)
    {
        // No separation penalty.
        return 0;
    }
    // We haven't added such edges, so we shouldn't get this callback.
    else assert(0);
    return 0;
}

ofloat
surface_cost(int surface, int column_index, int node_index, void *params)
{
    if(surfaces[surface].surface_probabilities.size())
    {
        return weight*(1-
            surfaces[surface].surface_probabilities[column_index][node_index]);
    } else return 0;
}

ofloat
region_cost(int region, int column_index, int node_index, void *params)
{
    // There is one more region than there are surfaces.
    if(region == surfaces.size() &&
       surfaces[region-1].region_probabilities.size())
    {
        double r = 0;
        for(int i = 0; i < surfaces.size(); i++)
            r += surfaces[i].region_probabilities[column_index][node_index];
        return (1-weight)*r;
    }
    else if(surfaces[region].region_probabilities.size())
    {
        return (1-weight)*(1-
            surfaces[region].region_probabilities[column_index][node_index]);
    } else return 0;
}

int
main(int ac, char* av[])
{
    try
    {
        int compute_min_marginals;
        std::vector<int> smoothness_constraints;
        std::vector<ofloat> smoothness_penalties;
        std::vector<std::string> region_probability_files,
            surface_probability_files;
        std::string mesh_file, column_file, output_prefix;
        po::options_description options_description("Allowed options");
        options_description.add_options()
            ("help,h", "produce help message")
            ("mesh_file,e",
                po::value<std::string>(&mesh_file),
                "mesh file (gts format)")
            ("column_file,c",
                po::value<std::string>(&column_file),
                "column file")
            ("surface_probability_files,s",
                po::value<std::vector<std::string> >(
                    &surface_probability_files)->multitoken(),
                "surface probability files")
            ("region_probability_files,r",
                po::value<std::vector<std::string> >(&region_probability_files)
                    ->multitoken(),
                "region probability files")
            ("smoothness_penalties,i",
                po::value<std::vector<ofloat> >(&smoothness_penalties),
                "smoothness penalties of the surfaces (def: 1)")
            ("smoothness_constraints,I",
                po::value<std::vector<int> >(&smoothness_constraints),
                "smoothness constraints of the surfaces (def: -1)")
            ("weight,w",
                po::value<ofloat>(&weight)->default_value(0.5),
                "weight between surface (1) and region (0) cost (def: 0.5)")
            ("min_marginals,C",
                po::value<int>(&compute_min_marginals)->default_value(0),
                (std::string("0 - no min marginals, 1 = 0/1, ")+
                 std::string("2 = banded binary, 3 = full binary, ")+
                 std::string("4 = banded n-ary, 5 = full n-ary")).c_str())
            ("output_prefix,p",
                po::value<std::string>(&output_prefix),
                "output files using this prefix");
        po::variables_map vm;
        po::store(po::parse_command_line(ac, av, options_description), vm);
        po::notify(vm);
        if(vm.count("help"))
        {
            std::cout << "Optimal surface segmentation"
                      << std::endl;
            std::cout << "Usage: options_description [options]" << std::endl;
            std::cout << options_description;
            return 0;
        }
        int surface_count = std::max(surface_probability_files.size(),
            region_probability_files.size());
        surfaces.assign(surface_count, surface_type());
        if(!surface_count)
        {
            std::cerr << "Error: please provide at least one probability file"
                << std::endl;
            return 1;
        }
#ifdef OPFRONT_DEBUG
        std::cout << "Assuming " << surface_count << " surface(s) are sought"
            << std::endl;
#endif
        if(region_probability_files.size())
        {   
            if(region_probability_files.size() != surface_count)
            {
                std::cerr << "Error: expected 0 or " << surface_count
                    << " region probability file(s)" << std::endl;
                return 1;
            }
            for(int i = 0; i < surface_count; i++)
                load_col_file(region_probability_files[i],
                    surfaces[i].region_probabilities);
            // Set default weight to region = 1, surface = 0.
            weight = 0;
        }
        if(surface_probability_files.size())
        {
            if(surface_probability_files.size() != surface_count)
            {
                std::cerr << "Error: expected 0 or " << surface_count
                    << " surface probability file(s)" << std::endl;
                return 1;
            }
            for(int i = 0; i < surface_count; i++)
                load_col_file(surface_probability_files[i],
                    surfaces[i].surface_probabilities);
            // Set default weight to region = 0, surface = 1.
            weight = 1;
        }
        if(smoothness_penalties.size())
        {
            if(smoothness_penalties.size() != surface_count)
            {
                std::cerr << "Error: expected 0 or " << surface_count
                    << " smoothness penaltie(s)" << std::endl;
                return 1;
            }
            for(int i = 0; i < surface_count; i++)
                surfaces[i].smoothness_penalty = smoothness_penalties[i];
        }
        else
        {
            for(int i = 0; i < surface_count; i++)
                surfaces[i].smoothness_penalty = 0;
        }
        if(smoothness_constraints.size())
        {
            if(smoothness_constraints.size() != surface_count)
            {
                std::cerr << "Error: expected 0 or " << surface_count
                    << " smoothness constraint(s)" << std::endl;
                return 1;
            }
            for(int i = 0; i < surface_count; i++)
                surfaces[i].smoothness_constraint = smoothness_constraints[i];
        }
        else
        {
            for(int i = 0; i < surface_count; i++)
                surfaces[i].smoothness_constraint = -1;
        }
        GtsSurface *surface = gts_surface_new(gts_surface_class(),
            gts_face_class(), gts_edge_class(),
            GTS_VERTEX_CLASS(gts_mvertex_class()));
        FILE *pfile = fopen(mesh_file.c_str(), "r");
        GtsFile *fp = gts_file_new(pfile);
        GtsVertex **vertices;
        GtsEdge **edges;
        GtsFace **faces;
        int err = gts_surface_read_ext(surface, fp, &vertices, &edges, &faces);
        gts_file_destroy(fp);
        fclose(pfile);
        multi_surface(surface, surface_count);
        multi_surface_select(surface, 0);
        multi_surface_load_columns(column_file.c_str(), surface, vertices);
        multi_graph<ofloat> graph(surface_count,
            gts_surface_vertex_number(surface), edge_cost, 0, 0, 0,
            region_cost, 0); 
        // Processing the complete graph in one go so set front indices.
        for(int i = 0; i < gts_surface_vertex_number(surface); i++)
            GTS_MVERTEX(vertices[i])->index = i;

        graph_setup(surface_count, reinterpret_cast<GtsMVertex**>(vertices),
            gts_surface_vertex_number(surface), edges,
            gts_surface_edge_number(surface), graph, edge_offset, 0,
            edge_constraint, 0, surface_cost, 0);
#ifdef OPFRONT_PRINT_CUT_TIME
        boost::timer::cpu_timer timer;
#endif
        graph.calculate();
        if(compute_min_marginals)
            graph_update_surface_with_min_marginals(surface_count,
                reinterpret_cast<GtsMVertex**>(vertices),
                gts_surface_vertex_number(surface), graph,
                compute_min_marginals, 0, 0);
        else graph_update_surface(surface_count,
            reinterpret_cast<GtsMVertex**>(vertices),
            gts_surface_vertex_number(surface), graph, 0, 0);
#ifdef OPFRONT_PRINT_CUT_TIME
        std::cout << "Minimum cut time: " << timer.format() << std::endl;
#endif
        multi_surface_save_tool(surface, output_prefix.c_str(), false,
            compute_min_marginals>0, vertices, edges, faces); 
        gts_object_destroy(GTS_OBJECT(surface));
    }
    catch(std::exception& e)
    {
        std::cout << "Error: " << e.what() << std::endl;
        gts_finalize();
        return 1;
    }
    gts_finalize();
    return 0;
}

