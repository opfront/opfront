/* Opfront - Optimal front segmentation
 * Copyright (c) 2014, Jens Petersen
 * All rights reserved.
 *
 * Opfront is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this software.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Opfront has a range of dependencies and if you decide to use or
 * modify it, it should be done in compliance with what ever licenses and
 * copyright issues that apply for those dependencies.
 */
#ifndef __IGRAPH_H__
#define __IGRAPH_H__

#include "optisurf/bgraph.h"

// Optimal net graph
//  Minimum cut graph constructed according to the method described in:
//  "Optimal Net Surface Problems with Applications, Wy, X., Chen, D.".
template<class CAPACITY_TYPE>
class optimal_net_graph : public base_graph<CAPACITY_TYPE>
{
private:
    typedef base_graph<CAPACITY_TYPE> b;
    typedef typename b::column_type column_type;
    typedef typename column_type::nit nit;

    virtual void
    construct_specific()
    {
        for(int i = 0; i < b::column_count; i++)
        {
            column_type& c = b::columns[i];
            // Intra column edges.
            for(int j = 0; j < c.get_length()-1; j++)
            {
                b::graph.add_edge(c.get_node_handle(j+1), c.get_node_handle(j),
                    b::infinite_capacity, 0);
#ifdef IGRAPH_DEBUG
                std::cout << "(" << i << "_" << j << "<->"
                          << i << "_" << j+1 << "): "
                          << 0 << " " << b::infinite_capacity << std::endl;
#endif
                b::edge_count++;
            }
            for(int j = 0; j < c.get_length(); j++)
            {
                CAPACITY_TYPE w;
                if(j > 0)
                {
                    w = c.get_node(j)-c.get_node(j-1);
                    if(b::region_cost)
                        w += b::region_cost(i, j, b::region_cost_params);
                }
                else w = -b::infinite_capacity;
                if(w >= 0)
                {
                    b::graph.add_tweights(c.get_node_handle(j), 0, w);
#ifdef IGRAPH_DEBUG
                    std::cout << "(" << i << "_" << j << "->t): "
                              << w << std::endl;
#endif
                }
                else
                {
                    b::graph.add_tweights(c.get_node_handle(j), -w, 0);
#ifdef IGRAPH_DEBUG
                    std::cout << "(s->" << i << "_" << j << "): "
                              << -w << std::endl;
#endif
                }
                b::edge_count++;
            }
        }
    }

public:
    optimal_net_graph(int _column_count,
        CAPACITY_TYPE (*_edge_cost)(int, int, int, void*) = 0,
        void *_params = 0,
        CAPACITY_TYPE (*_edge_cost_second_order)(int, int, int, void*) = 0,
        void *_second_order_params = 0,
        CAPACITY_TYPE (*_region_cost)(int, int, void*) = 0,
        void *_region_cost_params = 0,
        void (_error_function)(const char*) = 0):
        b(_column_count, _edge_cost, _params, _edge_cost_second_order,
          _second_order_params, _region_cost, _region_cost_params,
          _error_function){}
};

// Simple graph
//  Minimum cut graph constructed according to the method described in:
//  "Optimal Graph Based Segmentation Using Flow Lines with Application to
//   Airway Wall Segmentation".
template<class CAPACITY_TYPE>
class simple_graph : public base_graph<CAPACITY_TYPE>
{
private:
    typedef base_graph<CAPACITY_TYPE> b;
    typedef typename b::column_type column_type;
    typedef typename column_type::nit nit;

    CAPACITY_TYPE minimum_weight;

private:
    CAPACITY_TYPE
    intra_capacity(const column_type& c, int node_index)
    {
        return c.get_node(node_index)-minimum_weight;
    }
   
    virtual void
    construct_specific()
    {
        //std::cout << "Intra" << std::endl;
        minimum_weight = std::numeric_limits<CAPACITY_TYPE>::max();
        for(int i = 0; i < b::column_count; i++)
        {
            column_type& c = b::columns[i];
            CAPACITY_TYPE r = 0;
            for(int j = 0; j < c.get_length(); j++)
            {
                CAPACITY_TYPE w = c.get_node(j);
                if(b::region_cost)
                    r += b::region_cost(i, j, b::region_cost_params);
                w += r;
                if(w < minimum_weight) minimum_weight = w;
            }
        }
        for(int i = 0; i < b::column_count; i++)
        {
            column_type& c = b::columns[i];
            assert(c.get_length());
            int top_node = c.get_length()-1;
            // Intra column edges.
            CAPACITY_TYPE r = 0;
            for(int j = 0; j < top_node; j++)
            {
                if(b::region_cost)
                    r += b::region_cost(i, j, b::region_cost_params);
#ifdef IGRAPH_DEBUG
                std::cout << "(" << i << "_" << j << "<->"
                          << i << "_" << j+1 << "): "
                          << std::max(intra_capacity(c, j)+r, 0.0) << " "
                          << b::infinite_capacity << std::endl;
#endif
                b::graph.add_edge(c.get_node_handle(j), c.get_node_handle(j+1),
                    std::max(intra_capacity(c, j)+r, 0.0),
                    b::infinite_capacity);
                assert(intra_capacity(c, j)+r >= 0);
                b::edge_count++;
            }
            if(b::region_cost)
                r += b::region_cost(i, top_node, b::region_cost_params);
#ifdef IGRAPH_DEBUG
            std::cout << "(" << i << "_" << top_node << "->t): "
                      << std::max(intra_capacity(c, top_node)+r, 0.0)
                      << std::endl;
            std::cout << "(s->" << i << "_" << 0 << "): "
                      << b::infinite_capacity << std::endl;
#endif
            // Source and sink node edges.
            b::graph.add_tweights(c.get_node_handle(top_node), 0,
                std::max(intra_capacity(c, top_node)+r, 0.0));
            b::graph.add_tweights(c.get_node_handle(0),
                b::infinite_capacity, 0);
            assert(intra_capacity(c, top_node)+r >= 0);
            b::edge_count += 2;
        }
    }
    
public:
    simple_graph(int _column_count,
        CAPACITY_TYPE (*_edge_cost)(int, int, int, void*) = 0,
        void *_params = 0,
        CAPACITY_TYPE (*_edge_cost_second_order)(int, int, int, void*) = 0,
        void *_second_order_params = 0,
        CAPACITY_TYPE (*_region_cost)(int, int, void*) = 0,
        void *_region_cost_params = 0,
        void (*_error_function)(const char*) = 0):
        b(_column_count, _edge_cost, _params, _edge_cost_second_order,
          _second_order_params, _region_cost, _region_cost_params,
          _error_function){}
};

// Brute force
//  Brute force solver of optimal net surface problems.
template<class CAPACITY_TYPE>
class brute_force : public base_graph<CAPACITY_TYPE>
{
private:
    enum status_type
    {
        status_initial,
        status_processing,
        status_done
    };

    class node
    {
    public:
        int index;
        status_type status;
        node(): status(status_initial){}
    };

    typedef base_graph<CAPACITY_TYPE> b;
    typedef typename b::column_type column_type;
    typedef typename column_type::nit nit;
    typedef typename column_type::const_nit const_nit;

    node *solution;
    int node_processing, node_done;
    CAPACITY_TYPE best_weight;

    virtual void
    construct_specific()
    {
    }
    
    void
    copy_solution(node* src, node* dst)
    {
        for(int i = 0; i < b::column_count; i++) dst[i] = src[i];
    }

    CAPACITY_TYPE
    step(int column_index, int node_index, CAPACITY_TYPE edge_weight)
    {
        //std::cout << "(" << column_index << ", " << node_index << ") "
        //          << solution[column_index].status << " "
        //          << solution[column_index].index << std::endl;
        column_type& c = b::columns[column_index];
        if(solution[column_index].status != status_initial)
        {
            // No extra steps if this node is already part of the solution.
            if(solution[column_index].index == node_index)
            {
                // Only count edge weight if visited node is not done, this
                // way sub-solutions will always include all costs.
                if(solution[column_index].status == status_done) return 0;
                else return edge_weight;
            }
            // Solution is not valid, if another node was already chosen in
            // this column.
            else return std::numeric_limits<CAPACITY_TYPE>::max();
        }
        node *backup_solution = new node[b::column_count],
            *best_solution = new node[b::column_count];
        solution[column_index].status = status_processing;
        solution[column_index].index = node_index;
        CAPACITY_TYPE w = c.get_node(node_index);
        for(int i = 0; i <= node_index; i++)
            w += b::region_cost(column_index, i, b::region_cost_params);
        for(const_nit i = c.neighbors.begin(); i != c.neighbors.end(); i++)
        {
            CAPACITY_TYPE best_weight =
                std::numeric_limits<CAPACITY_TYPE>::max();
            int neighbor_index = i->first-b::columns;
            copy_solution(solution, backup_solution);
            for(int j = c.bottom_neighbor_node(i, node_index);
                j <= c.top_neighbor_node(i, node_index); j++)
            {
                CAPACITY_TYPE weight =
                    step(neighbor_index, j, b::edge_cost(column_index,
                    neighbor_index, abs(node_index-j+i->second.offset),
                    b::params));
                // Add edge cost if the visited node was done. 
                if(weight < best_weight)
                {
                    best_weight = weight;
                    copy_solution(solution, best_solution);
                }
                copy_solution(backup_solution, solution);
            }
            if(best_weight == std::numeric_limits<CAPACITY_TYPE>::max())
            {
                w = std::numeric_limits<CAPACITY_TYPE>::max();
                break;
            }
            copy_solution(best_solution, solution);
            w += best_weight;
        }
        solution[column_index].status = status_done;
        //std::cout << "[" << column_index << ", " << node_index << "]: "
        //          << w << std::endl;
        delete[] backup_solution;
        delete[] best_solution;
        return w;
    }

    brute_force(const brute_force& b){}

    void
    component(int i)
    {
        node *backup_solution = new node[b::column_count],
             *best_solution = new node[b::column_count];
        copy_solution(solution, backup_solution);
        best_weight = std::numeric_limits<CAPACITY_TYPE>::max();
        column_type& c = b::columns[i];
        for(int j = 0; j < c.get_length(); j++)
        {
            CAPACITY_TYPE weight = step(i, j, 0);
            if(weight < best_weight)
            {
                copy_solution(solution, best_solution);
                best_weight = weight;
            }
            copy_solution(backup_solution, solution);
        }
        copy_solution(best_solution, solution);
        delete[] backup_solution;
        delete[] best_solution;
    }

public:
    brute_force(int column_count,
                CAPACITY_TYPE (*_edge_cost)(int, int, int, void*) = 0,
                void* params = 0,
                CAPACITY_TYPE (*_region_cost)(int, int, void*) = 0,
                void *_region_cost_params = 0):
                b(column_count, _edge_cost, params, 0, 0, _region_cost,
                  _region_cost_params)
    {
        solution = new node[column_count];
    }
    
    virtual
    ~brute_force()
    {
        delete[] solution;
    }

    virtual void
    construct()
    {
        b::infinite_capacity = std::numeric_limits<CAPACITY_TYPE>::max();
    }

    virtual void
    calculate()
    {
        b::feasible = true;
        for(int i = 0; i < b::column_count; i++)
        {
            if(solution[i].status != status_initial) continue;
            component(i);
            if(best_weight == std::numeric_limits<CAPACITY_TYPE>::max())
            {
                b::feasible = false;
                return;
            }
        }
    }
    
    virtual int
    source(int column_index, int node_index)
    {
        assert(column_index >= 0);
        assert(column_index < b::column_count);
        assert(node_index >= 0);
        assert(node_index < b::columns[column_index].get_length());
        return solution[column_index].index >= node_index? 1: 0;
    }
};

#endif

