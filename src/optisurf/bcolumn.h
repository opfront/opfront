/* Opfront - Optimal front segmentation
 * Copyright (c) 2014, Jens Petersen
 * All rights reserved.
 *
 * Opfront is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this software.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Opfront has a range of dependencies and if you decide to use or
 * modify it, it should be done in compliance with what ever licenses and
 * copyright issues that apply for those dependencies.
 */
#ifndef __BCOLUMN_H__
#define __BCOLUMN_H__

#include <limits>
#include <map>

class edge_interval
{
public:
    int bottom, top;

    edge_interval():
        bottom(std::numeric_limits<int>::max()),
        top(std::numeric_limits<int>::min()){}

    void
    add(int node)
    {
        if(node < bottom) bottom = node;
        if(node > top) top = node;
    }

    bool
    is_deficient() const
    {
        return bottom == std::numeric_limits<int>::max();
    }
};

template<class CAPACITY_TYPE>
class column;

template<class CAPACITY_TYPE>
class neighbor
{
private:
    edge_interval* edge_intervals;
    const column<CAPACITY_TYPE>* c;

public:
    // This is the relative offset between the indices of the two columns. So
    // column c index k is at the same relative position as the neighbor
    // column index k-offset.
    int offset;

    neighbor(): edge_intervals(0){}

    neighbor(const column<CAPACITY_TYPE>* _c, int _o): c(_c), offset(_o)
    {
        edge_intervals = new edge_interval[c->length];
    }

    neighbor(const neighbor& n) : c(n.c), offset(n.offset)
    {
        //std::cout << &n << " ";
        //std::cout << n.edge_intervals << std::endl;
        if(n.edge_intervals)
        {
            edge_intervals = new edge_interval[c->length];
            for(int i = 0; i < c->length; i++)
                edge_intervals[i] = n.edge_intervals[i];
        }
    }

    void
    change_size(int bottom, int top)
    {
        edge_interval* new_edge_intervals =
            new edge_interval[top-bottom+1];

        for(int i = bottom; i <= top ; i++)
            new_edge_intervals[i-bottom] = edge_intervals[i];
        delete[] edge_intervals;
        edge_intervals = new_edge_intervals;
    }

    int
    top(int index) const
    {
        assert(index >= 0);
        assert(index < c->length);
        //std::cout << "|" << index << " " << c->length << " " <<
        //             edge_intervals[index].top << " "
        //             << std::numeric_limits<int>::min() << "|"
        //             << std::flush;
        return edge_intervals[index].top;
    }

    int
    bottom(int index) const
    {
        assert(index >= 0);
        assert(index < c->length);
        return edge_intervals[index].bottom;
    }

    void
    add_edge(int index, int neighbor_node_index)
    {
        edge_intervals[index].add(neighbor_node_index);
    }

    const edge_interval&
    interval(int index) const
    {
        return edge_intervals[index];
    }

    ~neighbor()
    {
        if(edge_intervals) delete[] edge_intervals;
    }
};

template<class CAPACITY_TYPE>
class column
{
public:
    typedef neighbor<CAPACITY_TYPE> neighbor_type;
    typedef column<CAPACITY_TYPE> column_type;
    typedef std::map<column_type*, neighbor_type> neighbor_map;
    typedef typename neighbor_map::iterator nit;
    typedef typename neighbor_map::const_iterator const_nit;
    neighbor_map neighbors;

private:
    friend class neighbor<CAPACITY_TYPE>;

    CAPACITY_TYPE* nodes;
    int length;

    // Privatize until properly implemented.
    column(const column_type& c){assert(0);}

    void
    mark_deficient(int node_index, CAPACITY_TYPE infinite_cost)
    {
        for(const_nit j = neighbors.begin(); j != neighbors.end(); j++)
        {
            if(j->second.interval(node_index).is_deficient())
            {
                nodes[node_index] = infinite_cost;
                break;
            }
        }
    }

public:
    int handle;

    column(): length(0){}

    column(int _length): length(_length)
    {
        nodes = new CAPACITY_TYPE[length];
    }

    ~column()
    {
        if(length) delete[] nodes;
    }

    void
    add_neighbor(column_type& neighbor, int offset)
    {
        nit i = neighbors.find(&neighbor);
        if(i == neighbors.end())
        {
            std::pair<column_type*, neighbor_type> p(&neighbor,
                neighbor_type(this, offset));
            neighbors.insert(p);
        } else i->second.offset = offset;
    }

    bool
    is_neighbor(const column_type& neighbor) const
    {
        const_nit i = neighbors.find(&neighbor);
        if(i == neighbors.end()) return false;
        else return true;
    }

    void
    add_edge(int node_index, nit& neighbor, int neighbor_node_index)
    {
        assert(neighbor != neighbors.end());
        neighbor->second.add_edge(node_index, neighbor_node_index);
    }

    void
    add_edge(int node_index, column_type& neighbor_column,
             int neighbor_node_index)
    {
        nit neighbor = neighbors.find(&neighbor_column);
        add_edge(node_index, neighbor, neighbor_node_index);
    }

    // Add edge range
    //  Note indices here are supposed to be thought of as relative, meaning
    //  relative to the neighbor offset.
    void
    add_edge_range(column_type& neighbor_column, int from_index, int to_index)
    {
        int neighbor_top_node = neighbor_column.get_length()-1;
        nit neighbor = neighbors.find(&neighbor_column);
        int o = neighbor->second.offset;
        for(int i = 0; i < length; i++, o++)
        {
            int bottom, top;
            if(from_index > to_index)
            {
                bottom = std::numeric_limits<int>::min();
                top = std::numeric_limits<int>::max();
            }
            else
            {
                bottom = o+from_index;
                top = o+to_index;
            }
            // Likely deficient nodes.
            if(bottom > neighbor_top_node) continue;
            if(top < 0) continue;

            if(bottom < 0) bottom = 0;
            if(top > neighbor_top_node) top = neighbor_top_node;
            add_edge(i, neighbor, top);
            add_edge(i, neighbor, bottom);
        }
    }

    // Add edge range
    //  Note indices here are supposed to be thought of as relative, meaning
    //  relative to the neighbor offset.
    void
    add_edge_range(column_type& neighbor_column, int indices)
    {
        add_edge_range(neighbor_column, -indices, indices);
    }
    
    int
    bottom_neighbor_node(const_nit& neighbor_iterator, int node_index) const
    {
        return neighbor_iterator->second.bottom(node_index);
    }

    int
    bottom_neighbor_node(const column_type& neighbor_column, int node_index)
        const
    {
        const_nit n = neighbors.find(&neighbor_column);
        assert(n != neighbors.end());
        return bottom_neighbor_node(n, node_index);
    }

    int
    top_neighbor_node(const_nit& neighbor_iterator, int node_index) const
    {
        return neighbor_iterator->second.top(node_index);
    }

    int
    top_neighbor_node(const column_type& neighbor_column, int node_index) const
    {
        nit n = neighbors.find(&neighbor_column);
        assert(n != neighbors.end());
        return top_neighbor_node(n, node_index);
    }

    void
    set_neighbor_offset(column_type& neighbor_column, int offset)
    {
        nit n = neighbors.find(&neighbor_column);
        assert(n != neighbors.end());
        n->second.offset = offset;
    }

    void
    set_node(int node_index, CAPACITY_TYPE weight)
    {
        assert(node_index >= 0);
        assert(node_index < length);
        nodes[node_index] = weight;
    }

    CAPACITY_TYPE
    get_node(int node_index) const
    {
        assert(node_index >= 0);
        assert(node_index < length);
        return nodes[node_index];
    }

    int
    get_node_handle(int node_index) const
    {
        assert(node_index >= 0);
        assert(node_index < length);
        return handle+node_index;
    }

    int
    get_length() const
    {
        return length;
    }

    // Mark deficient
    //  This gives the deficient nodes in this column infinite cost.
    void
    mark_deficient(CAPACITY_TYPE infinite_cost)
    {
        for(int i = 0; i < length; i++) mark_deficient(i, infinite_cost);
    }
};

#endif

