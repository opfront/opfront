/* Opfront - Optimal front segmentation
 * Copyright (c) 2014, Jens Petersen
 * All rights reserved.
 *
 * Opfront is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this software.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Opfront has a range of dependencies and if you decide to use or
 * modify it, it should be done in compliance with what ever licenses and
 * copyright issues that apply for those dependencies.
 */
#ifndef __MGRAPH_H__
#define __MGRAPH_H__

#include "optisurf/igraph.h"

template<class CAPACITY_TYPE>
class multi_graph_params
{
public:
    int column_count;

    CAPACITY_TYPE (*edge_cost)(int, int, int, int, int, void*);
    CAPACITY_TYPE (*edge_cost_second_order)(int, int, int, int, int, void*);
    CAPACITY_TYPE (*region_cost)(int, int, int, void*);
    void *user_params, *user_params_second_order, *user_params_region_cost;
};


template<class CAPACITY_TYPE>
CAPACITY_TYPE
multi_graph_edge_cost(int column_i, int column_j, int delta, void *params)
{
    if(!params) return 0;
    multi_graph_params<CAPACITY_TYPE> *data =
        static_cast<multi_graph_params<CAPACITY_TYPE>*>(params);
    int subgraph_i = column_i/data->column_count,
        subgraph_j = column_j/data->column_count;
    //column_i %= data->column_count;
    //column_j %= data->column_count;
    column_i -= subgraph_i*data->column_count;
    column_j -= subgraph_j*data->column_count;
    return data->edge_cost(subgraph_i, subgraph_j, column_i, column_j, delta,
                           data->user_params);
}

template<class CAPACITY_TYPE>
CAPACITY_TYPE
multi_graph_edge_cost_second_order(int column_i, int column_j, int delta,
                                   void *params)
{
    if(!params) return 0;    
    multi_graph_params<CAPACITY_TYPE> *data =
        static_cast<multi_graph_params<CAPACITY_TYPE>*>(params);
    int subgraph_i = column_i/data->column_count,
        subgraph_j = column_j/data->column_count;
    column_i %= data->column_count;
    column_j %= data->column_count;
    return data->edge_cost_second_order(subgraph_i, subgraph_j, column_i,
        column_j, delta, data->user_params_second_order);
}

template<class CAPACITY_TYPE>
CAPACITY_TYPE
multi_graph_region_cost(int column_index, int node_index, void *params)
{
    if(!params) return 0;
    multi_graph_params<CAPACITY_TYPE> *data =
        static_cast<multi_graph_params<CAPACITY_TYPE>*>(params);
    int subgraph = column_index/data->column_count;
    column_index %= data->column_count;
    return data->region_cost(subgraph, column_index, node_index,
        data->user_params_region_cost)-data->region_cost(subgraph+1,
        column_index, node_index, data->user_params_region_cost);
}

// Multi graph
//  Implements an easy to use interface for multiple surface problems on top of
//  the igraph framework according to description in:
//  "Optimal Graph Based Segmentation Using Flow Lines with Application to
//   Airway Wall Segmentation".
//  Note:
//   Every subgraph is forced to have the same number of columns.
//   Columns in the different subgraphs are not assumed to be related, so
//   they can have different lengths and different midpoints.
//   Thus most of the functions are just wrapper functions to the simple_graph
//   class, making it possible to have a subgraph index as well.
template<class CAPACITY_TYPE>
class multi_graph : public optimal_net_graph<CAPACITY_TYPE>
{
private:
    typedef optimal_net_graph<CAPACITY_TYPE> b;
    
    multi_graph_params<CAPACITY_TYPE> params;

    int
    convert_column_index(int subgraph, int column_index) const
    {
        return column_index+subgraph*params.column_count;
    }
    
    void
    instantiate() const
    {
        // Make sure template functions are instantiated.
        multi_graph_edge_cost<CAPACITY_TYPE>(0, 0, 0, 0);
        multi_graph_edge_cost_second_order<CAPACITY_TYPE>(0, 0, 0, 0);
        multi_graph_region_cost<CAPACITY_TYPE>(0, 0, 0);
    }

public:
    multi_graph(int subgraph_count, int column_count,
        CAPACITY_TYPE (*_edge_cost)(int, int, int, int, int, void*) = 0,
        void *_params = 0,
        CAPACITY_TYPE (*_edge_cost_second_order)
            (int, int, int, int, int, void*) = 0,
        void *_second_order_params = 0,
        CAPACITY_TYPE (*_region_cost)(int, int, int, void*) = 0,
        void *_region_cost_params = 0,
        void (*_error_function)(const char*) = 0):
        b(column_count*subgraph_count, 
        (_edge_cost)? static_cast<CAPACITY_TYPE (*)(int, int, int, void*)>
            (multi_graph_edge_cost<CAPACITY_TYPE>): 0,
        static_cast<void*>(&params),
        _edge_cost_second_order? static_cast<CAPACITY_TYPE (*)(int, int, int,
            void*)>(multi_graph_edge_cost_second_order<CAPACITY_TYPE>): 0,
        static_cast<void*>(&params),
        _region_cost? static_cast<CAPACITY_TYPE (*)(int, int, void*)>
            (multi_graph_region_cost<CAPACITY_TYPE>): 0,
        static_cast<void*>(&params),
        _error_function)
    {
        instantiate();
        params.column_count = column_count;
        params.user_params = _params;
        params.user_params_second_order = _second_order_params;
        params.user_params_region_cost = _region_cost_params;
        params.edge_cost = _edge_cost;
        params.edge_cost_second_order = _edge_cost_second_order;
        params.region_cost = _region_cost;
    }

    void
    set_column_length(int subgraph, int column_index, int length)
    {
        b::set_column_length(convert_column_index(subgraph, column_index),
            length);
    }

    void
    add_node(int subgraph, int column_index, int node_index,
             CAPACITY_TYPE weight)
    {
        b::add_node(convert_column_index(subgraph, column_index), node_index,
                    weight);
    }

    void
    set_neighbors(int subgraph_i, int column_i, int subgraph_j, int column_j,
                  int offset)
    {
        assert(column_i != column_j || subgraph_i != subgraph_j);
        b::set_neighbors(convert_column_index(subgraph_i, column_i),
                         convert_column_index(subgraph_j, column_j),
                         offset);
    }

    void
    add_edge(int subgraph_i, int column_i, int node_i,
             int subgraph_j, int column_j, int node_j)
    {
        assert(column_i != column_j || subgraph_i != subgraph_j);
        b::add_edge(convert_column_index(subgraph_i, column_i), node_i,
                    convert_column_index(subgraph_j, column_j), node_j);
    }
    
    void
    add_edge_range(int subgraph_i, int column_i,
        int subgraph_j, int column_j, int from_indices, int to_indices)
    {
        assert(column_i != column_j || subgraph_i != subgraph_j);
        b::add_edge_range(convert_column_index(subgraph_i, column_i),
            convert_column_index(subgraph_j, column_j), from_indices,
            to_indices);
    }

    void
    add_smoothness_constraint(int subgraph_i, int column_i,
                              int subgraph_j, int column_j, int indices)
    {
        b::add_smoothness_constraint(
            convert_column_index(subgraph_i, column_i),
            convert_column_index(subgraph_j, column_j), indices);
    }

    virtual int
    source(int subgraph, int column_index, int node_index)
    {
        return b::source(convert_column_index(subgraph, column_index),
                         node_index);
    }
    
    virtual int
    source(int subgraph, int column_index, int node_index,
        CAPACITY_TYPE &min_marginal_s, CAPACITY_TYPE &min_marginal_t)
    {
        return b::source(convert_column_index(subgraph, column_index),
                         node_index, min_marginal_s, min_marginal_t);
    }
    
    virtual int
    source(int subgraph, int column_index,
        CAPACITY_TYPE *min_marginal_s, CAPACITY_TYPE *min_marginal_t)
    {
        return b::source(convert_column_index(subgraph, column_index),
                         min_marginal_s, min_marginal_t);
    }
    
    virtual void
    surface(int subgraph, int column_index, int node_index,
        CAPACITY_TYPE &cost)
    {
        return b::surface(convert_column_index(subgraph, column_index),
                          node_index, cost);
    }
};

#endif

