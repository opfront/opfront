/* Opfront - Optimal front segmentation
 * Copyright (c) 2014, Jens Petersen
 * All rights reserved.
 *
 * Opfront is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this software.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Opfront has a range of dependencies and if you decide to use or
 * modify it, it should be done in compliance with what ever licenses and
 * copyright issues that apply for those dependencies.
 */
#ifndef __BGRAPH_H__
#define __BGRAPH_H__

#include <iostream>
#include "graph.h"
#include "optisurf/bcolumn.h"

template<class CAPACITY_TYPE>
class base_graph
{
protected:
    typedef Graph<CAPACITY_TYPE, CAPACITY_TYPE, CAPACITY_TYPE> graph_type;
    typedef column<CAPACITY_TYPE> column_type;
    typedef neighbor<CAPACITY_TYPE> neighbor_type;
    typedef typename column_type::nit nit;
    typedef typename column_type::const_nit const_nit;
    graph_type graph;
    column_type *columns;
    int column_count;
    bool feasible;
    CAPACITY_TYPE (*edge_cost)(int, int, int, void*),
                  (*edge_cost_second_order)(int, int, int, void*),
                  (*region_cost)(int, int, void*);
    void *params, *second_order_params, *region_cost_params;
    double infinite_capacity, certainty_infinite, maxflow;

    // Triangle
    //  Triangle function given in 'Optimal Graph Based Segmentation Using
    //  Flow Lines with Application to Airway Wall Segmentation'.
    CAPACITY_TYPE
    triangle(int column_from, int column_to, int delta)
    {
        if(delta < 0) return 0;
        if(edge_cost_second_order)
        {
            if(!delta)
                return edge_cost(column_from, column_to, delta, params);
            return edge_cost_second_order(column_from, column_to, delta,
                                          second_order_params);
        }
        if(edge_cost)
        {
            CAPACITY_TYPE t0 =
                edge_cost(column_from, column_to, delta, params);
            CAPACITY_TYPE t1 =
                edge_cost(column_from, column_to, delta+1, params)-t0; 
            if(!delta) return t1;
            return t1-(t0-edge_cost(column_from, column_to, delta-1, params));
        }
        return 0;
    }

    // Capacity
    //  Returns the capacity of an edge to a node in one column to a
    //  node in a neighboring column.
    CAPACITY_TYPE
    capacity(int column_from, int column_to, int from_node, int to_node,
             int edge_bottom, int offset)
    {
        // Edges outside allowed arrange recheck this, should not happen.
        assert(to_node >= edge_bottom);
        // Edges to bottom node are not needed.
        if(!to_node || to_node < edge_bottom) return 0;
        // Enforce allowed range.
        if(to_node == edge_bottom) return infinite_capacity;

        CAPACITY_TYPE r =
            triangle(column_from, column_to, from_node-to_node+offset);
        // This happens when user specified a non-convex function or due to
        // rounding errors. We can not check for the former due to the latter.
        if(r < 0) return 0;
        else return r;
    }

    void
    add_bounded_bottom(int c0, int c1, const_nit& n0, const_nit& n1)
    {
        int short_column_index, long_column_index, offset = n0->second.offset;
        if(offset < 0)
        {
            short_column_index = c1;
            long_column_index = c0;
            offset = -offset;
        }
        else if(offset > 0)
        {
            short_column_index = c0;
            long_column_index = c1;
        }
        else
        {
            return;
        }
        column_type &sc = columns[short_column_index],
                    &lc = columns[long_column_index];
        // The top node in the column with the largest inside length might be
        // lower than the relative offset.
        int end_index;
        if(offset >= lc.get_length()) end_index = lc.get_length();
        else end_index = offset;

        // Note this has O(lc.get_length()^3) worst case running time.
        // It is probably not an issue, but should be fixed non-the-less!
        for(int i = -offset; i < 0; i++)
        {
            for(int j = 0; j < end_index; j++)
            {
                // Only the capacities of the edges going to column with
                // the largest inside length are relevant.
                // Note:
                //  We can (and have to!) skip the bottom_neighbor_node stuff
                //  because we are adding edges from non-existing nodes,
                //  which do not have connections to the neighbors. However
                //  this is not a problem, as these nodes do not become a part
                //  of the solution.
                CAPACITY_TYPE cap = capacity(short_column_index,
                    long_column_index, i, j,
                    std::numeric_limits<int>::min(), offset);
                if(!cap)
                {
                    continue;
                }
                // Add these capacities to the vertices below the endpoint.
                for(int k = j-1; k >= 0; k--)
                {
                    lc.set_node(k, lc.get_node(k)+cap);
                }
            }
        }
    }
    
    void
    add_bounded_top(int c0, int c1, const_nit& n0, const_nit& n1)
    {
        int short_column_index, long_column_index, offset = n0->second.offset;
        const_nit *n;
        if(columns[c0].get_length() < columns[c1].get_length()-offset)
        {
            short_column_index = c0;
            long_column_index = c1;
            n = &n1;
            offset = -offset;
        }
        else if(columns[c0].get_length() > columns[c1].get_length()-offset)
        {
            short_column_index = c1;
            long_column_index = c0;
            n = &n0;
        }
        else
        {
            return;
        }
        column_type &sc = columns[short_column_index],
                    &lc = columns[long_column_index];
        int start_index = sc.get_length()-offset;
        // The top node in the column with the shortest outside length might be
        // lower than the relative offset.
        if(start_index < 0) start_index = 0;

        // Note this has O(lc.get_length()^3) worst case running time.
        // It is probably not an issue, but should be fixed non-the-less!
        for(int i = start_index; i < lc.get_length(); i++)
        {
            int bnn = lc.bottom_neighbor_node(*n, i),
                tnn = lc.top_neighbor_node(*n, i);
            for(int j = std::max(sc.get_length(), bnn);
                j < lc.get_length()+offset; j++)
            {
                // Only the capacities of the edges going to the column with
                // the shortest outside length are relevant.
                CAPACITY_TYPE cap = capacity(long_column_index,
                    short_column_index, i, j, bnn, offset);
                if(!cap)
                {
                    continue;
                }
                // Add these capacities to the vertices above the
                // from-endpoint.
                for(int k = i; k < lc.get_length(); k++)
                {
                    lc.set_node(k, lc.get_node(k)+cap);
                }
            }
        }
    }

    void
    graph_add_edge(int from_column, int from_node,
        int to_column, int to_node,
        CAPACITY_TYPE to, CAPACITY_TYPE from)
    {
        column_type &s0 = columns[from_column], &s1 = columns[to_column];
        if(!from_node) from = 0;
        if(!to_node) to = 0;
        if(to == 0 && from == 0) return;
#ifdef IGRAPH_DEBUG
        std::cout << "inter (" << from_column << "_" << from_node << "<->"
            << to_column << "_" << to_node << "):"
            << to << " " << from << std::endl;
#endif
        graph.add_edge(s0.get_node_handle(from_node),
            s1.get_node_handle(to_node), to, from);
        edge_count++;
        
    }

    void
    add_none_bounded(int c0, int c1, const_nit& n0, const_nit& n1)
    {
        column_type &s0 = columns[c0], &s1 = columns[c1];
        int offset = n0->second.offset;
        for(int i = 0; i < s0.get_length(); i++)
        {
            int bnn = s0.bottom_neighbor_node(n0, i),
                tnn = s0.top_neighbor_node(n0, i);
            for(int j = bnn; j <= tnn; j++)
            {
                CAPACITY_TYPE to = capacity(c0, c1, i, j, bnn, offset),
                    from = capacity(c1, c0, j, i,
                        s1.bottom_neighbor_node(n1, j), -offset);
                graph_add_edge(c0, i, c1, j, to, from);
            }
        }
    }

    void
    add_bounded_bottom_linear(int c0, int c1, const_nit& n0, const_nit& n1)
    {
        int short_column_index, long_column_index, offset = n0->second.offset;
        if(offset < 0)
        {
            short_column_index = c1;
            long_column_index = c0;
            offset = -offset;
        }
        else if(offset > 0)
        {
            short_column_index = c0;
            long_column_index = c1;
        }
        else
        {
            return;
        }
        column_type &sc = columns[short_column_index],
                    &lc = columns[long_column_index];
        // The top node in the column with the largest inside length might be
        // lower than the relative offset.
        int end_index;
        if(offset >= lc.get_length()) end_index = lc.get_length();
        else end_index = offset;

        // Note this has O(lc.get_length()^2) worst case running time.
        // It is probably not an issue, but should be fixed non-the-less!
        for(int i = -offset; i < 0; i++)
        {
            int j = i+offset;
            if(j >= lc.get_length() || j < 0) continue;
            // Only the capacities of the edges going to column with
            // the largest inside length are relevant.
            // Note:
            //  We can (and have to!) skip the bottom_neighbor_node stuff
            //  because we are adding edges from non-existing nodes,
            //  which do not have connections to the neighbors. However
            //  this is not a problem, as these nodes do not become a part
            //  of the solution.
            CAPACITY_TYPE cap = capacity(short_column_index,
                long_column_index, i, j,
                std::numeric_limits<int>::min(), offset);
            // Add these capacities to the vertices below the endpoint.
            for(int k = j-1; k >= 0; k--)
            {
                lc.set_node(k, lc.get_node(k)+cap);
            }
        }
    }
    
    void
    add_bounded_top_linear(int c0, int c1, const_nit& n0, const_nit& n1)
    {
        int short_column_index, long_column_index, offset = n0->second.offset;
        const_nit *n;
        if(columns[c0].get_length() < columns[c1].get_length()-offset)
        {
            short_column_index = c0;
            long_column_index = c1;
            n = &n1;
            offset = -offset;
        }
        else if(columns[c0].get_length() > columns[c1].get_length()-offset)
        {
            short_column_index = c1;
            long_column_index = c0;
            n = &n0;
        }
        else
        {
            return;
        }
        column_type &sc = columns[short_column_index],
                    &lc = columns[long_column_index];
        int start_index = sc.get_length()-offset;
        // The top node in the column with the shortest outside length might be
        // lower than the relative offset.
        if(start_index < 0) start_index = 0;

        // Note this has O(lc.get_length()^2) worst case running time.
        // It is probably not an issue, but should be fixed non-the-less!
        for(int i = start_index; i < lc.get_length(); i++)
        {
            int bnn = lc.bottom_neighbor_node(*n, i);
            int j = i+offset;
            if(bnn >= j) continue;
            CAPACITY_TYPE cap = capacity(long_column_index,
                short_column_index, i, j, bnn, offset);
            // Only the capacities of the edges going to the column with
            // the shortest outside length are relevant.
            // Add these capacities to the vertices above the
            // from-endpoint.
            for(int k = i; k < lc.get_length(); k++)
            {
                lc.set_node(k, lc.get_node(k)+cap);
            }
        }
    }
    
    void
    add_none_bounded_linear(int c0, int c1, const_nit& n0, const_nit& n1)
    {
        column_type &s0 = columns[c0], &s1 = columns[c1];
        int offset = n0->second.offset;
        for(int from_node = 0; from_node < s0.get_length(); from_node++)
        {
            int s0bnn = s0.bottom_neighbor_node(n0, from_node);
            int to_node = from_node+offset;
            if(to_node >= 0 && to_node < s1.get_length())
            {
                int s1bnn = s1.bottom_neighbor_node(n1, to_node);
                CAPACITY_TYPE
                    to = capacity(c0, c1, from_node, to_node, s0bnn, offset),
                    from = capacity(c1, c0, to_node, from_node, s1bnn, -offset);
                graph_add_edge(c0, from_node, c1, to_node, to, from);
            }
            // Add infinity capacity node to bottom neighbour node once.
            if(s0bnn < 0 || s0bnn >= s1.get_length() || s0bnn == to_node)
                continue;
            CAPACITY_TYPE r;
            if(s1.bottom_neighbor_node(n1, s0bnn) == from_node)
                r = infinite_capacity;
            else r = 0;
            graph_add_edge(c0, from_node, c1, s0bnn, infinite_capacity, r);
        }
        for(int to_node = 0; to_node < s1.get_length(); to_node++)
        {
            int s1bnn = s1.bottom_neighbor_node(n1, to_node);
            int from_node = to_node-offset;
            if(s1bnn < 0 || s1bnn >= s0.get_length() || from_node == s1bnn)
                continue;
            // Check if already added.
            if(s0.bottom_neighbor_node(n0, s1bnn) == to_node) continue;
            graph_add_edge(c0, s1bnn, c1, to_node, 0, infinite_capacity);
        }
    }

    void
    add_inter_column_edges()
    {
        for(int c0 = 0; c0 < column_count; c0++)
        {
            column_type& c = columns[c0];
            // Inter column edges.
            for(const_nit n0 = c.neighbors.begin(); n0 != c.neighbors.end();
                n0++)
            {
                int c1 = n0->first-columns;
                // Skip if we already added these edges.
                if(c1 < c0) continue;
                // Check if we are using graph construction optimized for
                // linear penalties.
                const_nit n1 = columns[c1].neighbors.find(&c);
                if(edge_cost(c0, c1, -1, params) > 0)
                {
                    add_bounded_bottom(c0, c1, n0, n1);
                    add_bounded_top(c0, c1, n0, n1);
                    add_none_bounded(c0, c1, n0, n1);
                }
                else
                {
                    //add_bounded_bottom_linear(c0, c1, n0, n1);
                    //add_bounded_top_linear(c0, c1, n0, n1);
                    add_bounded_bottom_linear(c0, c1, n0, n1);
                    add_bounded_top_linear(c0, c1, n0, n1);
                    add_none_bounded_linear(c0, c1, n0, n1);
                }
            }
        }
    }

private:

    virtual void
    construct_specific() = 0;

    // Add edge
    //  Create an edge between nodes in neighboring columns.
    void
    add_edge(column_type& column_i, column_type& column_j,
             nit& neighbor_i, nit& neighbor_j, int node_i, int node_j)
    {
        column_i.add_edge(neighbor_j, node_i, node_j);
        column_j.add_edge(neighbor_i, node_j, node_i);
    }

public:
    int node_count, edge_count;

    base_graph(int _column_count,
        CAPACITY_TYPE (*_edge_cost)(int, int, int, void*) = 0,
        void *_params = 0,
        CAPACITY_TYPE (*_edge_cost_second_order)(int, int, int, void*) = 0,
        void *_second_order_params = 0,
        CAPACITY_TYPE (*_region_cost)(int, int, void*) = 0,
        void *_region_cost_params = 0,
        void (*_error_function)(const char *) = 0):
        column_count(_column_count), graph(0, 0, _error_function),
        edge_cost(_edge_cost),
        params(_params), edge_cost_second_order(_edge_cost_second_order),
        second_order_params(_second_order_params), region_cost(_region_cost),
        region_cost_params(_region_cost_params)
    {
        assert(column_count > 0);
        columns = new column_type[column_count];
        certainty_infinite = 1;
    }

    virtual
    ~base_graph()
    {
        delete[] columns;
    }

    // Set column length
    //  Set the number of nodes in the column.
    //  Should be done before adding nodes.
    void
    set_column_length(int column_index, int length)
    {
        assert(column_index < column_count);
        new(&columns[column_index]) column_type(length);
    }

    // Add node
    //  Adds a node in a column with a given weight.
    void
    add_node(int column_index, int node_index, CAPACITY_TYPE weight)
    {
        assert(column_index < column_count);
        columns[column_index].set_node(node_index, weight);
    }

    // Get neighbors
    //  Returns true if column_i and column_j are neighbors.
    bool
    get_neighbors(int column_i, int column_j)
    {
        assert(column_i < column_count);
        assert(column_j < column_count);
        bool n = columns[column_i].get_neighbor(column_j);
        assert(n == columns[column_j].get_neighbor(column_i));
        return n;
    }

    // Set neighbors
    //  Makes the columns i and j neighbors. Should be done
    //  before edges are added between the columns.
    void
    set_neighbors(int column_i, int column_j, int offset)
    {
        assert(column_i >= 0);
        assert(column_j >= 0);
        assert(column_i < column_count);
        assert(column_j < column_count);
        columns[column_i].add_neighbor(columns[column_j], offset);
        columns[column_j].add_neighbor(columns[column_i], -offset);
    }
    
    // Add edge
    //  Create an edge between nodes in neighboring columns.
    //  Note:
    //   The system will assume a correct proper ordered multi-column graph,
    //   which means that not every edge will be stored and that missing edges
    //   will be added implicitly.
    void
    add_edge(int column_i, int node_i, int column_j, int node_j)
    {
        assert(column_i >= 0);
        assert(column_j >= 0);
        assert(column_i < column_count);
        assert(column_j < column_count);
        columns[column_i].add_edge(node_i, columns[column_j], node_j);
        columns[column_j].add_edge(node_j, columns[column_i], node_i);
    }

    // Add smoothness constraint
    //  Creates edges from column_i to nodes in column j within offset +-
    //  indices and conversely.
    void
    add_smoothness_constraint(int _column_i, int _column_j, int indices)
    {
        assert(_column_i >= 0);
        assert(_column_j >= 0);
        assert(_column_i < column_count);
        assert(_column_j < column_count);
        column_type &column_i = columns[_column_i],
                    &column_j = columns[_column_j];
        column_i.add_edge_range(column_j, indices);
        column_j.add_edge_range(column_i, indices);
    }
    
    // Add range
    //  Creates edges from column_i to nodes in column j within the interval
    //  index+[from_index, to_index] and conversely. Indices are automatically
    //  cropped to column lengths.
    void
    add_edge_range(int _column_i, int _column_j, int from_index, int to_index)
    {
        assert(_column_i >= 0);
        assert(_column_j >= 0);
        assert(_column_i < column_count);
        assert(_column_j < column_count);
        column_type &column_i = columns[_column_i],
                    &column_j = columns[_column_j];
        
        column_i.add_edge_range(column_j, from_index, to_index);
        column_j.add_edge_range(column_i, -to_index, -from_index);
    }

    // Construct
    //  Construct the maximum flow graph.
    virtual void
    construct()
    {
        feasible = true;
        // First count nodes, set handles and compute an upper bound on the
        // cost of a valid solution.
        node_count = 0;
        edge_count = 0;
        double e = 0,
               wmin = std::numeric_limits<CAPACITY_TYPE>::max(),
               wmax = -std::numeric_limits<CAPACITY_TYPE>::max();
        int neighbor_count = 0;
        for(int i = 0; i < column_count; i++)
        {
            column_type &c = columns[i];
            c.handle = node_count;
            node_count += c.get_length();
            for(int j = 0; j < c.get_length(); j++)
            {
                double wjmin = c.get_node(j);
                double wjmax = wjmin;
                if(region_cost)
                {
                    // Might be inside or outside region, so assume worst case.
                    double r = region_cost(i, j, region_cost_params);
                    if(r > 0) wjmax += r;
                    else wjmin += r;
                }
                if(wjmin < wmin) wmin = wjmin;
                if(wjmax > wmax) wmax = wjmax;
                for(const_nit k = c.neighbors.begin(); k != c.neighbors.end();
                    k++)
                {
                    if(k->first-columns < i) continue;
                    neighbor_count++;


                    const edge_interval &ei = k->second.interval(j);
                    if(ei.is_deficient()) continue;
                    int top = abs(j-k->second.top(j)+k->second.offset),
                        bottom = abs(j-k->second.bottom(j)+k->second.offset),
                        delta;
                    if(top > bottom) delta = top;
                    else delta = bottom;
                    double ej = edge_cost(i, k->first-columns, delta, params);
                    if(ej > e) e = ej;
                }
            }
        }
        infinite_capacity = node_count*(wmax-wmin)+e*neighbor_count+1;
        // Set weight of deficient nodes to infinity.
        for(int i = 0; i < column_count; i++)
            columns[i].mark_deficient(infinite_capacity);
        for(int i = 0; i < column_count; i++)
        {
            column_type& c = columns[i];
        }
        if(!node_count) return;
        // Backup weights.
        int node_index = 0;
        CAPACITY_TYPE *weights = new CAPACITY_TYPE[node_count];
        for(int i = 0; i < column_count; i++)
        {
            column_type& c = columns[i];
            for(int j = 0; j < c.get_length(); j++)
            {
                weights[node_index++] = c.get_node(j);
            }
        }
        graph.add_node(node_count);
        add_inter_column_edges();
        construct_specific();
        // Restore weights.
        node_index = 0;
        for(int i = 0; i < column_count; i++)
        {
            column_type& c = columns[i];
            for(int j = 0; j < c.get_length(); j++) 
            {
                c.set_node(j, weights[node_index++]);
            }
        }
        delete[] weights;
    }

    virtual void
    calculate()
    {
#ifdef IGRAPH_DEBUG
        std::cout << "Max-flow graph nodes: " << node_count
                  << " edges: " << edge_count << std::endl;
#endif
        maxflow = graph.maxflow();
#ifdef IGRAPH_DEBUG
        std::cout << "Max-flow: " << maxflow << std::endl;
#endif
    }

    virtual int
    source(int column_index, int node_index)
    {
        assert(column_index >= 0);
        assert(column_index < column_count);
        if(node_index < 0) return 1;
        if(node_index >= columns[column_index].get_length()) return 0;
        return graph.what_segment(
            columns[column_index].get_node_handle(node_index)) ==
            graph_type::SOURCE? 1: 0;
    }
    
    virtual int
    source(int column_index, int node_index, CAPACITY_TYPE &min_marginal_s,
        CAPACITY_TYPE &min_marginal_t)
    {
        if(node_index == 0)
        {
            min_marginal_s = graph.maxflow(1);
            min_marginal_t = std::numeric_limits<CAPACITY_TYPE>::max();
            return 1;
        }
        int r = source(column_index, node_index);
        int node_handle = columns[column_index].get_node_handle(node_index);
        if(r)
        {
            while(1)
            {
                // Computing alternate solution
                graph.mark_node(node_handle);
                graph.add_tweights(node_handle, 0, certainty_infinite);
                min_marginal_t = graph.maxflow(1);
                // Restoring best solution.
                graph.mark_node(node_handle);
                graph.add_tweights(node_handle, 0, -certainty_infinite);
                if(!source(column_index, node_index)) break;
                certainty_infinite *= 2;
            }
            min_marginal_s = graph.maxflow(1);
        }
        else
        {
            while(1)
            {
                // Computing alternate solution.
                graph.mark_node(node_handle);
                graph.add_tweights(node_handle, certainty_infinite, 0);
                min_marginal_s = graph.maxflow(1);
                // Restoring best solution.
                graph.mark_node(node_handle);
                graph.add_tweights(node_handle, -certainty_infinite, 0);
                if(source(column_index, node_index)) break;
                certainty_infinite *= 2;
            }
            min_marginal_t = graph.maxflow(1);
        }
        return r;
    }
    
    virtual void
    surface(int column_index, int node_index, CAPACITY_TYPE &cost)
    {
        int node_handle = columns[column_index].get_node_handle(node_index),
            next_node_handle;
        if(node_index+1 < columns[column_index].get_length()) 
            next_node_handle = columns[column_index].get_node_handle(
                node_index+1);
        else next_node_handle = -1;
        while(1)
        {
            // Compute cost of a solution at this position.
            graph.mark_node(node_handle);
            graph.add_tweights(node_handle, certainty_infinite, 0);
            if(next_node_handle != -1)
            {
                graph.mark_node(next_node_handle);
                graph.add_tweights(next_node_handle, 0, certainty_infinite);
            }
            cost = graph.maxflow(1);
            // Restoring best solution.
            graph.mark_node(node_handle);
            graph.add_tweights(node_handle, -certainty_infinite, 0);
            if(next_node_handle != -1)
            {
                graph.mark_node(next_node_handle);
                graph.add_tweights(next_node_handle, 0, -certainty_infinite);
            }
            if(source(column_index, node_index) && (next_node_handle == -1 ||
               !source(column_index, node_index+1))) break;
            certainty_infinite *= 2;
        }
        // Need to reset graph to get correct source queries.
        graph.maxflow(1);
    } 
   
    void
    print_changed(Block<typename graph_type::node_id>* changed_list)
    {
        int changed = 0;
        typename graph_type::node_id* ptr;
        for(ptr=changed_list->ScanFirst(); ptr;
                ptr=changed_list->ScanNext())
        {
            typename graph_type::node_id i = *ptr;
            graph.remove_from_changed_list(i);
            changed++;
        }
        changed_list->Reset();
        std::cout << " " << changed << std::flush;
    }

    virtual int
    source(int column_index, CAPACITY_TYPE *min_marginal_s,
        CAPACITY_TYPE *min_marginal_t)
    {
        int r, l = columns[column_index].get_length();
        // We compute this again rather than relying on stored maxflow as
        // repeated addition and subtractions of terminal capacities, happening
        // as a result of repeated calls to this function, can lead to
        // numerical errors, which would otherwise shift marginals away from
        // each other.
        ofloat optimal_max_flow = graph.maxflow(1);

        // Find first node outside surface.
        for(r = 1; r < l; r++) if(!source(column_index, r)) break;
        // Compute min_marginals outside surface. Note this is done in order
        // of increasing cost, to limit the number of changes in the graph
        // and thus computation time.
        for(int i = r; i < l; i++)
        {
            int node_handle = columns[column_index].get_node_handle(i);
            min_marginal_t[i] = optimal_max_flow;
            while(1)
            {
                // Computing alternate solution.
                graph.mark_node(node_handle);
                graph.add_tweights(node_handle, certainty_infinite, 0);
                min_marginal_s[i] = graph.maxflow(1);
                //graph.mark_node(node_handle);
                //graph.add_tweights(node_handle, -certainty_infinite, 0);
                // Satisfied if node became source, otherwise try again.
                if(source(column_index, i)) break;
                // First adjust previous nodes in column to the new capacity.
                for(int j = r; j < i; j++)
                {
                    int n = columns[column_index].get_node_handle(j);
                    graph.mark_node(n);
                    graph.add_tweights(n, certainty_infinite, 0);
                }
                // Then try again.
                graph.mark_node(node_handle);
                graph.add_tweights(node_handle, -certainty_infinite, 0);
                certainty_infinite *= 2;
            }
        }
        // Remove infinite cost edges.
        for(int i = r; i < l; i++)
        {
            int node_handle = columns[column_index].get_node_handle(i);
            graph.mark_node(node_handle);
            graph.add_tweights(node_handle, -certainty_infinite, 0);
        }
        // Compute min_marginals inside surface. See note above.
        for(int i = r-1; i > 0; i--)
        {
            int node_handle = columns[column_index].get_node_handle(i);
            min_marginal_s[i] = optimal_max_flow;
            while(1)
            {
                // Computing alternate solution.
                graph.mark_node(node_handle);
                graph.add_tweights(node_handle, 0, certainty_infinite);
                min_marginal_t[i] = graph.maxflow(1);
                // Satisfied if node became terminal, otherwise try again.
                if(!source(column_index, i)) break;
                // First adjust previous nodes in column to the new capacity.
                for(int j = r-1; j > i; j--)
                {
                    int n = columns[column_index].get_node_handle(j);
                    graph.mark_node(n);
                    graph.add_tweights(n, 0, certainty_infinite);
                }
                // Then try again.
                graph.mark_node(node_handle);
                graph.add_tweights(node_handle, 0, -certainty_infinite);
                certainty_infinite *= 2;
            }
        }
        // Remove infinite cost edges.
        for(int i = r-1; i > 0; i--)
        {
            int node_handle = columns[column_index].get_node_handle(i);
            graph.mark_node(node_handle);
            graph.add_tweights(node_handle, 0, -certainty_infinite);
        }

        min_marginal_s[0] = optimal_max_flow;
        min_marginal_t[0] = std::numeric_limits<CAPACITY_TYPE>::max();
        return r-1;
    }
 
    /*virtual int
    source(int column_index, CAPACITY_TYPE *min_marginal_s,
        CAPACITY_TYPE *min_marginal_t)
    {
        int r, l = columns[column_index].get_length();
        // We compute this again rather than relying on stored maxflow as
        // repeated addition and subtractions of terminal capacities, happening
        // as a result of repeated calls to this function, can lead to
        // numerical errors, which would otherwise shift marginals away from
        // each other.
        ofloat optimal_max_flow = graph.maxflow(1);

        // Find first node outside surface.
        for(r = 1; r < l; r++) if(!source(column_index, r)) break;
        // Compute min_marginals outside surface. Note this is done in order
        // of increasing cost, to limit the number of changes in the graph
        // and thus computation time.
        for(int i = r; i < l; i++)
        {
            int node_handle = columns[column_index].get_node_handle(i);
            min_marginal_t[i] = optimal_max_flow;
            while(1)
            {
                // Computing alternate solution.
                graph.mark_node(node_handle);
                graph.add_tweights(node_handle, certainty_infinite, 0);
                min_marginal_s[i] = graph.maxflow(1);
                graph.mark_node(node_handle);
                graph.add_tweights(node_handle, -certainty_infinite, 0);
                // Satisfied if node became source, otherwise try again.
                if(source(column_index, i)) break;
                certainty_infinite *= 2;
            }
        }
        optimal_max_flow = graph.maxflow(1);
        // Compute min_marginals inside surface. See note above.
        for(int i = r-1; i > 0; i--)
        {
            int node_handle = columns[column_index].get_node_handle(i);
            min_marginal_s[i] = optimal_max_flow;
            while(1)
            {
                // Computing alternate solution.
                graph.mark_node(node_handle);
                graph.add_tweights(node_handle, 0, certainty_infinite);
                min_marginal_t[i] = graph.maxflow(1);
                graph.mark_node(node_handle);
                graph.add_tweights(node_handle, 0, -certainty_infinite);
                // Satisfied if node became terminal, otherwise try again.
                if(!source(column_index, i)) break;
                certainty_infinite *= 2;
            }
        }
        return r-1;
    }*/

    CAPACITY_TYPE
    get_maxflow()
    {
        return graph.maxflow(1);
    }

    CAPACITY_TYPE
    cost()
    {
        if(!feasible) return std::numeric_limits<CAPACITY_TYPE>::infinity();
        typedef typename column_type::nit nit;
        CAPACITY_TYPE r = 0;
        int *net = new int[column_count];
        for(int i = 0; i < column_count; i++)
        {
            int j;
            column_type& c = columns[i];
            for(j = c.get_length()-1; j >= 0; j--)
            {
                if(source(i, j)) break;
            }
            if(j < 0)
            {
#ifdef IGRAPH_DEBUG
                std::cout << "No source node in column: " << i << std::endl;
#endif
                delete[] net;
                feasible = false;
                return std::numeric_limits<CAPACITY_TYPE>::infinity();
            }
            net[i] = j;
        }
        for(int i = 0; i < column_count; i++)
        {
            column_type& c = columns[i];
            r += c.get_node(net[i]);
            if(!edge_cost) continue;
            for(const_nit n = c.neighbors.begin(); n != c.neighbors.end(); n++)
            {
                int ni = n->first-columns;
                if(ni < i) continue;
                if(net[ni] > c.top_neighbor_node(n, net[i]) ||
                   net[ni] < c.bottom_neighbor_node(n, net[i]))
                {
                    delete[] net;
                    feasible = false;
#ifdef IGRAPH_DEBUG
                    std::cout << "Solution outside constraints: " << i
                        << " " << ni << " " << net[ni] << std::endl;
#endif
                    return std::numeric_limits<CAPACITY_TYPE>::infinity();
                }
                r += edge_cost(i, ni, abs(net[i]-net[ni]+n->second.offset),
                               params);
            }
        }
        if(r >= infinite_capacity)
        {
            feasible = false;
            r = std::numeric_limits<CAPACITY_TYPE>::infinity();
        }
        delete[] net;
        return r;
    }
   
    // Print Solution
    //  Prints the obtained solution. 
    void
    print_solution()
    {
        std::cout << "Solution: ";
        if(!feasible)
        {
            std::cout << "not feasible!" << std::endl;
            return;
        }
        for(int i = 0; i < column_count; i++)
        {
            int j, node_index;
            std::cout << i << "_";
            column_type& c = columns[i];
            for(j = c.get_length()-1; j >= 0; j--)
            {
                if(source(i, j)) break;
            }
            assert(j >= 0);
            std::cout << j << " ";
        }
        std::cout << std::endl;
    }

    // Print Graph
    //  Prints the graph.
    void
    print_graph()
    {
        for(int i = 0; i < column_count; i++)
        {
            column_type& c = columns[i];
            std::cout << "Column: " << i << " ";
            for(int j = 0; j < c.get_length(); j++)
            {
                std::cout << "(" << j << ", " << c.get_node(j) << ") ";
            }
            std::cout << std::endl;
            for(const_nit n = c.neighbors.begin(); n != c.neighbors.end(); n++)
            {
                int ni = n->first-columns;
                if(ni < i) continue;
                std::cout << " Neighbors: " << i << " & " << ni << std::endl;
                for(int j = 0; j < c.get_length(); j++)
                {
                    std::cout << "  Edges: " << i << "_" << j << ": ";
                    for(int k = c.bottom_neighbor_node(n, j);
                        k <= c.top_neighbor_node(n, j); k++)
                    {
                        std::cout << "(" << ni << "_" << k << ": "
                            << edge_cost(i, ni, abs(j-k+n->second.offset),
                            params) << ") ";
                    }
                    std::cout << std::endl;
                }
            }
        }
    }
};

#endif

