/* Opfront - Optimal front segmentation
 * Copyright (c) 2014, Jens Petersen
 * All rights reserved.
 *
 * Opfront is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this software.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Opfront has a range of dependencies and if you decide to use or
 * modify it, it should be done in compliance with what ever licenses and
 * copyright issues that apply for those dependencies.
 */
#include <iostream>
#include <boost/program_options.hpp>
#include "cimgext/cimgext.h"
#include "opfront/opfront.h"
#include "opfront/multi_surface_meta.h"
#include "opfront/image_trace.h"
#include "util/gts_surface.h"

namespace po = boost::program_options;

ofloat seed_radius;

int
main(int ac, char* av[])
{
    try
    {
        int flow_line_type, flow_line_kernel_size;
        ofloat flow_line_kernel_spacing, flow_line_regularization,
            flow_line_sample_interval, flow_line_max_inner,
            flow_line_max_outer;
        std::string volume_file, segmentation_file, output_prefix;
        po::options_description options_description("Allowed options");
        options_description.add_options()
            ("help,h", "produce help message")
            ("volume_file,v",
                po::value<std::string>(&volume_file),
                "volume file (optional, used for voxel spacing)")
            ("segmentation_file,s",
                po::value<std::string>(&segmentation_file),
                "the initial segmentation file")
            ("output_prefix,p",
                po::value<std::string>(&output_prefix),
                "output flow line files using this prefix")
            ("flow_line_sample_interval,b",
                po::value<ofloat>(&flow_line_sample_interval)->
                default_value(1), "Flow line sampling interval")
            ("flow_line_type,t",
                po::value<int>(&flow_line_type)->default_value(0),
                "0 - Gauss, 1 - Canny-Deriche, 2 - ELF")
            ("flow_line_kernel_spacing,k",
                po::value<ofloat>(&flow_line_kernel_spacing)->default_value(1),
                "the convolution grid spacing")
            ("flow_line_regularization,r",
                po::value<ofloat>(&flow_line_regularization)->default_value(2),
                "the regularization used when computing flow lines")
            ("flow_line_kernel_size,c",
                po::value<int>(&flow_line_kernel_size)->default_value(21),
                "the convolution kernel size, should be odd positive number")
            ("flow_line_max_inner,M",
                po::value<ofloat>(&flow_line_max_inner)->default_value(-1),
                "the maximum inner flow line length (mm)")
            ("flow_line_max_outer,N",
                po::value<ofloat>(&flow_line_max_outer)->default_value(-1),
                "the maximum exterior flow line length (mm)");
        po::variables_map vm;
        po::store(po::parse_command_line(ac, av, options_description), vm);
        po::notify(vm);
        if(vm.count("help"))
        {
            std::cout << "Optimal front flow line extractor"
                      << std::endl;
            std::cout << "Usage: options_description [options]" << std::endl;
            std::cout << options_description;
            return 0;
        }
        if(segmentation_file == "")
        {
            std::cerr << "Error: please specify initial segmentation"
                << std::endl;
            return 1;
        }
        GtsSurface *surface = gts_surface_new(gts_surface_class(),
            gts_face_class(), gts_edge_class(),
            GTS_VERTEX_CLASS(gts_mvertex_class()));
        FILE *pfile = fopen(segmentation_file.c_str(), "r");
        GtsFile *fp = gts_file_new(pfile);
        GtsVertex **vertices;
        GtsEdge **edges;
        GtsFace **faces;
        int err = gts_surface_read_ext(surface, fp, &vertices, &edges, &faces);
        gts_file_destroy(fp);
        fclose(pfile);
        multi_surface(surface, 1);
        cimg_library::CImg<ofloat> volume;
        if(volume_file != "")
        {
            volume.load(volume_file.c_str());
            multi_surface_meta_ijk2xyz(volume, surface);
        }
        multi_surface_select(surface, 0);
        image_tracer_type image_tracer(flow_line_regularization,
            flow_line_sample_interval, flow_line_kernel_spacing,
            flow_line_max_inner, flow_line_max_outer,
            flow_line_type, flow_line_kernel_size);
        image_tracer.update_surface(surface);
        image_tracer(std::vector<GtsMVertex*>((GtsMVertex**)vertices,
            (GtsMVertex**)vertices+gts_surface_vertex_number(surface)));
#ifdef OPFRONT_DEBUG
        line_stat(std::vector<GtsMVertex*>((GtsMVertex**)vertices,
            (GtsMVertex**)vertices+gts_surface_vertex_number(surface)));
#endif
        if(volume_file != "") multi_surface_meta_xyz2ijk(volume, surface);
        std::string column_file = output_prefix+".col";
        multi_surface_save_columns(column_file.c_str(), surface, vertices);
        gts_object_destroy(GTS_OBJECT(surface));
    }
    catch(std::exception& e)
    {
        std::cout << "Error: " << e.what() << std::endl;
        gts_finalize();
        return 1;
    }
    gts_finalize();
    return 0;
}

