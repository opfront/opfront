/* Opfront - Optimal front segmentation
 * Copyright (c) 2014, Jens Petersen
 * All rights reserved.
 *
 * Opfront is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this software.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Opfront has a range of dependencies and if you decide to use or
 * modify it, it should be done in compliance with what ever licenses and
 * copyright issues that apply for those dependencies.
 */
#include <stdlib.h>
#include "util/gts_util.h"

static gint
gts_util_vertex_multiply(gpointer item, gpointer data)
{
    GtsVertex *v = GTS_VERTEX(item);
    double *s = static_cast<double*>(data);
    v->p.x *= s[0];
    v->p.y *= s[1];
    v->p.z *= s[2];
    return 0;
}

void
gts_util_multiply(GtsSurface *surface, double *s)
{
    gts_surface_foreach_vertex(surface, gts_util_vertex_multiply, s);
}

static gint
gts_util_vertex_multiply_s(gpointer item, gpointer data)
{
    GtsVertex *v = GTS_VERTEX(item);
    double *s = static_cast<double*>(data);
    v->p.x *= s[0];
    v->p.y *= s[0];
    v->p.z *= s[0];
    return 0;
}

void
gts_util_multiply_s(GtsSurface *surface, double s)
{
    gts_surface_foreach_vertex(surface, gts_util_vertex_multiply_s, &s);
}

static gint
gts_util_vertex_add(gpointer item, gpointer data)
{
    GtsVertex *v = GTS_VERTEX(item);
    double *s = static_cast<double*>(data);
    v->p.x += s[0];
    v->p.y += s[1];
    v->p.z += s[2];
    return 0;
}

void
gts_util_add(GtsSurface *surface, double *s)
{
    gts_surface_foreach_vertex(surface, gts_util_vertex_add, s);
}

struct gts_util_vertex_pair
{
    GtsVertex *o, *n;
};

struct gts_util_apply_vertex_function_data
{
    GtsVertex* (*vertex_function)(GtsVertex*, gpointer);
    void *params;
    gts_util_vertex_pair *replacements;
    int index;
};

static int
gts_util_apply_vertex_function_vertex(gpointer v, gpointer d)
{
    gts_util_apply_vertex_function_data *data =
        (gts_util_apply_vertex_function_data*)(d);
    gts_util_vertex_pair *p = &data->replacements[data->index];
    p->o = GTS_VERTEX(v);
    p->n = data->vertex_function(p->o, data->params);
    data->index++;
    return 0;
}

//#include <iostream>

void
gts_util_apply_vertex_function(GtsSurface *surface,
    GtsVertex* (*vertex_function)(GtsVertex *o, gpointer params),
    gpointer params)
{
    gts_util_apply_vertex_function_data data;
    data.vertex_function = vertex_function;
    data.params = params;
    data.index = 0;
    gint vertex_count = gts_surface_vertex_number(surface);
    data.replacements = (gts_util_vertex_pair*)
        malloc(sizeof(gts_util_vertex_pair)*vertex_count);
    gts_surface_foreach_vertex(surface,
        gts_util_apply_vertex_function_vertex, (gpointer)&data);
    for(int i = 0; i < vertex_count; i++)
    {
        gts_util_vertex_pair *p = data.replacements+i;
        GtsVertex *o = GTS_VERTEX(p->o), *n = GTS_VERTEX(p->n);
        //gts_vertex_replace(o, n);
        //n->segments = o->segments;
        //gdouble
        //    d0 = o->p.x-n->p.x,
        //    d1 = o->p.y-n->p.y,
        //    d2 = o->p.z-n->p.z;
        //if(d0*d0+d1*d1+d2*d2 > 10)
        //{
        //    std::cout << o->p.x << " " << o->p.y << " " << o->p.z << " - "
        //              << n->p.x << " " << n->p.y << " " << n->p.z << std::endl;
        //}
        for(GSList *j = o->segments; j; j = j->next)
        {
            GtsSegment *s = GTS_SEGMENT(j->data);
            if(s->v1 != n && s->v2 != n)
                n->segments = g_slist_prepend(n->segments, s);
            if(s->v1 == o) s->v1 = n;
            else
            {
                g_return_if_fail(s->v2 == o);
                s->v2 = n;
            }
        }
        g_slist_free(o->segments);
        o->segments = NULL;
        gts_object_destroy(GTS_OBJECT(o));
    }
    free(data.replacements);
}

// Vertex add
//  Add a weighted portion of the position of v1 to v0.
void
gts_util_vertex_add(GtsVertex *v0, const GtsVertex *v1, gdouble weight)
{
    v0->p.x += weight*v1->p.x;
    v0->p.y += weight*v1->p.y;
    v0->p.z += weight*v1->p.z;
}

// Vertex multiply
//  Multiply the position of v with x.
void
gts_util_vertex_multiply(GtsVertex *v, gdouble x)
{
    v->p.x *= x;
    v->p.y *= x;
    v->p.z *= x;
}

