/* Opfront - Optimal front segmentation
 * Copyright (c) 2014, Jens Petersen
 * All rights reserved.
 *
 * Opfront is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this software.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Opfront has a range of dependencies and if you decide to use or
 * modify it, it should be done in compliance with what ever licenses and
 * copyright issues that apply for those dependencies.
 */
#ifndef __GTS_MCUBE_H__
#define __GTS_MCUBE_H__

#include <gts.h>

template<class CIMG_TYPE>
void
gts_mcube_sample_isosurface(gdouble **a, GtsCartesianGrid g, guint i,
                            gpointer data)
{
    cimg_library::CImg<CIMG_TYPE> *isosurface =
        static_cast<cimg_library::CImg<CIMG_TYPE>*>(data);
    for(int i = 0; i < g.nx; i++)
    {
        double x = g.x+i*g.dx;
        for(int j = 0; j < g.ny; j++)
        {
            double y = g.y+j*g.dy;
            a[i][j] = isosurface->cubic_atXYZ(x, y, g.z);
        }
    }
}

// Gts marching cube
//  Compute a gts surface from an isosurface stored in the cimg image format
//  using the gts_isosurface_cartesian function.
template<class CIMG_TYPE>
void
gts_mcube(GtsSurface *surface,
          cimg_library::CImg<CIMG_TYPE>& isosurface,
          double x_interval,
          double y_interval,
          double z_interval,
          double isovalue)
{
    GtsCartesianGrid g;
    g.x = g.y = g.z = 0;
    g.dx = x_interval;
    g.dy = y_interval;
    g.dz = z_interval;
    g.nx = isosurface.width()/g.dx;
    g.ny = isosurface.height()/g.dy;
    g.nz = isosurface.depth()/g.dz;
    gts_isosurface_cartesian(surface, g,
        gts_mcube_sample_isosurface<CIMG_TYPE>,
        static_cast<gpointer>(&isosurface), isovalue);
}


template<class CIMG_TYPE>
void
gts_mcube_sample_isosurface_image_spacing(gdouble **a, GtsCartesianGrid g,
    guint i, gpointer data)
{
    cimg_library::CImg<CIMG_TYPE> *isosurface =
        static_cast<cimg_library::CImg<CIMG_TYPE>*>(data);
    for(int x = 0; x < g.nx; x++)
    {
        for(int y = 0; y < g.ny; y++)
        {
            a[x][y] = (*isosurface)(x, y, static_cast<int>(g.z+0.5));
        }
    }
}

// Gts marching cube
//  Compute a gts surface from an isosurface stored in the cimg image format
//  using the gts_isosurface_cartesian function.
template<class CIMG_TYPE>
void
gts_mcube_grid(GtsSurface *surface,
    cimg_library::CImg<CIMG_TYPE>& isosurface, double isovalue = 0.5)
{
    GtsCartesianGrid g;
    g.x = g.y = g.z = 0;
    g.dx = g.dy = g.dz = 1;
    g.nx = isosurface.width();
    g.ny = isosurface.height();
    g.nz = isosurface.depth();
    gts_isosurface_cartesian(surface, g,
        gts_mcube_sample_isosurface_image_spacing<CIMG_TYPE>,
        static_cast<gpointer>(&isosurface), isovalue);
}

#endif

