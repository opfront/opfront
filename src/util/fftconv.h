/* Opfront - Optimal front segmentation
 * Copyright (c) 2014, Jens Petersen
 * All rights reserved.
 *
 * Opfront is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this software.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Opfront has a range of dependencies and if you decide to use or
 * modify it, it should be done in compliance with what ever licenses and
 * copyright issues that apply for those dependencies.
 */
#ifndef __FFTCONV_H__
#define __FFTCONV_H__

static const int
small_prime_factors[] = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 12, 14, 15, 16, 18, 20, 21, 24, 25, 27, 28, 30, 32, 35, 36, 40, 42, 45, 48, 49, 50, 54, 56, 60, 63, 64, 70, 72, 75, 80, 81, 84, 90, 96, 98, 100, 105, 108, 112, 120, 125, 126, 128, 135, 140, 144, 147, 150, 160, 162, 168, 175, 180, 189, 192, 196, 200, 210, 216, 224, 225, 240, 243, 245, 250, 252, 256, 270, 280, 288, 294, 300, 315, 320, 324, 336, 343, 350, 360, 375, 378, 384, 392, 400, 405, 420, 432, 441, 448, 450, 480, 486, 490, 500, 504, 512, 525, 540, 560, 567, 576, 588, 600, 625, 630, 640, 648, 672, 675, 686, 700, 720, 729, 735, 750, 756, 768, 784, 800, 810, 840, 864, 875, 882, 896, 900, 945, 960, 972, 980, 1000, 1008, 1024, 1029, 1050, 1080, 1120, 1125, 1134, 1152, 1176, 1200, 1215, 1225, 1250, 1260, 1280, 1296, 1323, 1344, 1350, 1372, 1400, 1440, 1458, 1470, 1500, 1512, 1536, 1568, 1575, 1600, 1620, 1680, 1701, 1715, 1728, 1750, 1764, 1792, 1800, 1875, 1890, 1920, 1944, 1960, 2000, 2016, 2025, 2048, 2058, 2100, 2160, 2187, 2205, 2240, 2250, 2268, 2304, 2352, 2400, 2401, 2430, 2450, 2500, 2520, 2560, 2592, 2625, 2646, 2688, 2700, 2744, 2800, 2835, 2880, 2916, 2940, 3000, 3024, 3072, 3087, 3125, 3136, 3150, 3200, 3240, 3360, 3375, 3402, 3430, 3456, 3500, 3528, 3584, 3600, 3645, 3675, 3750, 3780, 3840, 3888, 3920, 3969, 4000};

static const int
small_prime_factors_length = 245;

template<class CIMG_TYPE>
void
fftconv(const cimg_library::CImg<CIMG_TYPE>& mask,
        cimg_library::CImg<CIMG_TYPE>& image)
{
    assert(image.width() > 0);
    //assert(image.height() == 1 || mask.height() != 1);
    //assert(image.depth() == 1 || mask.depth() != 1); 
    int max_mask_dim =
            std::max(std::max(mask.width(), mask.height()), mask.depth()),
        max_image_dim =
            std::max(std::max(image.width(), image.height()), image.depth());
    int size = max_mask_dim+max_image_dim-1;
#ifdef cimg_use_fftw3
    int i;
    for(i = 0; i < small_prime_factors_length &&
        small_prime_factors[i] < size; i++);
    assert(i != small_prime_factors_length);
    int n = small_prime_factors[i];
#else
    int n = 1;
    for(int i = 0; i < 16 && n < size; i++, n*=2);
    assert(n >= size);
#endif
    int x_res, y_res, z_res;
    if(image.width() != 1) x_res = n;
    else x_res = 1;
    if(image.height() != 1) y_res = n;
    else y_res = 1;
    if(image.depth() != 1) z_res = n;
    else z_res = 1;
    cimg_library::CImg<CIMG_TYPE>
        real_image(x_res, y_res, z_res, 1, 0),
        imag_image(x_res, y_res, z_res, 1, 0),
        real_mask(x_res, y_res, z_res, 1, 0),
        imag_mask(x_res, y_res, z_res, 1, 0);
#ifdef FFT_DEBUG
    std::cout << "FFT size: " << x_res << " " << y_res << " " << z_res
              << " combined: " << max_mask_dim << " " << max_image_dim << " "
              << size << std::endl;
#endif
    cimg_forXYZ(image, x, y, z) real_image(x, y, z) = image(x, y, z);
    cimg_forXYZ(mask, x, y, z) real_mask(x, y, z) = mask(x, y, z);
    cimg_library::CImg<CIMG_TYPE>::FFT(real_image, imag_image);
    cimg_library::CImg<CIMG_TYPE>::FFT(real_mask, imag_mask);
    cimg_forXYZ(real_image, x, y, z)
    {
        double real_imagexyz = real_image(x, y, z);
        real_image(x, y, z) = real_imagexyz*real_mask(x, y, z)-
            imag_image(x, y, z)*imag_mask(x, y, z);
        imag_image(x, y, z) = imag_image(x, y, z)*real_mask(x, y, z)+
            real_imagexyz*imag_mask(x, y, z);
    }
    cimg_library::CImg<CIMG_TYPE>::FFT(real_image, imag_image, true);
    /*cimg_library::CImg<unsigned char> back(real_image.width(), real_image.height(),
        real_image.depth());
    cimg_forXYZ(real_image, x, y, z)
    {
        if(real_image(x, y, z) > 1) back(x, y, z) = 255;
        else if(real_image(x, y, z) < 0) back(x, y, z) = 0;
        else back(x, y, z) = real_image(x, y, z)*255;
        if(real_image(x, y, z))
        {
            std::cout << real_image(x, y, z) << " " << back(x, y, z) << std::endl;
        }
    }
    back.save("test.dcm");*/
    cimg_forXYZ(image, x, y, z)
        image(x, y, z) =
            real_image(x+mask.width()/2, y+mask.height()/2, z+mask.depth()/2);
}

#endif

