/* Opfront - Optimal front segmentation
 * Copyright (c) 2014, Jens Petersen
 * All rights reserved.
 *
 * Opfront is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this software.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Opfront has a range of dependencies and if you decide to use or
 * modify it, it should be done in compliance with what ever licenses and
 * copyright issues that apply for those dependencies.
 */
#ifndef __KERNEL_H__
#define __KERNEL_H__

#ifndef PI
#define PI 3.1415926535897932384626433832795
#endif

template<class CIMG_TYPE>
cimg_library::CImg<CIMG_TYPE>
deriche1d(CIMG_TYPE alpha, int size, CIMG_TYPE scale_x)
{
    long double s = 0;
    cimg_library::CImg<CIMG_TYPE> kernel(size);
    alpha = 1/alpha;
    cimg_forX(kernel, x)
    {
        double xt = fabs(scale_x*(x-size/2));
        kernel(x) = exp(-alpha*xt)*(alpha*xt+1);
        s += kernel(x);
    }
    kernel /= s;
    return kernel;
}

template<class CIMG_TYPE>
cimg_library::CImg<CIMG_TYPE>
deriche(CIMG_TYPE alpha, int size, CIMG_TYPE scale_x, CIMG_TYPE scale_y,
        CIMG_TYPE scale_z)
{
    long double s = 0;
    cimg_library::CImg<CIMG_TYPE> kernel(size, size, size);
    alpha = 1/alpha;
    cimg_forXYZ(kernel, x, y, z)
    {
        double xt = scale_x*(x-size/2), yt = scale_y*(y-size/2),
               zt = scale_z*(z-size/2);
        double root = sqrt(xt*xt+yt*yt+zt*zt);
        kernel(x, y, z) = exp(-alpha*root)*(alpha*root+1);
        s += kernel(x, y, z);
    }
    kernel /= s;
    return kernel;
}

template<class CIMG_TYPE>
cimg_library::CImg<CIMG_TYPE>
deriche_x(CIMG_TYPE alpha, int size, CIMG_TYPE scale_x, CIMG_TYPE scale_y,
          CIMG_TYPE scale_z)
{
    cimg_library::CImg<CIMG_TYPE> kernel(size, size, size);
    alpha = 1/alpha;
    cimg_forXYZ(kernel, x, y, z)
    {
        double xt = scale_x*(x-size/2), yt = scale_y*(y-size/2),
               zt = scale_z*(z-size/2);
        kernel(x, y, z) = -xt*exp(-alpha*sqrt(xt*xt+yt*yt+zt*zt));
    }
    return kernel;
}

template<class CIMG_TYPE>
cimg_library::CImg<CIMG_TYPE>
deriche_y(CIMG_TYPE alpha, int size, CIMG_TYPE scale_x, CIMG_TYPE scale_y,
          CIMG_TYPE scale_z)
{
    cimg_library::CImg<CIMG_TYPE> kernel(size, size, size);
    alpha = 1/alpha;
    cimg_forXYZ(kernel, x, y, z)
    {
        double xt = scale_x*(x-size/2), yt = scale_y*(y-size/2),
               zt = scale_z*(z-size/2);
        kernel(x, y, z) = -yt*exp(-alpha*sqrt(xt*xt+yt*yt+zt*zt));
    }
    return kernel;
}

template<class CIMG_TYPE>
cimg_library::CImg<CIMG_TYPE>
deriche_z(CIMG_TYPE alpha, int size, CIMG_TYPE scale_x, CIMG_TYPE scale_y,
          CIMG_TYPE scale_z)
{
    cimg_library::CImg<CIMG_TYPE> kernel(size, size, size);
    alpha = 1/alpha;
    cimg_forXYZ(kernel, x, y, z)
    {
        double xt = scale_x*(x-size/2), yt = scale_y*(y-size/2),
               zt = scale_z*(z-size/2);
        kernel(x, y, z) = -zt*exp(-alpha*sqrt(xt*xt+yt*yt+zt*zt));
    }
    return kernel;
}

template<class CIMG_TYPE>
cimg_library::CImg<CIMG_TYPE>
potential1d(CIMG_TYPE alpha, CIMG_TYPE beta, int size, CIMG_TYPE scale_x)
{
    long double s = 0;
    cimg_library::CImg<CIMG_TYPE> kernel(size);
    cimg_forX(kernel, x)
    {
        double xt = scale_x*(x-size/2);
        kernel(x) = alpha/(alpha+pow(fabs(xt), beta));
        s += kernel(x);
    }
    kernel /= s;
    return kernel;
}

template<class CIMG_TYPE>
cimg_library::CImg<CIMG_TYPE>
potential(CIMG_TYPE alpha, CIMG_TYPE beta,
          int size_x, int size_y, int size_z,
          CIMG_TYPE scale_x, CIMG_TYPE scale_y, CIMG_TYPE scale_z)
{
    long double s = 0;
    cimg_library::CImg<CIMG_TYPE> kernel(size_x, size_y, size_z);
    cimg_forXYZ(kernel, x, y, z)
    {
        double xt = scale_x*(x-size_x/2), yt = scale_y*(y-size_y/2),
               zt = scale_z*(z-size_z/2);
        kernel(x, y, z) = alpha/(alpha+
                          pow(sqrt(xt*xt+yt*yt+zt*zt), beta));
        s += kernel(x, y, z);
    }
    kernel /= s;
    return kernel;
}

template<class CIMG_TYPE>
cimg_library::CImg<CIMG_TYPE>
potential(CIMG_TYPE alpha, CIMG_TYPE beta, int size,
          CIMG_TYPE scale_x, CIMG_TYPE scale_y, CIMG_TYPE scale_z)
{
    long double s = 0;
    cimg_library::CImg<CIMG_TYPE> kernel(size, size, size);
    cimg_forXYZ(kernel, x, y, z)
    {
        double xt = scale_x*(x-size/2), yt = scale_y*(y-size/2),
               zt = scale_z*(z-size/2);
        kernel(x, y, z) = alpha/(alpha+
                          pow(sqrt(xt*xt+yt*yt+zt*zt), beta));
        s += kernel(x, y, z);
    }
    kernel /= s;
    return kernel;
}

template<class CIMG_TYPE>
cimg_library::CImg<CIMG_TYPE>
potential_x(CIMG_TYPE alpha, CIMG_TYPE beta, int size,
            CIMG_TYPE scale_x, CIMG_TYPE scale_y, CIMG_TYPE scale_z)
{
    cimg_library::CImg<CIMG_TYPE> kernel(size, size, size);
    cimg_forXYZ(kernel, x, y, z)
    {
        double xt = scale_x*(x-size/2), yt = scale_y*(y-size/2),
               zt = scale_z*(z-size/2);
        double d = xt*xt+yt*yt+zt*zt;
        double dd = alpha+pow(d, beta/2);
        kernel(x, y, z) = -(beta*xt*pow(d, beta/2-1))/(dd*dd);
    }
    return kernel;
}

template<class CIMG_TYPE>
cimg_library::CImg<CIMG_TYPE>
potential_y(CIMG_TYPE alpha, CIMG_TYPE beta, int size,
            CIMG_TYPE scale_x, CIMG_TYPE scale_y, CIMG_TYPE scale_z)
{
    cimg_library::CImg<CIMG_TYPE> kernel(size, size, size);
    cimg_forXYZ(kernel, x, y, z)
    {
        double xt = scale_x*(x-size/2), yt = scale_y*(y-size/2),
               zt = scale_z*(z-size/2);
        double d = xt*xt+yt*yt+zt*zt;
        double dd = alpha+pow(d, beta/2);
        kernel(x, y, z) = -(beta*yt*pow(d, beta/2-1))/(dd*dd);
    }
    return kernel;
}

template<class CIMG_TYPE>
cimg_library::CImg<CIMG_TYPE>
potential_z(CIMG_TYPE alpha, CIMG_TYPE beta, int size,
            CIMG_TYPE scale_x, CIMG_TYPE scale_y, CIMG_TYPE scale_z)
{
    cimg_library::CImg<CIMG_TYPE> kernel(size, size, size);
    cimg_forXYZ(kernel, x, y, z)
    {
        double xt = scale_x*(x-size/2), yt = scale_y*(y-size/2),
               zt = scale_z*(z-size/2);
        double d = xt*xt+yt*yt+zt*zt;
        double dd = alpha+pow(d, beta/2);
        kernel(x, y, z) = -(beta*zt*pow(d, beta/2-1))/(dd*dd);
    }
    return kernel;
}

template<class CIMG_TYPE>
cimg_library::CImg<CIMG_TYPE>
gauss(CIMG_TYPE sigma, int size, CIMG_TYPE scale)
{
    cimg_library::CImg<CIMG_TYPE> kernel(size);
    CIMG_TYPE t = 2*sigma*sigma;
    CIMG_TYPE r = 1/sqrt(PI*t);
    long double s = 0;
    cimg_forX(kernel, x)
    {
        CIMG_TYPE xt = scale*(x-size/2);
        kernel(x) = r*exp(-(xt*xt)/t);
        s += kernel(x);
    }
    kernel /= s;
    return kernel;
}

template<class CIMG_TYPE>
cimg_library::CImg<CIMG_TYPE>
gauss_first_order(CIMG_TYPE sigma, int size, CIMG_TYPE scale)
{
    cimg_library::CImg<CIMG_TYPE> kernel(size);
    CIMG_TYPE t = 2*sigma*sigma;
    CIMG_TYPE r = 1/(sigma*sigma*sqrt(PI*t));
    cimg_forX(kernel, x)
    {
        CIMG_TYPE xt = scale*(x-size/2);
        kernel(x) = -xt*r*exp(-(xt*xt)/t);
    }
    return kernel;
}

/*template<class CIMG_TYPE>
cimg_library::CImg<CIMG_TYPE>
gauss_x(CIMG_TYPE sigma, int size,
        CIMG_TYPE scale_x, CIMG_TYPE scale_y, CIMG_TYPE scale_z)
{
    cimg_library::CImg<CIMG_TYPE> kernel(size, size, size);
    CIMG_TYPE s = sigma*sigma;
    CIMG_TYPE r = 1/sqrt(2*PI*s);
    s *= 2;
    r = r*r*r;
    cimg_forXYZ(kernel, x, y, z)
    {
        CIMG_TYPE xt = scale_x*(x-size/2), yt = scale_y*(y-size/2),
                  zt = scale_z*(z-size/2);
        kernel(x, y, z) = -r*xt*exp(-(xt*xt+yt*yt+zt*zt)/s);
    }
    return kernel;
}

template<class CIMG_TYPE>
cimg_library::CImg<CIMG_TYPE>
gauss_y(CIMG_TYPE sigma, int size,
        CIMG_TYPE scale_x, CIMG_TYPE scale_y, CIMG_TYPE scale_z)
{
    cimg_library::CImg<CIMG_TYPE> kernel(size, size, size);
    CIMG_TYPE s = sigma*sigma;
    CIMG_TYPE r = 1/sqrt(2*PI*s);
    s *= 2;
    r = r*r*r;
    cimg_forXYZ(kernel, x, y, z)
    {
        CIMG_TYPE xt = scale_x*(x-size/2), yt = scale_y*(y-size/2),
                  zt = scale_z*(z-size/2);
        kernel(x, y, z) = -r*yt*exp(-(xt*xt+yt*yt+zt*zt)/s);
    }
    return kernel;
}

template<class CIMG_TYPE>
cimg_library::CImg<CIMG_TYPE>
gauss_z(CIMG_TYPE sigma, int size,
        CIMG_TYPE scale_x, CIMG_TYPE scale_y, CIMG_TYPE scale_z)
{
    cimg_library::CImg<CIMG_TYPE> kernel(size, size, size);
    CIMG_TYPE s = sigma*sigma;
    CIMG_TYPE r = 1/sqrt(2*PI*s);
    s *= 2;
    r = r*r*r;
    cimg_forXYZ(kernel, x, y, z)
    {
        CIMG_TYPE xt = scale_x*(x-size/2), yt = scale_y*(y-size/2),
                  zt = scale_z*(z-size/2);
        kernel(x, y, z) = -r*zt*exp(-(xt*xt+yt*yt+zt*zt)/s);
    }
    return kernel;
}*/

#endif

