/* Opfront - Optimal front segmentation
 * Copyright (c) 2014, Jens Petersen
 * All rights reserved.
 *
 * Opfront is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this software.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Opfront has a range of dependencies and if you decide to use or
 * modify it, it should be done in compliance with what ever licenses and
 * copyright issues that apply for those dependencies.
 */
#ifndef __GTS_UTIL_H__
#define __GTS_UTIL_H__

#include <gts.h>

void
gts_util_multiply(GtsSurface *surface, gdouble *s);

void
gts_util_multiply_s(GtsSurface *surface, gdouble s);

void
gts_util_add(GtsSurface *surface, gdouble *s);

void
gts_util_apply_vertex_function(GtsSurface *surface,
    GtsVertex* (*vertex_function)(GtsVertex *o, gpointer params),
    gpointer params);

void
gts_util_vertex_add(GtsVertex *v0, const GtsVertex *v1, gdouble weight);

void
gts_util_vertex_multiply(GtsVertex *v, gdouble x);

#endif

