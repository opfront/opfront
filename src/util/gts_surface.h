/* GTS - Library for the manipulation of triangulated surfaces
 * Copyright (C) 1999 St�phane Popinet
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	 See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef __GTS_SURFACE_H__
#define __GTS_SURFACE_H__

#include <stdlib.h>
#include <math.h>
#include <string.h>
#include "gts.h"

/**
 * gts_surface_read:
 * @surface: a #GtsSurface.
 * @f: a #GtsFile.
 *
 * Add to @surface the data read from @f. The format of the file pointed to
 * by @f is as described in gts_surface_write().
 *
 * Returns: 0 if successful or the line number at which the parsing
 * stopped in case of error (in which case the @error field of @f is
 * set to a description of the error which occured).  
 */
/* Update split.c/surface_read() if modifying this function */
guint
gts_surface_read_ext(GtsSurface *surface, GtsFile *f,
    GtsVertex ***vertices, GtsEdge ***edges, GtsFace ***faces)
{
    guint n, nv, ne, nf;

    g_return_val_if_fail(surface != NULL, 1);
    g_return_val_if_fail(f != NULL, 1);

    if(f->type != GTS_INT)
    {
        gts_file_error(f, "expecting an integer (number of vertices)");
        return f->line;
    }
    nv = atoi(f->token->str);

    gts_file_next_token(f);
    if(f->type != GTS_INT)
    {
        gts_file_error(f, "expecting an integer (number of edges)");
        return f->line;
    }
    ne = atoi(f->token->str);

    gts_file_next_token(f);
    if(f->type != GTS_INT)
    {
        gts_file_error(f, "expecting an integer (number of faces)");
        return f->line;
    }
    nf = atoi(f->token->str);

    gts_file_next_token(f);
    if(f->type == GTS_STRING)
    {
        if(f->type != GTS_STRING)
        {
            gts_file_error(f, "expecting a string (GtsSurfaceClass)");
            return f->line;
        }
        gts_file_next_token(f);
        if(f->type != GTS_STRING)
        {
            gts_file_error(f, "expecting a string (GtsFaceClass)");
            return f->line;
        }
        gts_file_next_token(f);
        if(f->type != GTS_STRING)
        {
            gts_file_error(f, "expecting a string (GtsEdgeClass)");
            return f->line;
        }
        gts_file_next_token(f);
        if(f->type != GTS_STRING)
        {
            gts_file_error(f, "expecting a string (GtsVertexClass)");
            return f->line;
        }
        if(!strcmp (f->token->str, "GtsVertexBinary"))
            GTS_POINT_CLASS(surface->vertex_class)->binary = TRUE;
        else
        {
            GTS_POINT_CLASS(surface->vertex_class)->binary = FALSE;
            gts_file_first_token_after (f, (GtsTokenType)'\n');
        }
    }
    else gts_file_first_token_after(f, (GtsTokenType)'\n');

    if(nf <= 0) return 0;

    /* allocate nv + 1 just in case nv == 0 */
    (*vertices) = (GtsVertex**)g_malloc((nv + 1)*sizeof (GtsVertex *));
    (*edges) = (GtsEdge**)g_malloc((ne + 1)*sizeof (GtsEdge *));
    (*faces) = (GtsFace**)g_malloc((nf + 1)*sizeof (GtsFace *));

    n = 0;
    while(n < nv && f->type != GTS_ERROR)
    {
        GtsObject *new_vertex =
            gts_object_new(GTS_OBJECT_CLASS(surface->vertex_class));

        (* GTS_OBJECT_CLASS(surface->vertex_class)->read) (&new_vertex, f);
        if (f->type != GTS_ERROR)
        {
            if(!GTS_POINT_CLASS(surface->vertex_class)->binary)
                gts_file_first_token_after(f, (GtsTokenType)'\n');
            (*vertices)[n++] = GTS_VERTEX(new_vertex);
        }
        else gts_object_destroy(new_vertex);
    }
    if(f->type == GTS_ERROR) nv = n;
    if(GTS_POINT_CLASS(surface->vertex_class)->binary)
        gts_file_first_token_after(f, (GtsTokenType)'\n');

    n = 0;
    while(n < ne && f->type != GTS_ERROR)
    {
        guint p1, p2;

        if(f->type != GTS_INT)
            gts_file_error(f, "expecting an integer (first vertex index)");
        else
        {
            p1 = atoi (f->token->str);
            if(p1 == 0 || p1 > nv)
                gts_file_error(f, "vertex index `%d' is out of range `[1,%d]'", 
                    p1, nv);
            else
            {
                gts_file_next_token (f);
                if(f->type != GTS_INT)
                    gts_file_error(f,
                        "expecting an integer (second vertex index)");
                else
                {
                    p2 = atoi (f->token->str);
                    if(p2 == 0 || p2 > nv)
                        gts_file_error(f,
                            "vertex index `%d' is out of range `[1,%d]'", 
                            p2, nv);
                    else
                    {
                        GtsEdge *new_edge =
                            gts_edge_new (surface->edge_class,
                                (*vertices)[p1 - 1], (*vertices)[p2 - 1]);
                        gts_file_next_token(f);
                        if(f->type != '\n')
                            if(GTS_OBJECT_CLASS (surface->edge_class)->read)
                                (*GTS_OBJECT_CLASS (surface->edge_class)->read)
                                    ((GtsObject **) &new_edge, f);
                        gts_file_first_token_after(f, (GtsTokenType)'\n');
                        (*edges)[n++] = new_edge;
                    }
                }
            }
        }
    }
    if(f->type == GTS_ERROR) ne = n;

    n = 0;
    while (n < nf && f->type != GTS_ERROR) {
        guint s1, s2, s3;

        if (f->type != GTS_INT)
            gts_file_error (f, "expecting an integer (first edge index)");
        else {
            s1 = atoi (f->token->str);
            if (s1 == 0 || s1 > ne)
                gts_file_error (f, "edge index `%d' is out of range `[1,%d]'", 
                        s1, ne);
            else {
                gts_file_next_token (f);
                if (f->type != GTS_INT)
                    gts_file_error (f, "expecting an integer (second edge index)");
                else {
                    s2 = atoi (f->token->str);
                    if (s2 == 0 || s2 > ne)
                        gts_file_error (f, "edge index `%d' is out of range `[1,%d]'", 
                                s2, ne);
                    else {
                        gts_file_next_token (f);
                        if (f->type != GTS_INT)
                            gts_file_error (f, "expecting an integer (third edge index)");
                        else {
                            s3 = atoi (f->token->str);
                            if (s3 == 0 || s3 > ne)
                                gts_file_error (f, "edge index `%d' is out of range `[1,%d]'", 
                                        s3, ne);
                            else {
                                GtsFace * new_face = gts_face_new (surface->face_class,
                                        (*edges)[s1 - 1],
                                        (*edges)[s2 - 1],
                                        (*edges)[s3 - 1]);

                                gts_file_next_token (f);
                                if (f->type != '\n')
                                    if (GTS_OBJECT_CLASS (surface->face_class)->read)
                                        (*GTS_OBJECT_CLASS (surface->face_class)->read)
                                            ((GtsObject **) &new_face, f);
                                gts_file_first_token_after (f, (GtsTokenType)'\n');
                                gts_surface_add_face (surface, new_face);
                                (*faces)[n++] = new_face;
                            }
                        }
                    }
                }
            }
        }
    }

    if (f->type == GTS_ERROR) {
        gts_allow_floating_vertices = TRUE;
        while (nv)
            gts_object_destroy (GTS_OBJECT ((*vertices)[nv-- - 1]));
        gts_allow_floating_vertices = FALSE;
    }

    if (f->type == GTS_ERROR)
        return f->line;
    return 0;
}

#endif

