/* Opfront - Optimal front segmentation
 * Copyright (c) 2014, Jens Petersen
 * All rights reserved.
 *
 * Opfront is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this software.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Opfront has a range of dependencies and if you decide to use or
 * modify it, it should be done in compliance with what ever licenses and
 * copyright issues that apply for those dependencies.
 */
#ifndef __LINALG_H__
#define __LINALG_H__

#include <fstream>

template<class MTYPE, int DIM>
class linalg_mat
{
private:
    MTYPE x[DIM][DIM];

public:
    int
    load(const char *filename)
    {
        std::ifstream mfile(filename);
        if(!mfile.is_open()) return 0;
        for(int i = 0; i < DIM; i++) for(int j = 0; j < DIM; j++)
        {
            if(mfile.eof()) return 0;
            mfile >> x[i][j];
        }
        mfile.close();
        return 1;
    }
    
    int
    save(const char *filename) const
    {
        std::ofstream mfile(filename);
        if(!mfile.is_open()) return 0;
        for(int i = 0; i < DIM; i++)
        {
            for(int j = 0; j < DIM-1; j++)
                mfile << x[i][j] << " ";
            mfile << x[i][DIM-1] << std::endl;
        }
        mfile.close();
        return 1;
    }

    MTYPE&
    operator()(int i, int j)
    {
        assert(i >= 0 && i < DIM && j >= 0 && j < DIM);
        return x[i][j];
    }
    
    const MTYPE&
    operator()(int i, int j) const
    {
        assert(i >= 0 && i < DIM && j >= 0 && j < DIM);
        return x[i][j];
    }
};

template<class VTYPE>
VTYPE*
linalg_cop_ve3(const VTYPE *s, VTYPE *d)
{
    for(int i = 0; i < 3; i++) d[i] = s[i];
    return d;
}

template<class VTYPE>
void
linalg_swa_ve3(VTYPE *a, VTYPE *b)
{
    for(int i = 0; i < 3; i++)
    {
        VTYPE t = a[i];
        a[i] = b[i];
        b[i] = t;
    }
}

template<class MTYPE, int DIM>
void
linalg_zero_mat(linalg_mat<MTYPE, DIM> &m)
{
    for(int i = 0; i < DIM; i++)
        for(int j = 0; j < DIM; j++)
            m(i, j) = 0;
}

template<class MTYPE, int DIM>
void
linalg_identity_mat(linalg_mat<MTYPE, DIM> &m)
{
    linalg_zero_mat(m);
    for(int i = 0; i < DIM; i++) m(i, i) = 1;
}

template<class MTYPE, class VTYPE>
VTYPE*
linalg_mul_m44_ve3(const linalg_mat<MTYPE, 4> &m, const VTYPE *v, VTYPE *r)
{
    for(int i = 0; i < 3; i++)
        r[i] = m(i, 0)*v[0]+m(i, 1)*v[1]+m(i, 2)*v[2]+m(i, 3);
    return r;
}

template<class MTYPE, class VTYPE>
VTYPE*
linalg_mul_m44_ve3(const linalg_mat<MTYPE, 4> &m, VTYPE *v)
{
    VTYPE r[3];
    linalg_mul_m44_ve3(m, v, r);
    for(int i = 0; i < 3; i++) v[i] = r[i];
    return v;
}

template<class MTYPE, class VTYPE>
VTYPE*
linalg_mul_m33_ve3(const linalg_mat<MTYPE, 3> &m, const VTYPE *v, VTYPE *r)
{
    for(int i = 0; i < 3; i++)
        r[i] = m(i, 0)*v[0]+m(i, 1)*v[1]+m(i, 2)*v[2];
    return r;
}

// TBD Note this needs to be fixed to support different dimensions
template<class MTYPE, int DIM>
linalg_mat<MTYPE, DIM>
linalg_mul_mat_mat(const linalg_mat<MTYPE, DIM> &a,
    const linalg_mat<MTYPE, DIM> &b)
{
    linalg_mat<MTYPE, DIM> m;
    linalg_zero_mat(m);
    for(int i = 0; i < DIM; i++)
    {
        for(int j = 0; j < DIM; j++)
        {
            for(int k = 0; k < DIM; k++)
            {
                m(i, j) += a(i, k)*b(k, j);
            }
        }
    }
    return m;
}

template<class MTYPE>
MTYPE
linalg_det_m33(const linalg_mat<MTYPE, 3> &m)
{
    return m(0,0)*(m(1,1)*m(2,2)-m(2,1)*m(1,2))-
           m(0,1)*(m(1,0)*m(2,2)-m(1,2)*m(2,0))+
           m(0,2)*(m(1,0)*m(2,1)-m(1,1)*m(2,0));
}

template<class MTYPE>
linalg_mat<MTYPE, 3>
linalg_inv_m33(const linalg_mat<MTYPE, 3> &m)
{
    linalg_mat<MTYPE, 3> r;
    MTYPE invdet = 1/linalg_det_m33(m);
    r(0,0) =  (m(1,1)*m(2,2)-m(2,1)*m(1,2))*invdet;
    r(0,1) = -(m(0,1)*m(2,2)-m(0,2)*m(2,1))*invdet;
    r(0,2) =  (m(0,1)*m(1,2)-m(0,2)*m(1,1))*invdet;
    r(1,0) = -(m(1,0)*m(2,2)-m(1,2)*m(2,0))*invdet;
    r(1,1) =  (m(0,0)*m(2,2)-m(0,2)*m(2,0))*invdet;
    r(1,2) = -(m(0,0)*m(1,2)-m(1,0)*m(0,2))*invdet;
    r(2,0) =  (m(1,0)*m(2,1)-m(2,0)*m(1,1))*invdet;
    r(2,1) = -(m(0,0)*m(2,1)-m(2,0)*m(0,1))*invdet;
    r(2,2) =  (m(0,0)*m(1,1)-m(1,0)*m(0,1))*invdet;
    return r;
}

template<class VTYPE, class STYPE>
VTYPE*
linalg_mul_scl_ve3(STYPE s, VTYPE *v)
{
    for(int i = 0; i < 3; i++) v[i] *= s;
    return v;
}

template<class STYPE, class VTYPE, class UTYPE>
UTYPE*
linalg_mul_scl_ve3(STYPE s, const VTYPE *v, UTYPE *u)
{
    for(int i = 0; i < 3; i++) u[i] = v[i]*s;
    return u;
}

template<class VTYPE, class STYPE>
VTYPE*
linalg_div_ve3_scl(VTYPE *v, STYPE s)
{
    for(int i = 0; i < 3; i++) v[i] /= s;
    return v;
}

template<class ATYPE, class BTYPE>
BTYPE*
linalg_add_ve3_ve3(const ATYPE *a, BTYPE *b)
{
    for(int i = 0; i < 3; i++) b[i] += a[i];
    return b;
}

template<class ATYPE, class BTYPE, class CTYPE>
CTYPE*
linalg_add_ve3_ve3(const ATYPE *a, const BTYPE *b, CTYPE *c)
{
    for(int i = 0; i < 3; i++) c[i] = b[i]+a[i];
    return c;
}

template<class ATYPE, class BTYPE>
BTYPE*
linalg_sub_ve3_ve3(const ATYPE *a, BTYPE *b)
{
    for(int i = 0; i < 3; i++) b[i] -= a[i];
    return b;
}

template<class ATYPE, class BTYPE, class CTYPE>
BTYPE*
linalg_sub_ve3_ve3(const ATYPE *a, const BTYPE *b, CTYPE *c)
{
    for(int i = 0; i < 3; i++) c[i] = a[i]-b[i];
    return c;
}

template<class VTYPE>
VTYPE
linalg_dot_ve3(const VTYPE *a, const VTYPE *b)
{
    return a[0]*b[0]+a[1]*b[1]+a[2]*b[2];
}

template<class VTYPE>
VTYPE
linalg_len_ve3(const VTYPE *v)
{
    return sqrt(linalg_dot_ve3(v, v));
}

template<class VTYPE>
VTYPE*
linalg_nor_ve3(VTYPE *v)
{
    linalg_mul_scl_ve3(1/linalg_len_ve3(v), v);
    return v;
}

template<class VTYPE>
VTYPE
linalg_dsq_ve3(const VTYPE *a, const VTYPE *b)
{
    VTYPE d[] = {a[0]-b[0], a[1]-b[1], a[2]-b[2]};
    return linalg_dot_ve3(d, d);
}

template<class VTYPE>
VTYPE
linalg_dis_ve3(const VTYPE *a, const VTYPE *b)
{
    return sqrt(linalg_dsq_ve3(a, b));
}

template<class VTYPE>
VTYPE*
linalg_crs_ve3(const VTYPE *a, const VTYPE *b, VTYPE *c)
{
    c[0] = a[1]*b[2]-a[2]*b[1];
    c[1] = a[2]*b[0]-a[0]*b[2];
    c[2] = a[0]*b[1]-a[1]*b[0];
    return c;
}

template<class VTYPE>
VTYPE
linalg_dsq_seg_ve3(const VTYPE *a, const VTYPE *b, const VTYPE *c)
{
    double oth = (b[0]-a[0])*(c[0]-b[0])+
                 (b[1]-a[1])*(c[1]-b[1])+
                 (b[2]-a[2])*(c[2]-b[2]);
    double dsqcb = linalg_dsq_ve3(c, b), dsqba = linalg_dsq_ve3(b, a);
    double t = -oth/dsqcb;
    if(t < 0) return static_cast<VTYPE>(dsqba);
    else if(t > 1) return static_cast<VTYPE>(linalg_dsq_ve3(c, a));
    return static_cast<VTYPE>((dsqba*dsqcb-oth*oth)/dsqcb);

}

template<class VTYPE>
VTYPE
linalg_ang_ve3(const VTYPE *a, const VTYPE *b)
{
    return acos(linalg_dot_ve3(a, b)/(linalg_len_ve3(a)*linalg_len_ve3(b)));
}

template<class VTYPE>
bool
linalg_equ_ve3(const VTYPE *a, const VTYPE *b)
{
    for(int i = 0; i < 3; i++) if(a[i] != b[i]) return false;
    return true;
}

// Volume of tetrahedron
template<class VTYPE>
VTYPE
linalg_vol_ve3(const VTYPE *a, const VTYPE *b, const VTYPE *c, const VTYPE *d)
{
    VTYPE ab[3], ac[3], ad[3], n[3];
    linalg_sub_ve3_ve3(a, b, ab);
    linalg_sub_ve3_ve3(a, c, ac);
    linalg_sub_ve3_ve3(a, d, ad);
    linalg_crs_ve3(ab, ac, n);
    VTYPE l = linalg_len_ve3(n);
    if(!l) return 0;
    // Note this is a safe operation, despite the division, because
    // a small l implies small valued n as well.
    linalg_div_ve3_scl(n, l);

    VTYPE h = linalg_dot_ve3(ad, n);
    return l*fabs(h)/6;
}

// Area of triangle
template<class VTYPE>
VTYPE
linalg_are_ve3(const VTYPE *a, const VTYPE *b, const VTYPE *c)
{
    VTYPE ab[3], ac[3], n[3];
    linalg_sub_ve3_ve3(a, b, ab);
    linalg_sub_ve3_ve3(a, c, ac);
    linalg_crs_ve3(ab, ac, n);
    return linalg_len_ve3(n)/2;
}

// Area of quadrilateral
//  Input: corners of quadrilateral, where ac and bd form the diagonals.
template<class VTYPE>
VTYPE
linalg_are_ve3(const VTYPE *a, const VTYPE *b, const VTYPE *c, const VTYPE *d)
{
    VTYPE ac[3], bd[3], n[3];
    linalg_sub_ve3_ve3(a, c, ac);
    linalg_sub_ve3_ve3(b, d, bd);
    linalg_crs_ve3(ac, bd, n);
    return linalg_len_ve3(n)/2;
}

// Barycentric matrix
template<class VTYPE>
linalg_mat<VTYPE, 3>
linalg_bcm_ve3(const VTYPE *a, const VTYPE *b, const VTYPE *c, const VTYPE *d)
{
    linalg_mat<VTYPE, 3> m;
    m(0, 0) = a[0]-d[0]; m(0, 1) = b[0]-d[0]; m(0, 2) = c[0]-d[0];
    m(1, 0) = a[1]-d[1]; m(1, 1) = b[1]-d[1]; m(1, 2) = c[1]-d[1];
    m(2, 0) = a[2]-d[2]; m(2, 1) = b[2]-d[2]; m(2, 2) = c[2]-d[2];
    return m;
}

// Plane normal
template<class VTYPE>
void
linalg_pno_ve3(const VTYPE *v0, const VTYPE *v1, const VTYPE *v2, VTYPE *n)
{
    VTYPE v01[3], v02[3];
    lalg_sub_ve3_ve3(v0, v1, v01);
    lalg_sub_ve3_ve3(v0, v2, v02);
    lalg_crs_ve3_ve3(v01, v02, n);
}

// Test if triangles have same orientation
template<class VTYPE>
bool
linalg_ori_ve3(const VTYPE *u0, const VTYPE *u1, const VTYPE *u2,
               const VTYPE *v0, const VTYPE *v1, const VTYPE *v2)
{
    VTYPE nu[3], nv[3];
    linalg_pno_ve3(u0, u1, u2, nu);
    linalg_pno_ve3(v0, v1, v2, nv);
    return linalg_crs_ve3(nu, nv) > 0;
}

#endif

