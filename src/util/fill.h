/* Opfront - Optimal front segmentation
 * Copyright (c) 2014, Jens Petersen
 * All rights reserved.
 *
 * Opfront is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this software.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Opfront has a range of dependencies and if you decide to use or
 * modify it, it should be done in compliance with what ever licenses and
 * copyright issues that apply for those dependencies.
 */
#ifndef __FILL_H__
#define __FILL_H__

#include <limits>

// Fill
//   Functionality to efficiently classify points.
//
//   LABEL_TYPE is label type.
//   POINT_TYPE is the point type.
//   CLASS_TYPE is a classification class, which implements the function
//     LABEL_TYPE classify(const POINT_TYPE*) const.
//
template<class LABEL_TYPE, class POINT_TYPE, class CLASS_TYPE>
class fill
{
public:
    typedef cimg_library::CImg<LABEL_TYPE>      image_type;
    typedef std::pair<LABEL_TYPE, POINT_TYPE*>  node;

private:
    const CLASS_TYPE*                           classifier;

    // Add point
    //  This is a grow helper function, adds the seed point if it is inside
    //  the image and not classified yet.
    void
    add_point(unsigned *seeds, unsigned& seed_count, image_type& image,
              int x, int y, int z) const
    {
        if(!image.atXYZ(x, y, z, 0, 1))
        {
            // Mark it as queued for processing.
            image(x, y, z) = std::numeric_limits<LABEL_TYPE>::max();
            seeds[seed_count++] =
                static_cast<unsigned>(z)*
                static_cast<unsigned>(image.height())*
                static_cast<unsigned>(image.width())+
                static_cast<unsigned>(y)*
                static_cast<unsigned>(image.width())+
                static_cast<unsigned>(x);
        }
    }

public:
    fill() : classifier(0){}
    fill(const CLASS_TYPE* _classifier) : classifier(_classifier) {}
   
    // Grow sub
    //  Classifies the pixels by growing the region around the seed points.
    //  Voxels are subdivided in subs*subs*subs sub-voxels to incorporate
    //  partial volume effects.
    //  Efficient when the non-zero region is significantly smaller than the
    //  image.
    void
    grow_sub(image_type& image, const POINT_TYPE *_seeds, int _seed_count,
        int subs = 2) const
    {
        if(!classifier) return;
        unsigned idx = image.width(), idy = image.height(), idz = image.depth();
        image.fill(0);
        unsigned *seeds = new unsigned[idx*idy*idz], seed_count = 0;
        for(int i = 0; i < _seed_count; i+=3)
        {
            POINT_TYPE x = _seeds[i], y = _seeds[i+1], z = _seeds[i+2];
            image.xyz2ijk(x, y, z);
            add_point(seeds, seed_count, image, static_cast<int>(x+0.5),
                static_cast<int>(y+0.5), static_cast<int>(z+0.5));
        }
#ifdef FILL_DEBUG
        double inside = 0;
#endif
        double dx = image.metadata.spacing[0]/(subs+1),
               dy = image.metadata.spacing[1]/(subs+1),
               dz = image.metadata.spacing[2]/(subs+1);
        double sx = dx/2-image.metadata.spacing[0]/2,
               sy = dy/2-image.metadata.spacing[1]/2,
               sz = dz/2-image.metadata.spacing[2]/2;
        POINT_TYPE p[3];
        while(seed_count--)
        {
            unsigned seed = seeds[seed_count];
            unsigned z = seed/(idx*idy);
            unsigned y = (seed-z*idx*idy)/idx;
            unsigned x = seed-z*idx*idy-y*idx;
            double xr = x, yr = y, zr = z;
            image.ijk2xyz(xr, yr, zr);

            xr += sx,
            yr += sy,
            zr += sz;
            LABEL_TYPE c = 0;
            for(int i = 0; i < subs; i++)
            {
                p[0] = xr+dx*i;
                for(int j = 0; j < subs; j++)
                {
                    p[1] = yr+dy*j;
                    for(int k = 0; k < subs; k++)
                    {
                        p[2] = zr+dz*k;
                        c += classifier->classify(p);
                    }
                }
            }
            c /= subs*subs*subs;
#ifdef FILL_DEBUG
            inside += c;
#endif
            if(c)
            {
                add_point(seeds, seed_count, image, x-1, y, z);
                add_point(seeds, seed_count, image, x+1, y, z);
                add_point(seeds, seed_count, image, x, y-1, z);
                add_point(seeds, seed_count, image, x, y+1, z);
                add_point(seeds, seed_count, image, x, y, z-1);
                add_point(seeds, seed_count, image, x, y, z+1);
            }
            image(x, y, z) = c;
        }
#ifdef FILL_DEBUG
        std::cout << "Inside: " << inside << std::endl;
#endif
        delete[] seeds;
    }
};

#endif

