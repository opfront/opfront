/* Opfront - Optimal front segmentation
 * Copyright (c) 2014, Jens Petersen
 * All rights reserved.
 *
 * Opfront is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this software.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Opfront has a range of dependencies and if you decide to use or
 * modify it, it should be done in compliance with what ever licenses and
 * copyright issues that apply for those dependencies.
 */
#ifndef __GTS_REPAIR_H__
#define __GTS_REPAIR_H__

#include <gts.h>
#include <math.h>
#include "util/gts_util.h"
#include "opfront/gts_plugin.h"

gboolean
gts_repair_stop(gdouble cost, guint length, gpointer data)
{
    if(cost > *static_cast<gdouble*>(data)) return true;
    else return false;
}

gdouble
gts_is_intersecting(gpointer item, gpointer data)
{
    GtsEdge *edge = GTS_EDGE(item);
    GtsSurface *surface = GTS_SURFACE(data);
    if(!gts_edge_has_parent_surface(edge, surface))
    {
        return 1;
    }
    else
    {
        gdouble r = gts_point_distance2(
                GTS_POINT(GTS_SEGMENT(edge)->v1),
                GTS_POINT(GTS_SEGMENT(edge)->v2));
        if(r) r = -1/r;
        else r = -std::numeric_limits<gdouble>::max();
        return r;
    }
}

// Repair by edge collapse
//  Collapses the shortest edges part of self-intersecting faces in turn
//  until no more remain. Returns 1 if no further repairs are needed and
//  -1 if no changes could be made.
gint
gts_repair_by_edge_collapse(GtsSurface *s,
    GtsVertex* midvertex(GtsEdge *, GtsVertexClass *))
{
    static gdouble coarsen_length = 0, fold = 3.14/180;
    GtsSurface *intersecting = gts_surface_is_self_intersecting_front(s);
    if(!intersecting) return 1;
    int intersecting_faces = gts_surface_face_number(intersecting);
#ifdef OPFRONT_DEBUG
    std::cout << "Number of degenerate faces before "
        << intersecting_faces << std::flush;
#endif
    gts_surface_coarsen(s, gts_is_intersecting, intersecting,
        (GtsCoarsenFunc)midvertex, NULL, gts_repair_stop, &coarsen_length,
        fold);
    int intersecting_after = gts_surface_face_number(intersecting);
#ifdef OPFRONT_DEBUG
    std::cout << " after " << intersecting_after << std::endl;
#endif
    gts_object_destroy(GTS_OBJECT(intersecting));
    if(intersecting_faces == intersecting_after) return -1;
    else return 0;
}

GtsVertex*
gts_smooth_vertex(GtsVertex *o, gpointer params)
{
    //GtsSurface *surface = GTS_SURFACE(params);
    GtsVertex *n = GTS_VERTEX(gts_object_clone(GTS_OBJECT(o)));
    static const double amount = 0.2;
    double j = 1;
    for(GSList *i = o->segments; i; i = i->next)
    {
        GtsSegment *s = GTS_SEGMENT(i->data);
        GtsVertex *e;
        //if(!gts_edge_has_parent_surface(GTS_EDGE(s), surface))
        //    continue;
        if(s->v1 == o) e = s->v2;
        else e = s->v1;
        gts_util_vertex_add(n, e, amount);
        j += amount;
    }
    gts_util_vertex_multiply(n, 1.0/(double)j);
    return n;
}

// Repair by smoothing
//  Repairs the self-intersecting faces by smoothing. Returns 1
//  if no further repairs are needed.
gint
gts_repair_by_smoothing(GtsSurface *s)
{
    GtsSurface *intersecting = gts_surface_is_self_intersecting_front(s);
    if(!intersecting) return 1;
#ifdef OPFRONT_DEBUG
    std::cout << "Number of degenerate faces: " <<
        gts_surface_face_number(intersecting) << std::endl;
#endif
    gts_util_apply_vertex_function(intersecting, gts_smooth_vertex, 0);
    gts_object_destroy(GTS_OBJECT(intersecting));
    return 0;
}

void
gts_repair(GtsSurface *s, int iterations,
    GtsVertex* (*midvertex)(GtsEdge *, GtsVertexClass *))
{
    // Remove self-intersections.
    for(int i = 0; i < iterations+1; i++)
    {
        if(gts_repair_by_smoothing(s) ||
           gts_repair_by_edge_collapse(s, midvertex) == 1)
            break;
    }
    gts_repair_by_smoothing(s);
}

#endif

